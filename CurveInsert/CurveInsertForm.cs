﻿/*
【作者】郝伟

【日期】2022/01/16

【作用】
本程序用于将目标文件夹中的曲线数据导入到chystat的Curves表中。并在转换后将曲线文件移到到备份文件夹中。

【配置】
使用本程序需要配置CurveInsertForm.cs中的三个变量：
constr:                 数据库chystat的连接字符串；
curveDirectory:         曲线文件的输入文件夹；
curveBackupDirectory:   曲线备份文件夹，如果不移动文件的话，同一文件会一直转。

【目标数据表】
# QDAS.Curves 表
# 在QDAS数据库上建立Curves表用于存储曲线，包括以下字段
列名	类型	属性	作用
CurveID	    int	主键，       唯一自增，曲线编号
ModelID	    nchar(3)		表示转换器编号，如C08表示kistler
TestTime	datetime	    not null	测量时间
Code	    nvchar(50)	    曲线索引号	为输入文件的文件名，如 “C08_20190924163826_001_29995920.txt”
PointCount	int		        测量点的个数
PointData	nvchar(MAX)		所有数据点数据。

【参考资料】
曲线设计 https://docs.qq.com/sheet/DWWpPV0tQRHd0dHJt?tab=BB08J2

 */
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace CurveInsert
{
    public partial class CurveInsertForm : Form
    {
        // 曲线文件存放路径列表
        List<string> curveDirectories = new List<string>();

        //  数据库连接字符串, constr1 是本地连接， constr2 是远程连接
        //string constr = "Data Source=(local);Initial Catalog=chystat; Trusted_Connection=true;";  // 本地连接
        string constr = "Data Source = 10.0.7.186;Initial Catalog = chystat; User ID = sa; Password = Temp@pass;"; // 远程连接
        string curveDirectory = @"D:\cruves\curve";                   // 曲线文件路径
        string curveBackupDirectory = @"D:\cruves\curve_backup";     // 曲线文件备份路径
        string configFile = "curve_paths.txt";

        public CurveInsertForm()
        {
            InitializeComponent();
        }


        private void CurveInsertForm_Load(object sender, EventArgs e)
        {
            Text = "曲线导入chystate数据库程序 2022/01/16 Ver.beta";
            Text = "曲线导入chystate数据库程序 2022/01/16 Ver.1.0";
            Text = "曲线导入chystate数据库程序 2022/04/09 Ver.1.1";

            // 为测试方便 ，当目录下不存在配置文件时，会自动创建，并将@"C:\Data\curves"写入文件。
            if (!File.Exists(configFile))
                File.WriteAllText(configFile, curveDirectory);


            // 在窗体启动时，会加载 curve_paths.txt 文件 curve 目录，可以是多个
            foreach (var dir in File.ReadAllLines(configFile))
                if (Directory.Exists(dir))
                    curveDirectories.Add(dir);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = timer1.Enabled ? "开始转换" : "结束转换";
            timer1.Enabled = !timer1.Enabled;
        }

        /// <summary>
        /// 添加日志信息。
        /// </summary>
        /// <param name="log">日志内容。</param>
        public void AddLog(string log, string logfile = "system.log")
        {
            lblInfo.Text = log;
            StreamWriter sw = new StreamWriter(logfile, true, Encoding.UTF8);
            sw.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss} {log}");
            sw.Close();
        }

        // 主要处理逻辑，每秒检测1次
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 如果曲线目录不存在，则退出。
            if (!Directory.Exists(curveDirectory))
                return;

            // 如果备份目录不存在，会进行创建
            if (!Directory.Exists(curveBackupDirectory))
                Directory.CreateDirectory(curveBackupDirectory);

            try
            {
                SqlConnection sqlcon = new SqlConnection(constr);
                sqlcon.Open();
                lblInfo.Text = "连接成功";

                // 同时对多个目录进行监听。
                foreach (var curveDirectory in curveDirectories)
                {
                    foreach (var curvefile in Directory.GetFiles(curveDirectory))
                    {
                        // 文件类型筛选, 只接收以.txt为后缀的曲线文件
                        if (Path.GetExtension(curvefile).ToLower() == ".txt" && CurveInfo.Parse(curvefile) is CurveInfo ci)
                        {
                            // 数据入库，将将输入文件移动到备份文件夹，防止重复转换
                            try
                            {
                                new SqlCommand(ci.ToSqlString(), sqlcon).ExecuteNonQuery();
                            }
                            catch (Exception ex1)
                            {
                                File.Delete(curvefile);
                                lblInfo.Text = "错误, 原因：" + ex1.Message;
                                AddLog($"Insert failed: {curvefile}.");
                            }

                            // 文件备份，失败也会记录。
                            try
                            {
                                File.Move(curvefile, Path.Combine(curveBackupDirectory, ci.Code + ".txt"));
                            }
                            catch (Exception ex2)
                            {
                                File.Delete(curvefile);
                                lblInfo.Text = "错误, 原因：" + ex2.Message;
                                AddLog($"Move failed: {curvefile}");
                            }
                        }
                    }
                }
                sqlcon.Close();
            }
            catch (Exception ex)
            {
                lblInfo.Text = "错误, 原因：" + ex.Message;
                AddLog("Failed. Error message: " + ex.Message);
            }



        }

    }
}
