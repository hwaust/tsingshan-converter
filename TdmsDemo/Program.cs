﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdmsDemo
{
    class Program
    {
        static string tdms_file = Path.GetFullPath(@"..\\data\\T1-0S4013515000100BQ03AAMH02752_20210815081627_0.tdms");

        static void Main(string[] args)
        {
            test4();
        }

        public static void test4()
        {
            Dictionary<string, Channel> channels = new Dictionary<string, Channel>();
            StringBuilder sb = new StringBuilder();
            var tdms = new NationalInstruments.Tdms.File(tdms_file);
            tdms.Open();

            // var g0 = tdms.Groups["TestItems"];

            foreach (var group in tdms.Groups)
            {
                Console.WriteLine("-----------------------" + group.Key + "-----------------------");

                foreach (var channel in group.Value.Channels)
                {
                    var display = channel.Value.Properties["Disaplay"];
                    Console.WriteLine($"{channel.Key}:" + display);
                }
                break;
            }  
        }


        public static void test3()
        {
            Dictionary<string, Channel> channels = new Dictionary<string, Channel>();
            StringBuilder sb = new StringBuilder();
            var tdms = new NationalInstruments.Tdms.File(tdms_file);
            tdms.Open();
            foreach (var group in tdms.Groups)
            {
                foreach (var channel in group.Value.Channels)
                {
                    if (!channels.Keys.Contains(channel.Key))
                        channels.Add(channel.Key, new Channel(channel.Key));
                    var ch = channels[channel.Key];
                    foreach (var item in channel.Value.Properties)
                        ch.AddPropertyIfNotExisted(item.Key, item.Value.ToString());
                    foreach (var item in channel.Value.GetData<double>())
                        ch.data.Add(new DataRow(group.Key, channel.Key, item));
                }
            }
            tdms.ReWrite("test.txt");

            foreach (var key in channels.Keys)
            {
                Console.WriteLine(key + ", " + channels[key]);
                sb.AppendLine(key + ", " + channels[key]);
            }

            foreach (var key in channels.Keys)
            {
                Console.WriteLine(channels[key].key);
                sb.AppendLine(channels[key].key);

                foreach (var subitem in channels[key].data.Select(row => row.gid).Distinct())
                {
                    var vs = "\t" + subitem + ", dataCount=" + channels[key].data.Count(row => row.gid == subitem);
                    Console.WriteLine(vs);
                    sb.AppendLine(vs);
                }
            }

            File.WriteAllText("data.txt", sb.ToString());
        }

        public static void test2()
        {
            string tdms_file = Path.GetFullPath(@"..\\data\\T1-0S4013515000100BQ03AAMH02752_20210815081627_0.tdms");
            Dictionary<string, Channel> channels = new Dictionary<string, Channel>();
            StringBuilder sb = new StringBuilder();
            var tdms = new NationalInstruments.Tdms.File(tdms_file);
            tdms.Open();
            foreach (var group in tdms.Groups)
            {
                sb.AppendLine($"gkey={group.Key}, ch.Count={group.Value.Channels.Count}.");
                foreach (var channel in group.Value.Channels)
                {
                    sb.AppendLine($"\tckey={channel.Key}, data.Count={channel.Value.DataCount}, properties.Count={channel.Value.Properties.Count}");

                    //sb.AppendLine("\t\t-------------- Properties BEGIN --------------");
                    //foreach (var item in channel.Value.Properties)
                    //    sb.AppendLine($"\t\t{item.Key}  = {item.Value}");
                    //sb.AppendLine("\t\t-------------- Properties END --------------");


                    //sb.AppendLine("\t\t-------------- VALUES START --------------");
                    //foreach (var item in channel.Value.GetData<object>())
                    //    sb.AppendLine("\t\tvalue=" + item); 
                    //sb.AppendLine("\t\t-------------- VALUES END --------------");
                }
            }

            Console.WriteLine(sb.ToString());
        }

        // TDMS Reader: https://github.com/mikeobrien/TDMSReader
        // Install cmd: PM> Install-Package TDMSReader
        // 说明：本示例将TDMS格式输出，生成树型结构。
        public static void test1()
        {
            string tdms_file = Path.GetFullPath(@"..\\data\\T1-0S4013515000100BQ03AAMH02752_20210815081627_0.tdms");
            // using (var output = new System.IO.StreamWriter(System.IO.File.Create(@"D:\export.txt")))

            StringBuilder sb = new StringBuilder();
            using (var tdms = new NationalInstruments.Tdms.File(tdms_file))
            {
                tdms.Open();
                foreach (var group in tdms.Groups)
                {
                    sb.AppendLine($"Group Info: key = {group.Key}, valuecount={group.Value.Count()}.");
                    Console.WriteLine($"Group Info: key = {group.Key}, valuecount={group.Value.Count()}.");
                    foreach (var channel in group.Value.Channels)
                    {
                        sb.AppendLine($"\tChannel Info: key={channel.Key}, datacount={channel.Value.DataCount}, value.properties={channel.Value.Properties.Count}");
                        Console.WriteLine($"\tChannel Info: key={channel.Key}, datacount={channel.Value.DataCount}, value.properties={channel.Value.Properties.Count}");
                        sb.AppendLine("\t\t-------------- Properties BEGIN --------------");
                        foreach (var item in channel.Value.Properties)
                        {
                            sb.AppendLine($"\t\t{item.Key}  = {item.Value}");
                        }
                        sb.AppendLine("\t\t-------------- Properties END --------------");

                        sb.AppendLine("\t\t-------------- VALUES START --------------");
                        foreach (var item in channel.Value.GetData<object>())
                        {
                            sb.AppendLine("\t\tvalue=" + item);
                        }
                        sb.AppendLine("\t\t-------------- VALUES END --------------");
                    }
                }
                // foreach (var value in tdms.Groups["Noise data"].Channels["Noise_1"].GetData<double>())
                //    output.WriteLine(value);
            }
            File.WriteAllText("c:\\data\\tdms_data.txt", sb.ToString());
        }
    }
}
