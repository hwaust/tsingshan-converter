﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdmsDemo
{
    public class Channel
    {
        public string key="unknown";
        public string Display="no name";
		public string Disaplay = "输入电机扭矩";
		public string DevTag = "FPGA";
		public string Addon = "";
		public string ChanTag = "Input_Trq"; 
		public string Direct = "OUTPUT";
		public string Precision = "2";
		public string SourceUnit = "";
		public string PhyiscalUnit = "Nm"; 
		public string WaveForm = "no"; 
		public string Scaled = "yes"; 
		public string Desp = "输入转速（扭矩传感器）"; 
		public string CaliType = "0";
		public string CaliData = "wave"; 
		public string SignalProcessing = "";
		public string ProcessParameters = "";
		public string NI_ArrayColumn = "0";

		public List <DataRow> data = new List<DataRow>();

		public Dictionary<string, string> Propertyies = new Dictionary<string, string>();


		public Channel() { }
		public Channel(string key) { this.key = key; }

		public void AddPropertyIfNotExisted(string key, string value)
        {
            if (!Propertyies.Keys.Contains(key))
				Propertyies.Add(key, value);
        }

        public override string ToString()
        {
			return $"{key}: Pros.Count={Propertyies.Count}, data.Count={data.Count}.";
        }
    }

    public class DataRow
    {
        public string gid;

        public string cid;

		public int ordinal;

        public double value;


		public DataRow() { }

		public DataRow(string gid, string cid, double value)
        {
			this.gid = gid;
			this.cid = cid;
			this.value = value;
        }
    }
}
