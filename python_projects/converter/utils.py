class Person(object): 
    def talk(self):    # 父类中的方法
        print("person is talking....")


class Chinese(Person):    # 定义一个子类， 继承Person类
    def walk(self):      # 在子类中定义其自身的方法
        print('is walking...')


def test():
    print("Test")


