﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasTransfer
{
    class values
    {
        /// <summary>
        /// 日期格式说明。
        /// </summary>
        public static string DateFormatIntroduction =
@"为了方便自定义时间格式，采用以下通配符表示。
（假设当前时间为 2019.4.12 15:23:12）
yyyy: 4位年份, 如 2019
MM: 2位月，如 09
dd: 2位日，如 12
HH: 2位24小时制小时，如 15
hh: 2位12小时制小时，如 03
mm: 2位分钟，如 23
ss: 2位秒，如 12

示例：
# 输出完整的日期和时间
yyyy/MM/dd HH:mm:ss -> 2019/04/12 15:23:12
yyyy/MM/dd hh:mm:ss -> 2019/04/12 03:23:12
yyyyMMdd_hhmmss -> 20190412_032312
ddMMyyyy_hhmmss -> 12042019_032312

# 只输出日期
yyyy/MM/dd-> 2019/04/12 

# 只输出时间
HH:mm:ss -> 15:23:12";
        /* 一些帮助页面 */

    }
}
