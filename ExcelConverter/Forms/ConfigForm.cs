﻿using QDasTransfer.Classes;
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WindGoes6.Controls;

namespace QDasTransfer.Forms
{
    public partial class ConfigForm : Form
    {
        public ParamaterData pd = new ParamaterData();
        public bool needSave = false;

        public ConfigForm()
        {
            InitializeComponent();
        }

        private void setFolder(TextBox tb)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = tb.Text;
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tb.Text = fbd.SelectedPath;
            }
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            this.Text += $" (当前配置文件: {Path.GetFullPath(Common.ConfigFilepath)})";

            // 有的转换器会自带配置页，所以如果有的话，就加入进来。
            if (Common.transducer.ConfigPage != null)
            {
                TabPage page = new TabPage(Common.transducer.CompanyName);
                page.Controls.Add(Common.transducer.ConfigPage);
                this.tabControl1.TabPages.Add(page);
                // Common.transducer.LoadConfig();
            }
            //pgInput 
            rbBackup.Checked = pd.ProcessSourceFileType == 0;
            rbKeepInputFileLocation.Checked = pd.ProcessSourceFileType == 1;
            rbDelete.Checked = pd.ProcessSourceFileType == 2;
            gbBackupFolders.Enabled = rbBackup.Checked;
            txtSuccessfulFolder.Text = pd.FolderForSuccessed;
            txtFailedFolder.Text = pd.FolderForFailed;
            ckKeepBackupFolderStruct.Checked = pd.KeepBackupFolderStructType == 1;

            // pgOutput
            txtOutputFolder.Text = pd.OutputFolder;
            txtTempFolder.Text = pd.TempFolder;
            if (pd.DealSameOutputFileNameType == 0)
                rbReplaceOld.Checked = true;
            else if (pd.DealSameOutputFileNameType == 1)
                rbAddTimeToNew.Checked = true;
            else if (pd.DealSameOutputFileNameType == 2)
                rbIncrementIndex.Checked = true;
            else if (pd.DealSameOutputFileNameType == 3)
                rbMergeOutputFiles.Checked = true;

            ckKeepOutFolderStruct.Checked = pd.KeepOutFolderStructLevel >= 0;
            numlblKeepFolderLevel.Enabled = ckKeepOutFolderStruct.Checked;
            int lv = (int)Math.Abs(pd.KeepOutFolderStructLevel);
            numlblKeepFolderLevel.Value = lv < 1 ? 1 : lv;

            if (string.IsNullOrEmpty(pd.CostomizedDateTimeFormat))
            {
                rbNoDatetimeAppendix.Checked = true;
            }
            else if (pd.CostomizedDateTimeFormat == "#default#")
            {
                rbUseDefaultDateTimeAppendix.Checked = true;
            }
            else
            {
                rbUseCustomizedDateAppendix.Checked = true;
                txtCustomedDateFormat.Text = pd.CostomizedDateTimeFormat;
            }



            //pgAutoTransducer
            cbCircleUnit.SelectedIndex = 1;
            if (pd.SupportAutoTransducer)
            {
                ckAutoTransduce.Checked = pd.AutoTransducerAvaliable;
                gbAutoConfig.Enabled = pd.AutoTransducerAvaliable;
                numCircleValue.Value = pd.CircleValue;
                cbCircleUnit.SelectedIndex = pd.CircleUnit;
                dpStartTime.Value = pd.StartTime;
                dpEndTime.Value = pd.EndTime;
            }

            if (!pd.SupportAutoTransducer)
            {
                tabControl1.TabPages.Remove(pgAutoTransducer);
            }

            ckTraverseSubfolders.Checked = pd.TraverseSubfolders;

            /***** encoding page *****/
            lbEncodings.Items.Clear();
            foreach (Encoding ed in ParamaterData.Encodings)
                lbEncodings.Items.Add(ed.BodyName);
            lbEncodings.Items[0] = "default";
            lbEncodings.SelectedIndex = pd.EncodingID;


            // ControlHelper.AddBoollonTip(btnDateFormatInformation, "日期格式说明", values.DateFormatIntroduction);
        }

        private void btOutputFolder_Click(object sender, EventArgs e)
        {
            setFolder(txtOutputFolder);
        }

        private void btnTempFolder_Click(object sender, EventArgs e)
        {
            setFolder(txtTempFolder);
        }

        private void btnSuccessfulFolder_Click(object sender, EventArgs e)
        {
            setFolder(txtSuccessfulFolder);
        }

        private void btnFailedFolder_Click(object sender, EventArgs e)
        {
            setFolder(txtFailedFolder);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            needSave = true;

            // pgOutput
            pd.OutputFolder = txtOutputFolder.Text;
            pd.TempFolder = txtTempFolder.Text;
            if (rbReplaceOld.Checked)
                pd.DealSameOutputFileNameType = 0;
            else if (rbAddTimeToNew.Checked)
                pd.DealSameOutputFileNameType = 1;
            else if(rbIncrementIndex.Checked)
                pd.DealSameOutputFileNameType = 2;
            else if (rbMergeOutputFiles.Checked)
                pd.DealSameOutputFileNameType = 3;

            pd.KeepOutFolderStructLevel = ckKeepOutFolderStruct.Checked ? (int)numlblKeepFolderLevel.Value : -1;
            pd.KeepBackupFolderStructLevel = ckKeepOutFolderStruct.Checked ? (int)numlblKeepFolderLevel.Value : 0;
            pd.CostomizedDateTimeFormat = rbNoDatetimeAppendix.Checked ? "" : rbUseDefaultDateTimeAppendix.Checked ? "#default#" : txtCustomedDateFormat.Text;

            //pgBackups
            if (rbBackup.Checked)
                pd.ProcessSourceFileType = 0;
            else if (rbKeepInputFileLocation.Checked)
                pd.ProcessSourceFileType = 1;
            else if (rbDelete.Checked)
                pd.ProcessSourceFileType = 2;

            pd.KeepBackupFolderStructType = ckKeepBackupFolderStruct.Checked ? 1 : 0;
            pd.FolderForSuccessed = txtSuccessfulFolder.Text;
            pd.FolderForFailed = txtFailedFolder.Text;

            //pgAutoTransducer
            pd.AutoTransducerAvaliable = ckAutoTransduce.Checked;
            if (pd.AutoTransducerAvaliable)
            {
                pd.CircleValue = (int)numCircleValue.Value;
                pd.CircleUnit = cbCircleUnit.SelectedIndex;
                pd.StartTime = dpStartTime.Value;
                pd.EndTime = dpEndTime.Value;
            }

            pd.TraverseSubfolders = ckTraverseSubfolders.Checked;

            /****** Encoding page ******/
            pd.EncodingID = lbEncodings.SelectedIndex;

            // Common.transducer.SaveConfig();

            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ckKeepFolderStruct_Click(object sender, EventArgs e)
        {
            numlblKeepFolderLevel.Enabled = ckKeepOutFolderStruct.Checked;
        }

        private void ckAutoTransfer_Click(object sender, EventArgs e)
        {
            gbAutoConfig.Enabled = ckAutoTransduce.Checked;
        }

        private void rbBackup_CheckedChanged(object sender, EventArgs e)
        {
            gbBackupFolders.Enabled = true;
        }

        private void rbNoChange_CheckedChanged(object sender, EventArgs e)
        {
            gbBackupFolders.Enabled = false;
        }

        private void rbDelete_CheckedChanged(object sender, EventArgs e)
        {
            gbBackupFolders.Enabled = false;
        }

        private void CkKeepOutFolderStruct_CheckedChanged(object sender, EventArgs e)
        {
            numlblKeepFolderLevel.Enabled = ckKeepOutFolderStruct.Checked;
        }

        private void NumlblKeepFolderLevel_ValueChanged(object sender, EventArgs e)
        {

        }

        private void BtnDateFormatInformation_Click(object sender, EventArgs e)
        {

        }

        private void TxtCustomedDateFormat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string dtfmt = DateTime.Now.ToString(txtCustomedDateFormat.Text);
                txtCustomedDateFormatResult.Text = $"output.dfq -> output_{dtfmt}.dfq";
            }
            catch
            {
                txtCustomedDateFormatResult.Text = $"格式有误";
            }
        }



        private void RbUseDefaultDateTimeFormat_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDateTimeFormatGroupBox();
        }


        private void RbUseCustomizedDateFormat_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDateTimeFormatGroupBox();
        }

        private void RbNoDatetimeAppendix_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDateTimeFormatGroupBox();
        }

        private void UpdateDateTimeFormatGroupBox()
        {
            groupBox6.Enabled = rbUseCustomizedDateAppendix.Checked;
        }
    }
}
