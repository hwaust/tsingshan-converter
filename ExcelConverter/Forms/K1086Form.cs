﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QDasTransfer.Forms
{
    public partial class K1086Form : Form
    {
        public K1086Form()
        {
            InitializeComponent();
        }

        string config_file = "config_items.txt";
        public string K1086 = "";
        public string K0053 = "";

        private void lbItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbItems.SelectedIndex >= 0)
            {
                lblContent.Text = lbItems.SelectedItem.ToString();
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (lbItems.SelectedIndex >= 0)
            {
                K0053 = txtK0053.Text;
                K1086 = lblContent.Text;
                Close();
            }
        }

        private void K1086Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (K1086 == "")
            {
                var dr = MessageBox.Show("尚未选择任何配置项，是否确定不指定？", "配置项确认", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void K1086Form_Load(object sender, EventArgs e)
        {
            if (File.Exists(config_file))
            {
                lbItems.Items.Clear();
                foreach (var item in File.ReadAllLines(config_file))
                {
                    if (item.Trim().Length > 0 && !item.StartsWith("#"))
                        lbItems.Items.Add(item);                
                }
            }

            txtK0053.Focus();
        }
    }
}
