﻿namespace QDasTransfer.Forms
{
    partial class TaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbAutoConfig = new System.Windows.Forms.GroupBox();
            this.dpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dpStartTime = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numCircleValue = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbFile = new System.Windows.Forms.RadioButton();
            this.rbFolder = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSuccessfulFolder = new System.Windows.Forms.Button();
            this.txtSuccessfulFolder = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbAutoConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCircleValue)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAutoConfig
            // 
            this.gbAutoConfig.Controls.Add(this.label2);
            this.gbAutoConfig.Controls.Add(this.label1);
            this.gbAutoConfig.Controls.Add(this.dpStartTime);
            this.gbAutoConfig.Controls.Add(this.label17);
            this.gbAutoConfig.Controls.Add(this.label5);
            this.gbAutoConfig.Controls.Add(this.numCircleValue);
            this.gbAutoConfig.Controls.Add(this.dpEndTime);
            this.gbAutoConfig.Controls.Add(this.label6);
            this.gbAutoConfig.Enabled = false;
            this.gbAutoConfig.Location = new System.Drawing.Point(13, 115);
            this.gbAutoConfig.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAutoConfig.Name = "gbAutoConfig";
            this.gbAutoConfig.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAutoConfig.Size = new System.Drawing.Size(544, 176);
            this.gbAutoConfig.TabIndex = 29;
            this.gbAutoConfig.TabStop = false;
            this.gbAutoConfig.Text = "自动转换配置项";
            // 
            // dpEndTime
            // 
            this.dpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dpEndTime.Location = new System.Drawing.Point(320, 69);
            this.dpEndTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dpEndTime.Name = "dpEndTime";
            this.dpEndTime.ShowUpDown = true;
            this.dpEndTime.Size = new System.Drawing.Size(101, 26);
            this.dpEndTime.TabIndex = 33;
            this.dpEndTime.Value = new System.DateTime(2012, 6, 12, 23, 59, 59, 0);
            // 
            // dpStartTime
            // 
            this.dpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dpStartTime.Location = new System.Drawing.Point(88, 70);
            this.dpStartTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dpStartTime.Name = "dpStartTime";
            this.dpStartTime.ShowUpDown = true;
            this.dpStartTime.Size = new System.Drawing.Size(124, 26);
            this.dpStartTime.TabIndex = 32;
            this.dpStartTime.Value = new System.DateTime(2012, 6, 12, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(240, 76);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 27;
            this.label5.Text = "结束时间";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 76);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "开始时间";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 37);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 16);
            this.label17.TabIndex = 24;
            this.label17.Text = "监控周期";
            // 
            // numCircleValue
            // 
            this.numCircleValue.Location = new System.Drawing.Point(88, 33);
            this.numCircleValue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numCircleValue.Maximum = new decimal(new int[] {
            43200,
            0,
            0,
            0});
            this.numCircleValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCircleValue.Name = "numCircleValue";
            this.numCircleValue.Size = new System.Drawing.Size(124, 26);
            this.numCircleValue.TabIndex = 23;
            this.numCircleValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numCircleValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(219, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 16);
            this.label1.TabIndex = 36;
            this.label1.Text = "单位：分钟，最大值为 43200，即30天。";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 16);
            this.label2.TabIndex = 37;
            this.label2.Text = "注：开始时间需小于结束时间。";
            // 
            // rbFile
            // 
            this.rbFile.AutoSize = true;
            this.rbFile.Checked = true;
            this.rbFile.Location = new System.Drawing.Point(27, 27);
            this.rbFile.Margin = new System.Windows.Forms.Padding(4);
            this.rbFile.Name = "rbFile";
            this.rbFile.Size = new System.Drawing.Size(58, 20);
            this.rbFile.TabIndex = 0;
            this.rbFile.TabStop = true;
            this.rbFile.Text = "文件";
            this.rbFile.UseVisualStyleBackColor = true;
            // 
            // rbFolder
            // 
            this.rbFolder.AutoSize = true;
            this.rbFolder.Location = new System.Drawing.Point(170, 27);
            this.rbFolder.Margin = new System.Windows.Forms.Padding(4);
            this.rbFolder.Name = "rbFolder";
            this.rbFolder.Size = new System.Drawing.Size(74, 20);
            this.rbFolder.TabIndex = 1;
            this.rbFolder.Text = "文件夹";
            this.rbFolder.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSuccessfulFolder);
            this.groupBox1.Controls.Add(this.txtSuccessfulFolder);
            this.groupBox1.Controls.Add(this.rbFolder);
            this.groupBox1.Controls.Add(this.rbFile);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(544, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "任务类型";
            // 
            // btnSuccessfulFolder
            // 
            this.btnSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuccessfulFolder.Location = new System.Drawing.Point(436, 23);
            this.btnSuccessfulFolder.Name = "btnSuccessfulFolder";
            this.btnSuccessfulFolder.Size = new System.Drawing.Size(97, 29);
            this.btnSuccessfulFolder.TabIndex = 20;
            this.btnSuccessfulFolder.Text = "选择...";
            this.btnSuccessfulFolder.UseVisualStyleBackColor = true;
            this.btnSuccessfulFolder.Click += new System.EventHandler(this.BtnSuccessfulFolder_Click);
            // 
            // txtSuccessfulFolder
            // 
            this.txtSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSuccessfulFolder.Location = new System.Drawing.Point(31, 61);
            this.txtSuccessfulFolder.Name = "txtSuccessfulFolder";
            this.txtSuccessfulFolder.Size = new System.Drawing.Size(506, 26);
            this.txtSuccessfulFolder.TabIndex = 19;
            this.txtSuccessfulFolder.Text = "D:\\QDAS\\Backups\\Success";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(451, 298);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 34);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(339, 298);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(106, 34);
            this.btnSave.TabIndex = 30;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // TaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 344);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbAutoConfig);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 12F);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "TaskForm";
            this.Text = "任务窗体";
            this.gbAutoConfig.ResumeLayout(false);
            this.gbAutoConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCircleValue)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbAutoConfig;
        private System.Windows.Forms.DateTimePicker dpEndTime;
        private System.Windows.Forms.DateTimePicker dpStartTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numCircleValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbFile;
        private System.Windows.Forms.RadioButton rbFolder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSuccessfulFolder;
        private System.Windows.Forms.TextBox txtSuccessfulFolder;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}