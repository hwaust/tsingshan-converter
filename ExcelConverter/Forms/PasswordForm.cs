﻿using System;
using System.Windows.Forms;
using WindGoes6.Data;

namespace QDasTransfer
{
	public partial class PasswordForm : Form
	{
		string psw = "";
		IniAccess ia = new IniAccess(Common.ConfigFilepath);

		public PasswordForm()
		{
			InitializeComponent(); 
			// ia.Section = "System";
			psw = ia.ReadValue("ClosePassword");
			txtPassword.Text = psw;
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			ia.WriteValue("ClosePassword", txtPassword.Text);
			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void PasswordForm_Load(object sender, EventArgs e)
		{
			txtPassword.Select();
		}
	}
}
