﻿namespace QDasTransfer.Forms
{
	partial class ConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pgGeneral = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.ckRunAfterStart = new System.Windows.Forms.CheckBox();
            this.ckAutoStart = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ckConfirmClose = new System.Windows.Forms.CheckBox();
            this.pgInput = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbKeepInputFileLocation = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.gbBackupFolders = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ckKeepBackupFolderStruct = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSuccessfulFolder = new System.Windows.Forms.Button();
            this.txtFailedFolder = new System.Windows.Forms.TextBox();
            this.txtSuccessfulFolder = new System.Windows.Forms.TextBox();
            this.btnFailedFolder = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.rbBackup = new System.Windows.Forms.RadioButton();
            this.rbDelete = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ckTraverseSubfolders = new System.Windows.Forms.CheckBox();
            this.pgOutput = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbNoDatetimeAppendix = new System.Windows.Forms.RadioButton();
            this.rbUseDefaultDateTimeAppendix = new System.Windows.Forms.RadioButton();
            this.rbUseCustomizedDateAppendix = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnDateFormatInformation = new System.Windows.Forms.Button();
            this.txtCustomedDateFormatResult = new System.Windows.Forms.TextBox();
            this.txtCustomedDateFormat = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.ckKeepOutFolderStruct = new System.Windows.Forms.CheckBox();
            this.lblKeepFolderStruct = new System.Windows.Forms.Label();
            this.numlblKeepFolderLevel = new System.Windows.Forms.NumericUpDown();
            this.btOutputFolder = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTempFolder = new System.Windows.Forms.TextBox();
            this.btnTempFolder = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbIncrementIndex = new System.Windows.Forms.RadioButton();
            this.rbReplaceOld = new System.Windows.Forms.RadioButton();
            this.rbAddTimeToNew = new System.Windows.Forms.RadioButton();
            this.pgAutoTransducer = new System.Windows.Forms.TabPage();
            this.gbAutoConfig = new System.Windows.Forms.GroupBox();
            this.cbCircleUnit = new System.Windows.Forms.ComboBox();
            this.dpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dpStartTime = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numCircleValue = new System.Windows.Forms.NumericUpDown();
            this.ckAutoTransduce = new System.Windows.Forms.CheckBox();
            this.pgEncoding = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbEncodings = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.rbMergeOutputFiles = new System.Windows.Forms.RadioButton();
            this.tabControl1.SuspendLayout();
            this.pgGeneral.SuspendLayout();
            this.pgInput.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbBackupFolders.SuspendLayout();
            this.pgOutput.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numlblKeepFolderLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pgAutoTransducer.SuspendLayout();
            this.gbAutoConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCircleValue)).BeginInit();
            this.pgEncoding.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.pgGeneral);
            this.tabControl1.Controls.Add(this.pgInput);
            this.tabControl1.Controls.Add(this.pgOutput);
            this.tabControl1.Controls.Add(this.pgAutoTransducer);
            this.tabControl1.Controls.Add(this.pgEncoding);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(760, 554);
            this.tabControl1.TabIndex = 0;
            // 
            // pgGeneral
            // 
            this.pgGeneral.Controls.Add(this.label2);
            this.pgGeneral.Controls.Add(this.ckRunAfterStart);
            this.pgGeneral.Controls.Add(this.ckAutoStart);
            this.pgGeneral.Controls.Add(this.label16);
            this.pgGeneral.Controls.Add(this.ckConfirmClose);
            this.pgGeneral.Location = new System.Drawing.Point(4, 22);
            this.pgGeneral.Name = "pgGeneral";
            this.pgGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pgGeneral.Size = new System.Drawing.Size(752, 494);
            this.pgGeneral.TabIndex = 7;
            this.pgGeneral.Text = "综合";
            this.pgGeneral.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 8F);
            this.label2.Location = new System.Drawing.Point(40, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 11);
            this.label2.TabIndex = 33;
            this.label2.Text = "开启此选项后，程序启动后马上开始转换所有输入项。";
            // 
            // ckRunAfterStart
            // 
            this.ckRunAfterStart.AutoSize = true;
            this.ckRunAfterStart.Location = new System.Drawing.Point(23, 63);
            this.ckRunAfterStart.Name = "ckRunAfterStart";
            this.ckRunAfterStart.Size = new System.Drawing.Size(132, 16);
            this.ckRunAfterStart.TabIndex = 30;
            this.ckRunAfterStart.Text = "启动后立即开始转换";
            this.ckRunAfterStart.UseVisualStyleBackColor = true;
            // 
            // ckAutoStart
            // 
            this.ckAutoStart.AutoSize = true;
            this.ckAutoStart.Location = new System.Drawing.Point(23, 116);
            this.ckAutoStart.Name = "ckAutoStart";
            this.ckAutoStart.Size = new System.Drawing.Size(108, 16);
            this.ckAutoStart.TabIndex = 28;
            this.ckAutoStart.Text = "开机时自动启动";
            this.ckAutoStart.UseVisualStyleBackColor = true;
            this.ckAutoStart.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 8F);
            this.label16.Location = new System.Drawing.Point(40, 135);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(313, 11);
            this.label16.TabIndex = 29;
            this.label16.Text = "注意：此项改动会影响注册表，从而可能会引起安全软件报警。";
            this.label16.Visible = false;
            // 
            // ckConfirmClose
            // 
            this.ckConfirmClose.AutoSize = true;
            this.ckConfirmClose.Location = new System.Drawing.Point(23, 22);
            this.ckConfirmClose.Name = "ckConfirmClose";
            this.ckConfirmClose.Size = new System.Drawing.Size(132, 16);
            this.ckConfirmClose.TabIndex = 27;
            this.ckConfirmClose.Text = "关闭窗体时进行确认";
            this.ckConfirmClose.UseVisualStyleBackColor = true;
            // 
            // pgInput
            // 
            this.pgInput.Controls.Add(this.label15);
            this.pgInput.Controls.Add(this.groupBox4);
            this.pgInput.Controls.Add(this.label1);
            this.pgInput.Controls.Add(this.ckTraverseSubfolders);
            this.pgInput.Location = new System.Drawing.Point(4, 22);
            this.pgInput.Name = "pgInput";
            this.pgInput.Padding = new System.Windows.Forms.Padding(3);
            this.pgInput.Size = new System.Drawing.Size(752, 494);
            this.pgInput.TabIndex = 1;
            this.pgInput.Text = "输入选项";
            this.pgInput.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Location = new System.Drawing.Point(10, 315);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(734, 2);
            this.label15.TabIndex = 47;
            this.label15.Text = "label15";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbKeepInputFileLocation);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.gbBackupFolders);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.rbBackup);
            this.groupBox4.Controls.Add(this.rbDelete);
            this.groupBox4.Location = new System.Drawing.Point(11, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(732, 296);
            this.groupBox4.TabIndex = 46;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "转换后原输入文件处理方式";
            // 
            // rbKeepInputFileLocation
            // 
            this.rbKeepInputFileLocation.AutoSize = true;
            this.rbKeepInputFileLocation.Location = new System.Drawing.Point(22, 20);
            this.rbKeepInputFileLocation.Name = "rbKeepInputFileLocation";
            this.rbKeepInputFileLocation.Size = new System.Drawing.Size(143, 16);
            this.rbKeepInputFileLocation.TabIndex = 41;
            this.rbKeepInputFileLocation.Text = "保持输入文件位置不变";
            this.rbKeepInputFileLocation.UseVisualStyleBackColor = true;
            this.rbKeepInputFileLocation.CheckedChanged += new System.EventHandler(this.rbNoChange_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(40, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(401, 12);
            this.label9.TabIndex = 45;
            this.label9.Text = "转换后直接删除原文件。主要用于一些程序产生的重要性不高的过程文件。";
            // 
            // gbBackupFolders
            // 
            this.gbBackupFolders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBackupFolders.Controls.Add(this.label14);
            this.gbBackupFolders.Controls.Add(this.ckKeepBackupFolderStruct);
            this.gbBackupFolders.Controls.Add(this.label4);
            this.gbBackupFolders.Controls.Add(this.label3);
            this.gbBackupFolders.Controls.Add(this.btnSuccessfulFolder);
            this.gbBackupFolders.Controls.Add(this.txtFailedFolder);
            this.gbBackupFolders.Controls.Add(this.txtSuccessfulFolder);
            this.gbBackupFolders.Controls.Add(this.btnFailedFolder);
            this.gbBackupFolders.Enabled = false;
            this.gbBackupFolders.Location = new System.Drawing.Point(42, 82);
            this.gbBackupFolders.Name = "gbBackupFolders";
            this.gbBackupFolders.Size = new System.Drawing.Size(679, 164);
            this.gbBackupFolders.TabIndex = 39;
            this.gbBackupFolders.TabStop = false;
            this.gbBackupFolders.Text = "移动输入文件至以下文件夹";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(35, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(281, 12);
            this.label14.TabIndex = 50;
            this.label14.Text = "输出文件将根据输入目录的路径结构进行分类保存。";
            // 
            // ckKeepBackupFolderStruct
            // 
            this.ckKeepBackupFolderStruct.AutoSize = true;
            this.ckKeepBackupFolderStruct.Location = new System.Drawing.Point(16, 117);
            this.ckKeepBackupFolderStruct.Name = "ckKeepBackupFolderStruct";
            this.ckKeepBackupFolderStruct.Size = new System.Drawing.Size(108, 16);
            this.ckKeepBackupFolderStruct.TabIndex = 47;
            this.ckKeepBackupFolderStruct.Text = "保留原目录结构";
            this.ckKeepBackupFolderStruct.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(239, 12);
            this.label4.TabIndex = 19;
            this.label4.Text = "成功文件夹 （用于保存转换成功的文件夹）";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "失败文件夹 （用于保存转换失败的文件夹）";
            // 
            // btnSuccessfulFolder
            // 
            this.btnSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuccessfulFolder.Location = new System.Drawing.Point(601, 38);
            this.btnSuccessfulFolder.Name = "btnSuccessfulFolder";
            this.btnSuccessfulFolder.Size = new System.Drawing.Size(61, 21);
            this.btnSuccessfulFolder.TabIndex = 18;
            this.btnSuccessfulFolder.Text = "选择...";
            this.btnSuccessfulFolder.UseVisualStyleBackColor = true;
            this.btnSuccessfulFolder.Click += new System.EventHandler(this.btnSuccessfulFolder_Click);
            // 
            // txtFailedFolder
            // 
            this.txtFailedFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFailedFolder.Location = new System.Drawing.Point(16, 84);
            this.txtFailedFolder.Name = "txtFailedFolder";
            this.txtFailedFolder.Size = new System.Drawing.Size(579, 21);
            this.txtFailedFolder.TabIndex = 20;
            this.txtFailedFolder.Text = "D:\\QDAS\\Backups\\Failed";
            // 
            // txtSuccessfulFolder
            // 
            this.txtSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSuccessfulFolder.Location = new System.Drawing.Point(16, 36);
            this.txtSuccessfulFolder.Name = "txtSuccessfulFolder";
            this.txtSuccessfulFolder.Size = new System.Drawing.Size(579, 21);
            this.txtSuccessfulFolder.TabIndex = 17;
            this.txtSuccessfulFolder.Text = "D:\\QDAS\\Backups\\Success";
            // 
            // btnFailedFolder
            // 
            this.btnFailedFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFailedFolder.Location = new System.Drawing.Point(601, 83);
            this.btnFailedFolder.Name = "btnFailedFolder";
            this.btnFailedFolder.Size = new System.Drawing.Size(61, 21);
            this.btnFailedFolder.TabIndex = 21;
            this.btnFailedFolder.Text = "选择...";
            this.btnFailedFolder.UseVisualStyleBackColor = true;
            this.btnFailedFolder.Click += new System.EventHandler(this.btnFailedFolder_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(41, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(281, 12);
            this.label7.TabIndex = 44;
            this.label7.Text = "不对原文件进行处理。此选项主要用于增量转换器。";
            // 
            // rbBackup
            // 
            this.rbBackup.AutoSize = true;
            this.rbBackup.Checked = true;
            this.rbBackup.Location = new System.Drawing.Point(22, 60);
            this.rbBackup.Name = "rbBackup";
            this.rbBackup.Size = new System.Drawing.Size(119, 16);
            this.rbBackup.TabIndex = 42;
            this.rbBackup.TabStop = true;
            this.rbBackup.Text = "移动至指定文件夹";
            this.rbBackup.UseVisualStyleBackColor = true;
            this.rbBackup.CheckedChanged += new System.EventHandler(this.rbBackup_CheckedChanged);
            // 
            // rbDelete
            // 
            this.rbDelete.AutoSize = true;
            this.rbDelete.Location = new System.Drawing.Point(22, 252);
            this.rbDelete.Name = "rbDelete";
            this.rbDelete.Size = new System.Drawing.Size(71, 16);
            this.rbDelete.TabIndex = 43;
            this.rbDelete.Text = "直接删除";
            this.rbDelete.UseVisualStyleBackColor = true;
            this.rbDelete.CheckedChanged += new System.EventHandler(this.rbDelete_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 8F);
            this.label1.Location = new System.Drawing.Point(33, 346);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(357, 11);
            this.label1.TabIndex = 32;
            this.label1.Text = "开启此选项后，如果输入目录下有子目录，子目录的数据也会得到转换。";
            // 
            // ckTraverseSubfolders
            // 
            this.ckTraverseSubfolders.AutoSize = true;
            this.ckTraverseSubfolders.Location = new System.Drawing.Point(11, 327);
            this.ckTraverseSubfolders.Name = "ckTraverseSubfolders";
            this.ckTraverseSubfolders.Size = new System.Drawing.Size(144, 16);
            this.ckTraverseSubfolders.TabIndex = 31;
            this.ckTraverseSubfolders.Text = "转换目录时遍历子目录";
            this.ckTraverseSubfolders.UseVisualStyleBackColor = true;
            // 
            // pgOutput
            // 
            this.pgOutput.Controls.Add(this.groupBox5);
            this.pgOutput.Controls.Add(this.groupBox3);
            this.pgOutput.Controls.Add(this.groupBox1);
            this.pgOutput.Controls.Add(this.groupBox2);
            this.pgOutput.Location = new System.Drawing.Point(4, 22);
            this.pgOutput.Name = "pgOutput";
            this.pgOutput.Padding = new System.Windows.Forms.Padding(3);
            this.pgOutput.Size = new System.Drawing.Size(752, 528);
            this.pgOutput.TabIndex = 0;
            this.pgOutput.Text = "输出选项";
            this.pgOutput.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbNoDatetimeAppendix);
            this.groupBox5.Controls.Add(this.rbUseDefaultDateTimeAppendix);
            this.groupBox5.Controls.Add(this.rbUseCustomizedDateAppendix);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Location = new System.Drawing.Point(12, 314);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(723, 185);
            this.groupBox5.TabIndex = 59;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "在输出文件名中追加日期时间";
            // 
            // rbNoDatetimeAppendix
            // 
            this.rbNoDatetimeAppendix.AutoSize = true;
            this.rbNoDatetimeAppendix.Checked = true;
            this.rbNoDatetimeAppendix.Location = new System.Drawing.Point(19, 25);
            this.rbNoDatetimeAppendix.Name = "rbNoDatetimeAppendix";
            this.rbNoDatetimeAppendix.Size = new System.Drawing.Size(95, 16);
            this.rbNoDatetimeAppendix.TabIndex = 62;
            this.rbNoDatetimeAppendix.TabStop = true;
            this.rbNoDatetimeAppendix.Text = "不添加时间戳";
            this.rbNoDatetimeAppendix.UseVisualStyleBackColor = true;
            this.rbNoDatetimeAppendix.CheckedChanged += new System.EventHandler(this.RbNoDatetimeAppendix_CheckedChanged);
            // 
            // rbUseDefaultDateTimeAppendix
            // 
            this.rbUseDefaultDateTimeAppendix.AutoSize = true;
            this.rbUseDefaultDateTimeAppendix.Location = new System.Drawing.Point(19, 50);
            this.rbUseDefaultDateTimeAppendix.Name = "rbUseDefaultDateTimeAppendix";
            this.rbUseDefaultDateTimeAppendix.Size = new System.Drawing.Size(413, 16);
            this.rbUseDefaultDateTimeAppendix.TabIndex = 60;
            this.rbUseDefaultDateTimeAppendix.Text = "添加默认时间时间戳，格式：output.dfq -> output_20190412091522.dfq";
            this.rbUseDefaultDateTimeAppendix.UseVisualStyleBackColor = true;
            this.rbUseDefaultDateTimeAppendix.CheckedChanged += new System.EventHandler(this.RbUseDefaultDateTimeFormat_CheckedChanged);
            // 
            // rbUseCustomizedDateAppendix
            // 
            this.rbUseCustomizedDateAppendix.AutoSize = true;
            this.rbUseCustomizedDateAppendix.Location = new System.Drawing.Point(19, 75);
            this.rbUseCustomizedDateAppendix.Name = "rbUseCustomizedDateAppendix";
            this.rbUseCustomizedDateAppendix.Size = new System.Drawing.Size(119, 16);
            this.rbUseCustomizedDateAppendix.TabIndex = 61;
            this.rbUseCustomizedDateAppendix.Text = "添加自定义时间戳";
            this.rbUseCustomizedDateAppendix.UseVisualStyleBackColor = true;
            this.rbUseCustomizedDateAppendix.CheckedChanged += new System.EventHandler(this.RbUseCustomizedDateFormat_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.btnDateFormatInformation);
            this.groupBox6.Controls.Add(this.txtCustomedDateFormatResult);
            this.groupBox6.Controls.Add(this.txtCustomedDateFormat);
            this.groupBox6.Enabled = false;
            this.groupBox6.Location = new System.Drawing.Point(44, 95);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(679, 75);
            this.groupBox6.TabIndex = 59;
            this.groupBox6.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 56;
            this.label11.Text = "实际效果";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 55;
            this.label10.Text = "输入格式";
            // 
            // btnDateFormatInformation
            // 
            this.btnDateFormatInformation.Location = new System.Drawing.Point(577, 14);
            this.btnDateFormatInformation.Name = "btnDateFormatInformation";
            this.btnDateFormatInformation.Size = new System.Drawing.Size(96, 49);
            this.btnDateFormatInformation.TabIndex = 58;
            this.btnDateFormatInformation.Text = "格式说明\r\n（按住查看）";
            this.btnDateFormatInformation.UseVisualStyleBackColor = true;
            this.btnDateFormatInformation.Click += new System.EventHandler(this.BtnDateFormatInformation_Click);
            // 
            // txtCustomedDateFormatResult
            // 
            this.txtCustomedDateFormatResult.Location = new System.Drawing.Point(65, 45);
            this.txtCustomedDateFormatResult.Name = "txtCustomedDateFormatResult";
            this.txtCustomedDateFormatResult.ReadOnly = true;
            this.txtCustomedDateFormatResult.Size = new System.Drawing.Size(305, 21);
            this.txtCustomedDateFormatResult.TabIndex = 57;
            this.txtCustomedDateFormatResult.Text = "output.dfq -> output_2019_04_12_10_20_14_453.dfq";
            // 
            // txtCustomedDateFormat
            // 
            this.txtCustomedDateFormat.Location = new System.Drawing.Point(65, 18);
            this.txtCustomedDateFormat.Name = "txtCustomedDateFormat";
            this.txtCustomedDateFormat.Size = new System.Drawing.Size(305, 21);
            this.txtCustomedDateFormat.TabIndex = 54;
            this.txtCustomedDateFormat.Text = "yyyy_MM_dd_HH_mm_ss_fff";
            this.txtCustomedDateFormat.TextChanged += new System.EventHandler(this.TxtCustomedDateFormat_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtOutputFolder);
            this.groupBox3.Controls.Add(this.ckKeepOutFolderStruct);
            this.groupBox3.Controls.Add(this.lblKeepFolderStruct);
            this.groupBox3.Controls.Add(this.numlblKeepFolderLevel);
            this.groupBox3.Controls.Add(this.btOutputFolder);
            this.groupBox3.Location = new System.Drawing.Point(12, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(723, 88);
            this.groupBox3.TabIndex = 51;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "输出文件夹 （转换好的文件输出位置）";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 8F);
            this.label13.Location = new System.Drawing.Point(36, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(258, 11);
            this.label13.TabIndex = 49;
            this.label13.Text = "输出文件将根据输入目录的路径结构进行分类保存。";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputFolder.Location = new System.Drawing.Point(19, 18);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(631, 21);
            this.txtOutputFolder.TabIndex = 11;
            this.txtOutputFolder.Text = "D:\\QDAS\\Output";
            // 
            // ckKeepOutFolderStruct
            // 
            this.ckKeepOutFolderStruct.AutoSize = true;
            this.ckKeepOutFolderStruct.Location = new System.Drawing.Point(19, 48);
            this.ckKeepOutFolderStruct.Name = "ckKeepOutFolderStruct";
            this.ckKeepOutFolderStruct.Size = new System.Drawing.Size(108, 16);
            this.ckKeepOutFolderStruct.TabIndex = 46;
            this.ckKeepOutFolderStruct.Text = "保留原目录结构";
            this.ckKeepOutFolderStruct.UseVisualStyleBackColor = true;
            this.ckKeepOutFolderStruct.CheckedChanged += new System.EventHandler(this.CkKeepOutFolderStruct_CheckedChanged);
            this.ckKeepOutFolderStruct.Click += new System.EventHandler(this.ckKeepFolderStruct_Click);
            // 
            // lblKeepFolderStruct
            // 
            this.lblKeepFolderStruct.AutoSize = true;
            this.lblKeepFolderStruct.Location = new System.Drawing.Point(142, 49);
            this.lblKeepFolderStruct.Name = "lblKeepFolderStruct";
            this.lblKeepFolderStruct.Size = new System.Drawing.Size(53, 12);
            this.lblKeepFolderStruct.TabIndex = 47;
            this.lblKeepFolderStruct.Text = "保持级数";
            // 
            // numlblKeepFolderLevel
            // 
            this.numlblKeepFolderLevel.Enabled = false;
            this.numlblKeepFolderLevel.Location = new System.Drawing.Point(201, 46);
            this.numlblKeepFolderLevel.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numlblKeepFolderLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numlblKeepFolderLevel.Name = "numlblKeepFolderLevel";
            this.numlblKeepFolderLevel.Size = new System.Drawing.Size(65, 21);
            this.numlblKeepFolderLevel.TabIndex = 48;
            this.numlblKeepFolderLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numlblKeepFolderLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numlblKeepFolderLevel.ValueChanged += new System.EventHandler(this.NumlblKeepFolderLevel_ValueChanged);
            // 
            // btOutputFolder
            // 
            this.btOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btOutputFolder.Location = new System.Drawing.Point(656, 18);
            this.btOutputFolder.Name = "btOutputFolder";
            this.btOutputFolder.Size = new System.Drawing.Size(61, 21);
            this.btOutputFolder.TabIndex = 12;
            this.btOutputFolder.Text = "选择...";
            this.btOutputFolder.UseVisualStyleBackColor = true;
            this.btOutputFolder.Click += new System.EventHandler(this.btOutputFolder_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtTempFolder);
            this.groupBox1.Controls.Add(this.btnTempFolder);
            this.groupBox1.Location = new System.Drawing.Point(12, 111);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(723, 54);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "临时文件夹 （用于保存转换时的临时数据）";
            // 
            // txtTempFolder
            // 
            this.txtTempFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTempFolder.Location = new System.Drawing.Point(19, 21);
            this.txtTempFolder.Name = "txtTempFolder";
            this.txtTempFolder.Size = new System.Drawing.Size(631, 21);
            this.txtTempFolder.TabIndex = 14;
            this.txtTempFolder.Text = "D:\\QDAS\\Temp";
            // 
            // btnTempFolder
            // 
            this.btnTempFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTempFolder.Location = new System.Drawing.Point(656, 20);
            this.btnTempFolder.Name = "btnTempFolder";
            this.btnTempFolder.Size = new System.Drawing.Size(61, 21);
            this.btnTempFolder.TabIndex = 15;
            this.btnTempFolder.Text = "选择...";
            this.btnTempFolder.UseVisualStyleBackColor = true;
            this.btnTempFolder.Click += new System.EventHandler(this.btnTempFolder_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.rbMergeOutputFiles);
            this.groupBox2.Controls.Add(this.rbIncrementIndex);
            this.groupBox2.Controls.Add(this.rbReplaceOld);
            this.groupBox2.Controls.Add(this.rbAddTimeToNew);
            this.groupBox2.Location = new System.Drawing.Point(12, 171);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(723, 137);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "存在同名文件的处理方式";
            // 
            // rbIncrementIndex
            // 
            this.rbIncrementIndex.AutoSize = true;
            this.rbIncrementIndex.Location = new System.Drawing.Point(24, 83);
            this.rbIncrementIndex.Name = "rbIncrementIndex";
            this.rbIncrementIndex.Size = new System.Drawing.Size(287, 16);
            this.rbIncrementIndex.TabIndex = 4;
            this.rbIncrementIndex.Text = "加增量编号，如: output.dfq -> output_001.dfq";
            this.rbIncrementIndex.UseVisualStyleBackColor = true;
            // 
            // rbReplaceOld
            // 
            this.rbReplaceOld.AutoSize = true;
            this.rbReplaceOld.Checked = true;
            this.rbReplaceOld.Location = new System.Drawing.Point(24, 24);
            this.rbReplaceOld.Name = "rbReplaceOld";
            this.rbReplaceOld.Size = new System.Drawing.Size(233, 16);
            this.rbReplaceOld.TabIndex = 0;
            this.rbReplaceOld.TabStop = true;
            this.rbReplaceOld.Text = "直接覆盖，用新生成的文件替换原文件.";
            this.rbReplaceOld.UseVisualStyleBackColor = true;
            // 
            // rbAddTimeToNew
            // 
            this.rbAddTimeToNew.AutoSize = true;
            this.rbAddTimeToNew.Location = new System.Drawing.Point(24, 54);
            this.rbAddTimeToNew.Name = "rbAddTimeToNew";
            this.rbAddTimeToNew.Size = new System.Drawing.Size(281, 16);
            this.rbAddTimeToNew.TabIndex = 1;
            this.rbAddTimeToNew.Text = "加时间戳，如: 1.dfq -> 1_20120516183025.dfq";
            this.rbAddTimeToNew.UseVisualStyleBackColor = true;
            // 
            // pgAutoTransducer
            // 
            this.pgAutoTransducer.Controls.Add(this.gbAutoConfig);
            this.pgAutoTransducer.Controls.Add(this.ckAutoTransduce);
            this.pgAutoTransducer.Location = new System.Drawing.Point(4, 22);
            this.pgAutoTransducer.Name = "pgAutoTransducer";
            this.pgAutoTransducer.Padding = new System.Windows.Forms.Padding(3);
            this.pgAutoTransducer.Size = new System.Drawing.Size(752, 494);
            this.pgAutoTransducer.TabIndex = 5;
            this.pgAutoTransducer.Text = "自动转换";
            this.pgAutoTransducer.UseVisualStyleBackColor = true;
            // 
            // gbAutoConfig
            // 
            this.gbAutoConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAutoConfig.Controls.Add(this.cbCircleUnit);
            this.gbAutoConfig.Controls.Add(this.dpEndTime);
            this.gbAutoConfig.Controls.Add(this.dpStartTime);
            this.gbAutoConfig.Controls.Add(this.label5);
            this.gbAutoConfig.Controls.Add(this.label6);
            this.gbAutoConfig.Controls.Add(this.label17);
            this.gbAutoConfig.Controls.Add(this.numCircleValue);
            this.gbAutoConfig.Enabled = false;
            this.gbAutoConfig.Location = new System.Drawing.Point(36, 36);
            this.gbAutoConfig.Name = "gbAutoConfig";
            this.gbAutoConfig.Size = new System.Drawing.Size(703, 110);
            this.gbAutoConfig.TabIndex = 27;
            this.gbAutoConfig.TabStop = false;
            this.gbAutoConfig.Text = "自动转换配置项";
            // 
            // cbCircleUnit
            // 
            this.cbCircleUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCircleUnit.FormattingEnabled = true;
            this.cbCircleUnit.Items.AddRange(new object[] {
            "秒",
            "分钟",
            "小时"});
            this.cbCircleUnit.Location = new System.Drawing.Point(127, 26);
            this.cbCircleUnit.Name = "cbCircleUnit";
            this.cbCircleUnit.Size = new System.Drawing.Size(92, 20);
            this.cbCircleUnit.TabIndex = 35;
            // 
            // dpEndTime
            // 
            this.dpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dpEndTime.Location = new System.Drawing.Point(246, 58);
            this.dpEndTime.Name = "dpEndTime";
            this.dpEndTime.ShowUpDown = true;
            this.dpEndTime.Size = new System.Drawing.Size(77, 21);
            this.dpEndTime.TabIndex = 33;
            this.dpEndTime.Value = new System.DateTime(2012, 6, 12, 23, 59, 59, 0);
            // 
            // dpStartTime
            // 
            this.dpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dpStartTime.Location = new System.Drawing.Point(66, 58);
            this.dpStartTime.Name = "dpStartTime";
            this.dpStartTime.ShowUpDown = true;
            this.dpStartTime.Size = new System.Drawing.Size(94, 21);
            this.dpStartTime.TabIndex = 32;
            this.dpStartTime.Value = new System.DateTime(2012, 6, 12, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 27;
            this.label5.Text = "结束时间";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 26;
            this.label6.Text = "开始时间";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 24;
            this.label17.Text = "监控周期";
            // 
            // numCircleValue
            // 
            this.numCircleValue.Location = new System.Drawing.Point(66, 25);
            this.numCircleValue.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numCircleValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCircleValue.Name = "numCircleValue";
            this.numCircleValue.Size = new System.Drawing.Size(56, 21);
            this.numCircleValue.TabIndex = 23;
            this.numCircleValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numCircleValue.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // ckAutoTransduce
            // 
            this.ckAutoTransduce.AutoSize = true;
            this.ckAutoTransduce.Location = new System.Drawing.Point(16, 16);
            this.ckAutoTransduce.Name = "ckAutoTransduce";
            this.ckAutoTransduce.Size = new System.Drawing.Size(120, 16);
            this.ckAutoTransduce.TabIndex = 26;
            this.ckAutoTransduce.Text = "是否开启自动转换";
            this.ckAutoTransduce.UseVisualStyleBackColor = true;
            this.ckAutoTransduce.Click += new System.EventHandler(this.ckAutoTransfer_Click);
            // 
            // pgEncoding
            // 
            this.pgEncoding.Controls.Add(this.listBox1);
            this.pgEncoding.Controls.Add(this.label8);
            this.pgEncoding.Controls.Add(this.lbEncodings);
            this.pgEncoding.Controls.Add(this.label12);
            this.pgEncoding.Location = new System.Drawing.Point(4, 22);
            this.pgEncoding.Margin = new System.Windows.Forms.Padding(2);
            this.pgEncoding.Name = "pgEncoding";
            this.pgEncoding.Padding = new System.Windows.Forms.Padding(2);
            this.pgEncoding.Size = new System.Drawing.Size(752, 494);
            this.pgEncoding.TabIndex = 8;
            this.pgEncoding.Text = "编码";
            this.pgEncoding.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Items.AddRange(new object[] {
            "Default",
            "BIG5",
            "Unicode",
            "UTF-8",
            "UTF-16"});
            this.listBox1.Location = new System.Drawing.Point(32, 165);
            this.listBox1.Margin = new System.Windows.Forms.Padding(2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(602, 76);
            this.listBox1.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 139);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "输出文件编码";
            // 
            // lbEncodings
            // 
            this.lbEncodings.FormattingEnabled = true;
            this.lbEncodings.ItemHeight = 12;
            this.lbEncodings.Items.AddRange(new object[] {
            "Default",
            "BIG5",
            "Unicode",
            "UTF-8",
            "UTF-16"});
            this.lbEncodings.Location = new System.Drawing.Point(32, 40);
            this.lbEncodings.Margin = new System.Windows.Forms.Padding(2);
            this.lbEncodings.Name = "lbEncodings";
            this.lbEncodings.Size = new System.Drawing.Size(602, 76);
            this.lbEncodings.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 14);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "输入文件编码 ";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(544, 572);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(106, 34);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(656, 572);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 34);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // rbMergeOutputFiles
            // 
            this.rbMergeOutputFiles.AutoSize = true;
            this.rbMergeOutputFiles.Location = new System.Drawing.Point(23, 110);
            this.rbMergeOutputFiles.Name = "rbMergeOutputFiles";
            this.rbMergeOutputFiles.Size = new System.Drawing.Size(275, 16);
            this.rbMergeOutputFiles.TabIndex = 5;
            this.rbMergeOutputFiles.Text = "合并文件内容 (此功能只针对部分转换器可用）";
            this.rbMergeOutputFiles.UseVisualStyleBackColor = true;
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 618);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "综合设置";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.pgGeneral.ResumeLayout(false);
            this.pgGeneral.PerformLayout();
            this.pgInput.ResumeLayout(false);
            this.pgInput.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbBackupFolders.ResumeLayout(false);
            this.gbBackupFolders.PerformLayout();
            this.pgOutput.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numlblKeepFolderLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pgAutoTransducer.ResumeLayout(false);
            this.pgAutoTransducer.PerformLayout();
            this.gbAutoConfig.ResumeLayout(false);
            this.gbAutoConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCircleValue)).EndInit();
            this.pgEncoding.ResumeLayout(false);
            this.pgEncoding.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage pgOutput;
		private System.Windows.Forms.TabPage pgInput;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtOutputFolder;
		private System.Windows.Forms.Button btOutputFolder;
		private System.Windows.Forms.TextBox txtTempFolder;
		private System.Windows.Forms.Button btnTempFolder;
		private System.Windows.Forms.TabPage pgAutoTransducer;
		private System.Windows.Forms.GroupBox gbAutoConfig;
		private System.Windows.Forms.ComboBox cbCircleUnit;
		private System.Windows.Forms.DateTimePicker dpEndTime;
		private System.Windows.Forms.DateTimePicker dpStartTime;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.NumericUpDown numCircleValue;
		private System.Windows.Forms.CheckBox ckAutoTransduce;
		private System.Windows.Forms.GroupBox gbBackupFolders;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnSuccessfulFolder;
		private System.Windows.Forms.TextBox txtFailedFolder;
		private System.Windows.Forms.TextBox txtSuccessfulFolder;
		private System.Windows.Forms.Button btnFailedFolder;
		private System.Windows.Forms.TabPage pgGeneral;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rbReplaceOld;
		private System.Windows.Forms.RadioButton rbAddTimeToNew;
		private System.Windows.Forms.NumericUpDown numlblKeepFolderLevel;
		private System.Windows.Forms.Label lblKeepFolderStruct;
		private System.Windows.Forms.CheckBox ckKeepOutFolderStruct;
		private System.Windows.Forms.CheckBox ckRunAfterStart;
		private System.Windows.Forms.CheckBox ckAutoStart;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.CheckBox ckConfirmClose;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.RadioButton rbDelete;
		private System.Windows.Forms.RadioButton rbBackup;
		private System.Windows.Forms.RadioButton rbKeepInputFileLocation;
		private System.Windows.Forms.RadioButton rbIncrementIndex;
		private System.Windows.Forms.CheckBox ckTraverseSubfolders;
		private System.Windows.Forms.TabPage pgEncoding;
		private System.Windows.Forms.ListBox lbEncodings;
		private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox ckKeepBackupFolderStruct;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCustomedDateFormatResult;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCustomedDateFormat;
        private System.Windows.Forms.Button btnDateFormatInformation;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbUseDefaultDateTimeAppendix;
        private System.Windows.Forms.RadioButton rbUseCustomizedDateAppendix;
        private System.Windows.Forms.RadioButton rbNoDatetimeAppendix;
        private System.Windows.Forms.RadioButton rbMergeOutputFiles;
    }
}