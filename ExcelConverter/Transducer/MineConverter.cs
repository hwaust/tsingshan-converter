﻿/*
 * 两条转换指令说明
 * [Convert]    convert.exe c count2.mp4 10 10 209 112 0.35
 * [Screenshot] convert.exe s count2.mp4 1.png
*/


using QDasTransfer.Classes;
using QDasTransfer.Forms; 
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindGoes6.Data;
using WindGoes6.Sys;

namespace QDasTransfer.Transducer
{
    class MineConverter : TransducerBase
    {
        IniAccess ia = new IniAccess();

        /// <summary>
        /// 获得转换器的相关信息用于初始化。
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            CompanyName = "矿用XX系统";
            VertionInfo = "alpha1";
            pd.AutoTransducerAvaliable = true;
            pd.SupportAutoTransducer = true;
            pd.AddExt(".mp4");
        }

        public override bool TransferFile(string inpath)
        {

            string exe = ia.ReadValue("ExePath");
            exe = string.IsNullOrEmpty(exe) ? @"D:\data\MineDemo\convert\convert.exe" : exe;
            exe = @"D:\data\MineDemo\convert\convert.exe";
            string png = Path.GetFullPath("output.png");
            string info = ConsoleInvoker.RunCommand($"{exe}", 3000, "s", inpath, png);

            ImageAreaSelectionForm imageform = new ImageAreaSelectionForm();
            imageform.LoadImage(png);
            imageform.ShowDialog();

            Rectangle r = imageform.GetRectangle();
            string area = $"{r.X} {r.Y} {r.Width} {r.Height}";
            info = ConsoleInvoker.RunCommand($"{exe}", 13000, $"c {inpath} 10 10 209 112 0.35 \n");

            if (DateTime.Now > new DateTime(2019, 10, 1))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(info))
            {
                // 无后缀的文件名
                string filename = Path.GetFileNameWithoutExtension(inpath);
                string outdir = pd.GetOutDirectory(inpath);
                string outpath = Path.Combine(outdir, filename + ".txt");
                outpath = ProcessOutputFileNameIfRepeated(outpath);

                using (StreamWriter sw = new StreamWriter(outpath, true, Encoding.Default))
                {
                    string datetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    string infomation = info.Replace('\n', ',');
                    sw.WriteLine($"[{datetime}] {infomation}");
                }

                // 如果输出路径不存在，则进行创建。 
                if (!Directory.Exists(outdir))
                    Directory.CreateDirectory(outdir);

                LastOutputDfqFile =   outpath;
                LogList.Add(new TransLog(LogType.Success, "文件保存成功。", inpath, outpath));
            }
            else
            {
                LastOutputDfqFile = null;
                LogList.Add(new TransLog(LogType.Fail, "保存失败，原因路径不存在，或者没有写入权限。", inpath, ""));
            }
            
            return !string.IsNullOrEmpty(info);
        }



    }
}
