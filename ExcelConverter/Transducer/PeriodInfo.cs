﻿
namespace QDasTransfer.Transducer
{
    /// <summary>
    /// 表示转换时的周期信息。
    /// </summary>
    public class PeriodInfo
    {
        /// <summary>
        /// 周期间隔，单位为分钟，默认值为1.
        /// </summary>
        public int Interval { get; set; } = 1;

        /// <summary>
        /// 每天的开始时间，为相对于0点的分钟数。值域：[0, 60*24 - 1)
        /// </summary>
        public int StartSeconds { get; set; } = 0;

        /// <summary>
        /// 每天的结束时间，为相对于0点的分钟数。值域：[1, 60*24)
        /// </summary>
        public int EndSeconds { get; set; } = 1440;
         
    }
}
