﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QDasTransfer.Transducer
{
    /// <summary>
    /// 转换时的日志类，用于记录一些转换信息，每个Log对象表示一条日志信息。
    /// </summary>
    public class TransLog
    {
        private char splitter = (char)15;

        /// <summary>
        /// 用于生成最新的编号，默认值为10000。
        /// </summary>
        public static int LogID = 10000;

        /// <summary>
        /// 日志编号。
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 日志的类型。
        /// </summary>
        public LogType LogType { get; set; }

        /// <summary>
        /// 转换时间。
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// 输入文件或文件夹。
        /// </summary>
        public String Input { get; set; }

        /// <summary>
        /// 输出文件或文件组。
        /// </summary>
        public String Output { get; set; }

        /// <summary>
        /// 日志的内容。
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 备注。
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 可以附加的数据。
        /// </summary>
        public object Tag { get; set; }

        public TransLog() { ID = ++LogID; }

        public TransLog(LogType type, string message, string infile, string outfile = "-", string remark = "-")
        {
            ID = ++LogID;
            Date = DateTime.Now;
            LogType = type;
            Message = message;
            Input = infile;
            Output = outfile;
            Remark = remark;
        }

        public String ToFileString()
        {
            StringBuilder b = new StringBuilder();
            String s = splitter.ToString();
            b.Append(ID + s);
            b.Append(Date.ToString("yyyyMMddHHmmss") + s);
            b.Append(LogType + s);
            b.Append(Message + s);
            b.Append(Input + s);
            b.Append(Output);
            b.Append(Remark + s);


            return b.ToString();
        }


        public string[] GetStrings()
        {
            //编号，0.时间，1.事件，2.原因，3.输出文件
            return new string[] { ID.ToString(), Date.ToString(), LogType.ToString(), Message, Output };
        }
    } 
}
