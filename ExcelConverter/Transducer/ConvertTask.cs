﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace QDasTransfer.Transducer
{
    /// <summary>
    /// 转换任务，一次只对应一个文件或目录。可以是周期性的，也可以是一次性的。
    /// </summary>
    public class ConvertTask
    {
        /// <summary>
        /// 需要处理的文件路径。
        /// </summary>
        public string FullPath { get; set; }

        /// <summary>
        ///  0:file, 1: folder, 2: net shared folder
        /// </summary>
        public int InputType { get; set; } = 0;

        /// <summary>
        /// 最后一次转换时间。
        /// </summary>
        public DateTime LastTranducingTime { get; set; }

        /// <summary>
        /// 文件个数。
        /// </summary>
        public int FileCount { get; set; } = 1;

        /// <summary>
        /// 是否自动转换。
        /// </summary>
        public PeriodInfo AutoConvert { get; set; }

        /// <summary>
        /// 缺少构造函数。
        /// </summary>
        public ConvertTask() { }

        /// <summary>
        /// 路径和类型（0为文件，1为文件夹）
        /// </summary>
        /// <param name="path">输入路径。</param>
        /// <param name="type">任务类型。</param>
		public ConvertTask(string path, int type)
        {
            FullPath = path;
            InputType = type;
            FileCount = InputType == 0 ? 1 : Directory.GetFiles(path).Length;
            LastTranducingTime = new DateTime(2000, 1, 1);
        }

        /// <summary>
        /// 根据输入自动判断输入类型的构造函数。
        /// </summary>
        /// <param name="path"></param>
        public ConvertTask(string path)
        {
            FullPath = path;
            InputType = Directory.Exists(path) ? 1 : 0;
            FileCount = InputType == 0 ? 1 : Directory.GetFiles(path).Length;
            LastTranducingTime = new DateTime(2000, 1, 1);
        }


        public string[] ToStrings()
        {
            string s = LastTranducingTime.Year == 2000 ? "尚未转换" : LastTranducingTime.ToString();
            return new string[] { FullPath, FileCount + "", s, "○" };
        }
    }
}
