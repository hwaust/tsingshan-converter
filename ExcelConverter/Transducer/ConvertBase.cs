﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasTransfer.Transducer
{
    public class ConvertBase
    {
        /// <summary>
        /// 公司名称，必填信息。
        /// </summary>
        public string CompanyName { get; set; } 
        /// <summary>
        /// 版本号，必填信息。如，alpha1 或 v1.2.1。
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 转换器扩展名，以点号分隔，必填信息。如 "xls.xlsx"。
        /// </summary>
        public string Extensions { get; set; }

        /// <summary>
        /// 转换函数主体。
        /// </summary>
        /// <param name="inpath"></param>
        /// <param name="outpath"></param>
        /// <returns></returns>
        public virtual bool Convert(string inpath, string outpath)
        {
            return true;
        }

        /// <summary>
        /// 返回转换器信息，格式为：公司名称||版本号||扩展名
        /// </summary>
        /// <returns></returns>
        public string GetInfoString()
        {
            return $"{CompanyName}||{Version}||{Extensions}";
        }
    }
}
