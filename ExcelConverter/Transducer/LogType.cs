﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasTransfer.Transducer
{ 
    /// <summary>
    /// 日志的类型。
    /// </summary>
    public enum LogType : int
    {
        /// <summary>
        /// 一般的日志。
        /// </summary>
        Nomal = 0,
        /// <summary>
        /// 转换成功 。
        /// </summary>
        Success = 1,
        /// <summary>
        /// 转换失败。
        /// </summary>
        Fail = 2,
        /// <summary>
        /// 系统事件。
        /// </summary>
        System = 4,
        /// <summary>
        /// 备份事件。
        /// </summary>
        Backup = 8,
        /// <summary>
        /// 用于测试。
        /// </summary>
        Debug = 16,
        /// <summary>
        /// 未知。
        /// </summary>
        Unknown = -1
    }
}
