﻿using QDasTransfer.Transducer;
using Spire.Xls;
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace QDasTransfer
{
	class Common
    { 
        /// <summary>
        /// 配置文件全路径。
        /// </summary>
        public static string ConfigFilepath;

		/// <summary>
		/// 转换类，全局使用唯一一个。
		/// </summary>
		public static TransducerBase transducer;

		internal static void Initialize()
		{
            transducer = new ExcelConverter();
            ConfigFilepath = Application.StartupPath + "\\Config.ini";
        }

		/// <summary>
		/// 将日志内容追加至文件末尾。
		/// </summary>
		/// <param name="log">日志内容。</param>
		/// <param name="logfile">文件列表。</param>
		public static void Error(string log, string logfile = "application.log")
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(logfile, true, Encoding.UTF8))
				{
					sw.WriteLine($"{DateTime.Now:yyyy/MM/dd HH:mm:ss} {log}");
				}
			}
			catch { }
		}

		/// <summary>
		/// 从指定的Excel中提取表单，如果读取失败会写日志，同时返回为 null。
		/// </summary>
		/// <param name="inpath">输入Excel文件。</param>
		/// <param name="index">表单索引，下标从0开始。</param>
		/// <returns></returns>
		public static Worksheet LoadWorkSheet(string inpath, int index)
		{
			Worksheet sheet = null;
			Workbook workbook = new Workbook();
			try
			{
				workbook.LoadFromFile(inpath);
				if (workbook.Worksheets.Count == 0 || workbook.Worksheets.Count <= index)
					Error("文件索引错误，读取表单内容为空");
				else
					sheet = workbook.Worksheets[index]; // 读取成功后在这里赋值，此时才不为空。
			}
			catch (Exception ex1)
			{
				Error("文件读取失败，原因：" + ex1.Message);
			}
			finally
			{
				try
				{
					// 关闭文件，可能不需要操作，先保留

				}
				catch (Exception ex2) { Error($"{ inpath} 文件关闭失败，原因：" + ex2.Message); }
			}

			return sheet;
		}

		

		/// <summary>
		/// 从指定的Excel中提取表单，如果读取失败会写日志，同时返回为 null。
		/// </summary>
		/// <param name="inpath">输入Excel文件。</param>
		/// <param name="sheetName">表单的名称。</param>
		/// <returns></returns>
		public static Worksheet LoadWorkSheet(string inpath, string sheetName)
		{
			Worksheet sheet = null;
			Workbook workbook = new Workbook();
			try
			{
				workbook.LoadFromFile(inpath);
				if (workbook.Worksheets.Count == 0)
					Error("文件索引错误，读取表单内容为空");
				else
					sheet = workbook.Worksheets[sheetName]; // 读取成功后在这里赋值，此时才不为空。

			}
			catch (Exception ex1)
			{
				Error("文件读取失败，原因：" + ex1.Message);
			}
			finally
			{
				try
				{
					// 关闭文件，可能不需要操作，先保留

				}
				catch (Exception ex2) { Error($"{ inpath} 文件关闭失败，原因：" + ex2.Message); }
			}

			return sheet;
		}
	}
}
