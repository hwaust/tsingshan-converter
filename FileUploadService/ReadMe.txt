﻿
开发注意事项
1. 需要在 ServiceProcessInstaller 配置Account为LocalSystem
2. 服务的本质就是自动运行指定程序，所以服务没启动前可替换程序
4. 程序派生于ServiceBase类，包括借系统调用的接口
5. 服务与窗体类型，只要不停，就是在运行中，可以执行定时任务

安装说明
1. 使用 ServiceInstall.bat 安装服务
2. 需要使用管理员权限
3. 启动系统服务 services.msc 
4. 找到 FileUploadService 服务并设置为随系统启动

参考资料
[1] Create A Windows Service In C#, https://www.c-sharpcorner.com/article/create-windows-services-in-c-sharp/
