﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsHelloWorldService
{
    // C:\Windows\Microsoft.NET\Framework64\v4.0.30319>InstallUtil.exe C:\VSProjects\WindowsHelloWorldService\bin\Debug\WindowsHelloWorldService.exe
    // c:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe c:\data\MyFirstService\WindowsHelloWorldService.exe
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new UploadService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
