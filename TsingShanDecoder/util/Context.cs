﻿namespace DesoutterTest
{
    class Context
    {
        public static string ipAddress = "IP";
        public static string databaseName = "cvinet";
        public static string username = "XXXXXXXX";
        public static string password = "XXXXXXXX";

        //public static string connectionString = $"Server={ipAddress},1433;Database={databaseName};User Id={username};Password={password};TrustServerCertificate=true;";

        public static string GetConnectionString()
        {
            return $"Server={ipAddress},1433;Database={databaseName};User Id={username};Password={password};TrustServerCertificate=true;";
        }

        public static string ResultBasicSQL = "SELECT " + "RES_ID id," + "RES_ResultNumber resultnumber," + "RES_VIN vin," + "RES_DateTime datetime" + " FROM RESULT R ";
        public static string CurveBasicSQL = "SELECT " + "CRV_CurveID id," + "CRV_ResultID resultId," + "CRV_PointDuration pointDuration," + "CRV_PointCount pointCount," + "CRV_CurveType curveType," + "CRV_TorqueThreshold torqueThreshold," + "CRV_BlobFormat blobFormat," + "CRV_Points points " + " FROM CURVE C ";

    }
}
