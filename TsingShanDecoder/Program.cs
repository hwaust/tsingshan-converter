﻿using DesoutterTest;
using System;
using System.Collections.Generic;

namespace TsingShanDecoder
{
    internal class Program
    {
        /* 【使用说明】
         * 1. 在Context中配置好数据库的IP，数据库名，用户名和密码。
         * 2. 在Curves表中，找到CRV_ResultID的值，如60168420、60168430、60168434
         * 
         */
        static void Main(string[] args)
        {
            
            Context.ipAddress = "112.74.87.167"; // 112.74.87.167
            Context.databaseName = "newcvinet";
            Context.username = "sa";
            Context.password = "Root@2022";

            int[] idList = new int[] { 60168420, 60168430, 60168434 };//, 130770626, 130770620, 130770570, 130770557, 130770513, 101322014, 101321983, 101321966, 101321952, 101321947 };

            Console.WriteLine("================================开始测试=================================");

            ResultHandler resultHandler = new ResultHandler();
            // Data Source=112.74.87.167; Initial Catalog=newcvinet; User Id=sa;Password=Root@2022;
            // Integrated Security=SSPI; 或 Server=Aron1; Database=pubs; Trusted_Connection=True; 
            // Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUsername;Password=myPassword;
            // 账号: sa 密码：Root@2022 数据库名：

            foreach (int id in idList)
            {
                Result result = null;
                //加载Result数据
                result = resultHandler.FetchResultByID(id);

                //加载Result对应的Curve数据
                result = resultHandler.LazyLoadCurveInResult(result);

                //输出Result数据结果
                Console.WriteLine(result);
                //输出Result对应Cruve数据结果
                Console.WriteLine(result.Curve);

                List<Point> wavePoints = null;

                if (result.Curve.BlobFormat == 2)
                {
                    //如果BlobFormat为2需要提前解压缩
                    wavePoints = PointUtil.ConvertByteToPoints(DecodeUtil.DecompressZlibData(result.Curve.Points), DecodeUtil.ConvertToStride(result.Curve.PointDuration));
                }
                else
                {
                    //如果BlobFormat不为2常规解码
                    wavePoints = PointUtil.ConvertByteToPoints(result.Curve.Points, DecodeUtil.ConvertToStride(result.Curve.PointDuration));
                }

                //输出结果
                foreach (Point wavePoint in wavePoints)
                {
                    Console.WriteLine(wavePoint);
                }

                Console.WriteLine("=========================================================================");
            }
        }
    }
}
