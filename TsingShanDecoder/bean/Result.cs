﻿using System;

namespace DesoutterTest
{

    public class Result
    {
        private int id = -1;
        private long resultNumber;
        private string vin;
        private DateTime dateTime;
        private Curve curve;

        public Result()
        {
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Vin
        {
            get { return vin; }
            set { vin = value; }
        }

        public DateTime DateTime
        {
            get { return dateTime; }
            set { dateTime = value; }
        }

        public Curve Curve
        {
            get { return curve; }
            set { curve = value; }
        }

        public long ResultNumber
        {
            get { return resultNumber; }
            set { resultNumber = value; }
        }

        public override string ToString()
        {
            return $"Result{{id={id}, resultNumber={resultNumber}, vin='{vin}', dateTime={dateTime.ToString("yyyy-MM-dd HH:mm:ss")}}}";
        }

    }
}
