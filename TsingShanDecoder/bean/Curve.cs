﻿namespace DesoutterTest
{
    public class Curve
    {
        private int id;
        private int resultId;
        private short blobFormat;
        private short curveType;
        private short pointCount;
        private int pointDuration;
        private byte[] points;
        private double torqueThreshold;

        public Curve()
        {
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int ResultId
        {
            get { return resultId; }
            set { resultId = value; }
        }

        public short BlobFormat
        {
            get { return blobFormat; }
            set { blobFormat = value; }
        }

        public short CurveType
        {
            get { return curveType; }
            set { curveType = value; }
        }

        public short PointCount
        {
            get { return pointCount; }
            set { pointCount = value; }
        }

        public int PointDuration
        {
            get { return pointDuration; }
            set { pointDuration = value; }
        }

        public byte[] Points
        {
            get { return points; }
            set { points = value; }
        }

        public double TorqueThreshold
        {
            get { return torqueThreshold; }
            set { torqueThreshold = value; }
        }

        public override string ToString()
        {
            return $"Curve{{id={id}, resultId={resultId}, blobFormat={blobFormat}, curveType={curveType}, pointCount={pointCount}, pointDuration={pointDuration}, points={points.Length}, torqueThreshold={torqueThreshold}}}";
        }
    }

}
