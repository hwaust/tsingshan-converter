﻿using Ionic.Zlib;
using System;

namespace DesoutterTest
{
    class DecodeUtil
    {
        /// <summary>
        /// 计算步长
        /// </summary>
        /// <param name="pointDuration">数据库CURVE表中CRV_PointDuration字段值</param>
        /// <returns></returns>
        public static float ConvertToStride(int pointDuration)
        {
            float stride = pointDuration / 1000000.0F;
            return stride;
        }
        /// <summary>
        /// 以Zlib方式进行解压
        /// </summary>
        /// <param name="originalByteArray">数据库CURVE表中CRV_BlobFormat字段为2的前提下取得的CRV_BlobFormat字段的值</param>
        /// <returns></returns>
        public static byte[] DecompressZlibData(byte[] originalByteArray)
        {
            return DecompressZlibData(originalByteArray, 4);
        }

        private static byte[] DecompressZlibData(byte[] originalByteArray, int startIndex)
        {
            byte[] arrayOfByteForDecompress = new byte[originalByteArray.Length - startIndex];
            Buffer.BlockCopy(originalByteArray, startIndex, arrayOfByteForDecompress, 0, arrayOfByteForDecompress.Length);
            return ZlibStream.UncompressBuffer(arrayOfByteForDecompress);
        }


    }
}
