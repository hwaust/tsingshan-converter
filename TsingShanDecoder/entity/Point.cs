﻿namespace DesoutterTest
{
    class Point
    {
        public short Index { get; set; }
        public double Torque { get; set; }
        public double Angle { get; set; }
        public double TorqueRate { get; set; }
        public double Current { get; set; }
        public string Marker { get; set; }
        public float PointDuration { get; set; }

        public override string ToString()
        {
            return $"Point{{" +
                   $"torque={Torque}" +
                   $", angle={Angle}" +
                   $", torqueRate={TorqueRate}" +
                   $", current={Current}" +
                   $", marker='{Marker}'" +
                   $", time={PointDuration}" +
                   $"}}";
        }
    }
}
