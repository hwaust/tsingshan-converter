﻿using FileUploadService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    internal class Program
    {
        static void Main(string[] args)
        {

            DictionaryTest(); 
        }

        static void DictionaryTest()
        {

            // 直接初始化一个新对象。
            var dict = new DictionaryHelper();
            
            // 也可以从一个文件中初始化。
            dict = new DictionaryHelper ("config.dict"); 

            // 读取值，两种方式，具体可以对比v5和v6
            var v1 = dict.GetInt(cf.port, 0);
            var v2 = dict.GetFloat(cf.Temperature, 0);
            var v3 = dict.GetDouble(cf.TestValue, 0);
            var v4 = dict.GetDateTime("LastUpdateTime");
            var v5 = dict.GetBoolean(cf.ShowConfig);
            var v6 = dict[cf.ShowConfig];

            Console.WriteLine($"v1={v1},\ttype={v1.GetType().Name}");
            Console.WriteLine($"v2={v2},\ttype={v2.GetType().Name}");
            Console.WriteLine($"v3={v3},\ttype={v3.GetType().Name}");
            Console.WriteLine($"v4={v4},\ttype={v4.GetType().Name}");
            Console.WriteLine($"v5={v5},\ttype={v5.GetType().Name}");
            Console.WriteLine($"v6={v6},\ttype={v6.GetType().Name}\n");


            // 设置值，有以下两种方式
            // 方式1：直接使用字符串赋值
            dict["Name"]="Jack";
            dict[cf.port] = 10082 + "";

            // 方式2：使用SetValue方式
            dict.SetValue("FinalValue", 15.232);
            dict.SetValue("CurrentDate", DateTime.Now);


            // 输出为配置文件
            Console.WriteLine(dict.ToCSharpString() + "\n");

            // 保存为config.dict
            dict.Save("config.dict");
        }


        static void main()
        {
            var configFile = "config.ini";
            var dict = DictionaryHelper.LoadDictionary(configFile);
            if (dict != null)
                foreach (var key in dict.Keys)
                    Console.WriteLine($"key={key}, value={dict[key]}");
            else
                Console.WriteLine("Loading field.");

            var ip = "121.199.10.158";// dict["ip"];
            var port = "22"; // dict["port"];
            var user = "root";// dict["user"];
            var pwd = "$erver@2021";// dict["pwd"];

            // 使用 Process 进行任务处理
            Process process = new Process();
            process.StartInfo.FileName ="pscp.exe";
            process.StartInfo.UseShellExecute = false; // 必需设置此属性为true，下面两个属性才有效
            process.StartInfo.RedirectStandardOutput = true; // 关键行2
            process.StartInfo.CreateNoWindow = true;

            // 把所有 mapping 添加到此列表中
            var mappings = new List<string>();
            foreach (var key in dict.Keys)
                if (key.StartsWith("mapping"))
                    mappings.Add(dict[key]);

            foreach (var mapping in mappings)
            {
                if (!mapping.Contains('|'))
                    continue;

                // scr=源数据文件夹, dst=目标文件夹 
                var src = mapping.Split('|')[0];
                var dst = mapping.Split('|')[1];

                // 对源文件夹中的文件进行遍历处理。
                foreach (var file in Directory.GetFiles(src))
                {
                    // pscp -pw password C:\data\hello.exe root@121.199.10.158:hello.exe
                    // 测试可用： pscp -P 22 -pw $erver@2021 config.ini root@121.199.10.158: 
                    var argsStr = $"-P {port} -pw {pwd} {file} {user}@{ip}:{dst}";

                    process.StartInfo.Arguments = argsStr;
                    process.Start();

                    // 每个文件最多等待10秒，过期传输失败就删除。
                    process.WaitForExit(10000);
                    if (!process.HasExited)
                        process.Kill();
                }
            }

        }

    }
}
