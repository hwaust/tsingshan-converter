﻿FILNAM/'E:\T.dmo',05.3
$$ PC-DMIS Generated DMIS Output File
$$ PC-DMIS Ver: 2017 R1 
Date=2020/10/09
Time=22:16:46
UNITS/MM,ANGDEC
DA(STARTUP)=DATSET/MCS
WKPLAN/XYPLAN
TEXT/OUTFIL,'TEST_12345'
SA(TIP1)=SNSDEF/OFFSET,0.,0.,191.15,5.
OUTPUT/F(CIR1)
F(CIR1)=FEAT/CIRCLE,INNER,CART,0.,0.,0.,0.,0.,1.,15.
OUTPUT/FA(CIR1)
FA(CIR1)=FEAT/CIRCLE,INNER,CART,0.,0.,0.,0.,0.,1.,15.
OUTPUT/F(PT1)
F(PT1)=FEAT/POINT,CART,10.,10.,10.,0.,0.,1.
OUTPUT/FA(PT1)
FA(PT1)=FEAT/POINT,CART,10.,10.,10.,0.,0.,1.
OUTPUT/F(PT2)
F(PT2)=FEAT/POINT,CART,0.,0.,0.,0.,0.,1.
OUTPUT/FA(PT2)
FA(PT2)=FEAT/POINT,CART,0.,0.,0.,0.,0.,1.
OUTPUT/F(CIR1),T(POS1__X),T(POS1__Y),T(POS1__Z),T(POS1__D)
T(POS1__X)=TOL/CORTOL,XAXIS,-0.05,0.05
T(POS1__Y)=TOL/CORTOL,YAXIS,-0.05,0.05
T(POS1__Z)=TOL/CORTOL,ZAXIS,-0.05,0.05
T(POS1__D)=TOL/DIAM,-0.05,0.05
OUTPUT/FA(CIR1),TA(POS1__X),TA(POS1__Y),TA(POS1__Z),TA(POS1__D)
TA(POS1__X)=TOL/CORTOL,XAXIS,0.,INTOL
TA(POS1__Y)=TOL/CORTOL,YAXIS,0.,INTOL
TA(POS1__Z)=TOL/CORTOL,ZAXIS,0.,INTOL
TA(POS1__D)=TOL/DIAM,0.,INTOL
OUTPUT/F(CIR1),T(POS2__TRUE_DF),T(POS2__TRUE_TP)
T(POS2__TRUE_DF)=TOL/DIAM,-0.05,0.05
T(POS2__TRUE_TP)=TOL/POS,3D,0.01
OUTPUT/FA(CIR1),TA(POS2__TRUE_DF),TA(POS2__TRUE_TP)
TA(POS2__TRUE_DF)=TOL/DIAM,0.,INTOL
TA(POS2__TRUE_TP)=TOL/POS,3D,0.,INTOL
OUTPUT/F(CIR1),F(PT1),T(DIST1)
T(DIST1)=TOL/DISTB,NOMINL,14.1421,-0.01,0.01,PT2PT
OUTPUT/FA(CIR1),FA(PT1),TA(DIST1)
TA(DIST1)=TOL/DISTB,INTOL,NOMINL,14.1421,-0.01,0.01,PT2PT
ENDFIL