﻿namespace QDasConverter.Models
{
	partial class FolderSelector
	{
		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 组件设计器生成的代码

		/// <summary> 
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			groupBox8 = new System.Windows.Forms.GroupBox();
			txtOutputPath = new System.Windows.Forms.TextBox();
			btOutputPath = new System.Windows.Forms.Button();
			groupBox8.SuspendLayout();
			SuspendLayout();
			// 
			// groupBox8
			// 
			groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			groupBox8.Controls.Add(txtOutputPath);
			groupBox8.Controls.Add(btOutputPath);
			groupBox8.Location = new System.Drawing.Point(3, 0);
			groupBox8.Name = "groupBox8";
			groupBox8.Size = new System.Drawing.Size(480, 50);
			groupBox8.TabIndex = 14;
			groupBox8.TabStop = false;
			groupBox8.Text = "输出文件夹 （转换好的文件输出位置）";
			// 
			// txtOutputPath
			// 
			txtOutputPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			txtOutputPath.Location = new System.Drawing.Point(16, 17);
			txtOutputPath.Name = "txtOutputPath";
			txtOutputPath.Size = new System.Drawing.Size(387, 21);
			txtOutputPath.TabIndex = 9;
			txtOutputPath.Text = "D:\\Q-DAS_FILES";
			// 
			// btOutputPath
			// 
			btOutputPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			btOutputPath.Location = new System.Drawing.Point(409, 14);
			btOutputPath.Name = "btOutputPath";
			btOutputPath.Size = new System.Drawing.Size(48, 24);
			btOutputPath.TabIndex = 10;
			btOutputPath.Text = "选择";
			btOutputPath.UseVisualStyleBackColor = true;
			// 
			// FolderSelector
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			Controls.Add(groupBox8);
			Name = "FolderSelector";
			Size = new System.Drawing.Size(486, 52);
			groupBox8.ResumeLayout(false);
			groupBox8.PerformLayout();
			ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.TextBox txtOutputPath;
		private System.Windows.Forms.Button btOutputPath;
	}
}
