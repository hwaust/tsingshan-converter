﻿namespace QDasConverter.Forms
{
	partial class ConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pgGeneral = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.numBatchCount = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.numConvertInterval = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtLastResID = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ckRunAfterStart = new System.Windows.Forms.CheckBox();
            this.ckAutoStart = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ckConfirmClose = new System.Windows.Forms.CheckBox();
            this.pgInput = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbKeepInputFileLocation = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.gbBackupFolders = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ckKeepBackupFolderStruct = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSuccessfulFolder = new System.Windows.Forms.Button();
            this.txtFailedFolder = new System.Windows.Forms.TextBox();
            this.txtSuccessfulFolder = new System.Windows.Forms.TextBox();
            this.btnFailedFolder = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.rbBackup = new System.Windows.Forms.RadioButton();
            this.rbDelete = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ckTraverseSubfolders = new System.Windows.Forms.CheckBox();
            this.pgOutput = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbNoDatetimeAppendix = new System.Windows.Forms.RadioButton();
            this.rbUseDefaultDateTimeAppendix = new System.Windows.Forms.RadioButton();
            this.rbUseCustomizedDateAppendix = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnDateFormatInformation = new System.Windows.Forms.Button();
            this.txtCustomedDateFormatResult = new System.Windows.Forms.TextBox();
            this.txtCustomedDateFormat = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.ckKeepOutFolderStruct = new System.Windows.Forms.CheckBox();
            this.lblKeepFolderStruct = new System.Windows.Forms.Label();
            this.numlblKeepFolderLevel = new System.Windows.Forms.NumericUpDown();
            this.btOutputFolder = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTempFolder = new System.Windows.Forms.TextBox();
            this.btnTempFolder = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbMergeOutputFiles = new System.Windows.Forms.RadioButton();
            this.rbIncrementIndex = new System.Windows.Forms.RadioButton();
            this.rbReplaceOld = new System.Windows.Forms.RadioButton();
            this.rbAddTimeToNew = new System.Windows.Forms.RadioButton();
            this.pgAutoTransducer = new System.Windows.Forms.TabPage();
            this.gbAutoConfig = new System.Windows.Forms.GroupBox();
            this.cbCircleUnit = new System.Windows.Forms.ComboBox();
            this.dpEndTime = new System.Windows.Forms.DateTimePicker();
            this.dpStartTime = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numCircleValue = new System.Windows.Forms.NumericUpDown();
            this.ckAutoTransduce = new System.Windows.Forms.CheckBox();
            this.pgEncoding = new System.Windows.Forms.TabPage();
            this.lbOutputEncoding = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbInputEncoding = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbAllConverters = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.pgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBatchCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numConvertInterval)).BeginInit();
            this.pgInput.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbBackupFolders.SuspendLayout();
            this.pgOutput.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numlblKeepFolderLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pgAutoTransducer.SuspendLayout();
            this.gbAutoConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCircleValue)).BeginInit();
            this.pgEncoding.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.pgGeneral);
            this.tabControl1.Controls.Add(this.pgInput);
            this.tabControl1.Controls.Add(this.pgOutput);
            this.tabControl1.Controls.Add(this.pgAutoTransducer);
            this.tabControl1.Controls.Add(this.pgEncoding);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(16, 16);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1018, 533);
            this.tabControl1.TabIndex = 0;
            // 
            // pgGeneral
            // 
            this.pgGeneral.Controls.Add(this.label26);
            this.pgGeneral.Controls.Add(this.label25);
            this.pgGeneral.Controls.Add(this.label24);
            this.pgGeneral.Controls.Add(this.label23);
            this.pgGeneral.Controls.Add(this.numBatchCount);
            this.pgGeneral.Controls.Add(this.label22);
            this.pgGeneral.Controls.Add(this.label21);
            this.pgGeneral.Controls.Add(this.numConvertInterval);
            this.pgGeneral.Controls.Add(this.label20);
            this.pgGeneral.Controls.Add(this.label19);
            this.pgGeneral.Controls.Add(this.txtLastResID);
            this.pgGeneral.Controls.Add(this.label18);
            this.pgGeneral.Controls.Add(this.label2);
            this.pgGeneral.Controls.Add(this.ckRunAfterStart);
            this.pgGeneral.Controls.Add(this.ckAutoStart);
            this.pgGeneral.Controls.Add(this.label16);
            this.pgGeneral.Controls.Add(this.ckConfirmClose);
            this.pgGeneral.Location = new System.Drawing.Point(4, 26);
            this.pgGeneral.Margin = new System.Windows.Forms.Padding(4);
            this.pgGeneral.Name = "pgGeneral";
            this.pgGeneral.Padding = new System.Windows.Forms.Padding(4);
            this.pgGeneral.Size = new System.Drawing.Size(1010, 503);
            this.pgGeneral.TabIndex = 7;
            this.pgGeneral.Text = "综合";
            this.pgGeneral.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F);
            this.label26.Location = new System.Drawing.Point(71, 398);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(377, 12);
            this.label26.TabIndex = 47;
            this.label26.Text = "在数据库同步时，会按条处理，所以本设置表示每次处理的数据条数。";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("宋体", 9F);
            this.label25.Location = new System.Drawing.Point(151, 279);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(473, 24);
            this.label25.TabIndex = 46;
            this.label25.Text = "为了避免频率读取数据库造成的高负载，所以每次同步后会等待一段时间再去读取数据。\r\n默认值为60，即每分钟同步一次。";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 9F);
            this.label24.Location = new System.Drawing.Point(151, 173);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(377, 12);
            this.label24.TabIndex = 45;
            this.label24.Text = "在数据库同步时，会按条处理，所以本设置表示每次处理的数据条数。";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 12F);
            this.label23.Location = new System.Drawing.Point(335, 141);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(23, 16);
            this.label23.TabIndex = 44;
            this.label23.Text = "条";
            // 
            // numBatchCount
            // 
            this.numBatchCount.Font = new System.Drawing.Font("宋体", 12F);
            this.numBatchCount.Location = new System.Drawing.Point(154, 135);
            this.numBatchCount.Margin = new System.Windows.Forms.Padding(4);
            this.numBatchCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBatchCount.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numBatchCount.Name = "numBatchCount";
            this.numBatchCount.Size = new System.Drawing.Size(173, 26);
            this.numBatchCount.TabIndex = 43;
            this.numBatchCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numBatchCount.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("宋体", 9F);
            this.label22.Location = new System.Drawing.Point(151, 71);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(293, 24);
            this.label22.TabIndex = 42;
            this.label22.Text = "主表中当前开始转换的编号，每次转换后会不断增加。\r\n注意：其数据类型通常为整型。";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 12F);
            this.label21.Location = new System.Drawing.Point(335, 247);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 16);
            this.label21.TabIndex = 41;
            this.label21.Text = "秒";
            // 
            // numConvertInterval
            // 
            this.numConvertInterval.Font = new System.Drawing.Font("宋体", 12F);
            this.numConvertInterval.Location = new System.Drawing.Point(154, 240);
            this.numConvertInterval.Margin = new System.Windows.Forms.Padding(4);
            this.numConvertInterval.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.numConvertInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numConvertInterval.Name = "numConvertInterval";
            this.numConvertInterval.Size = new System.Drawing.Size(173, 26);
            this.numConvertInterval.TabIndex = 40;
            this.numConvertInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numConvertInterval.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("宋体", 12F);
            this.label20.Location = new System.Drawing.Point(38, 247);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(103, 16);
            this.label20.TabIndex = 38;
            this.label20.Text = "转换时间间隔";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 12F);
            this.label19.Location = new System.Drawing.Point(38, 141);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(103, 16);
            this.label19.TabIndex = 36;
            this.label19.Text = "每批转换数据";
            // 
            // txtLastResID
            // 
            this.txtLastResID.Font = new System.Drawing.Font("宋体", 12F);
            this.txtLastResID.Location = new System.Drawing.Point(150, 32);
            this.txtLastResID.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastResID.Name = "txtLastResID";
            this.txtLastResID.Size = new System.Drawing.Size(172, 26);
            this.txtLastResID.TabIndex = 35;
            this.txtLastResID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 12F);
            this.label18.Location = new System.Drawing.Point(70, 35);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 16);
            this.label18.TabIndex = 34;
            this.label18.Text = "起始编号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 8F);
            this.label2.Location = new System.Drawing.Point(53, 553);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 11);
            this.label2.TabIndex = 33;
            this.label2.Text = "开启此选项后，程序启动后马上开始转换所有输入项。";
            this.label2.Visible = false;
            // 
            // ckRunAfterStart
            // 
            this.ckRunAfterStart.AutoSize = true;
            this.ckRunAfterStart.Location = new System.Drawing.Point(31, 528);
            this.ckRunAfterStart.Margin = new System.Windows.Forms.Padding(4);
            this.ckRunAfterStart.Name = "ckRunAfterStart";
            this.ckRunAfterStart.Size = new System.Drawing.Size(170, 20);
            this.ckRunAfterStart.TabIndex = 30;
            this.ckRunAfterStart.Text = "启动后立即开始转换";
            this.ckRunAfterStart.UseVisualStyleBackColor = true;
            this.ckRunAfterStart.Visible = false;
            // 
            // ckAutoStart
            // 
            this.ckAutoStart.AutoSize = true;
            this.ckAutoStart.Location = new System.Drawing.Point(31, 599);
            this.ckAutoStart.Margin = new System.Windows.Forms.Padding(4);
            this.ckAutoStart.Name = "ckAutoStart";
            this.ckAutoStart.Size = new System.Drawing.Size(138, 20);
            this.ckAutoStart.TabIndex = 28;
            this.ckAutoStart.Text = "开机时自动启动";
            this.ckAutoStart.UseVisualStyleBackColor = true;
            this.ckAutoStart.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 8F);
            this.label16.Location = new System.Drawing.Point(53, 624);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(313, 11);
            this.label16.TabIndex = 29;
            this.label16.Text = "注意：此项改动会影响注册表，从而可能会引起安全软件报警。";
            this.label16.Visible = false;
            // 
            // ckConfirmClose
            // 
            this.ckConfirmClose.AutoSize = true;
            this.ckConfirmClose.Location = new System.Drawing.Point(41, 364);
            this.ckConfirmClose.Margin = new System.Windows.Forms.Padding(4);
            this.ckConfirmClose.Name = "ckConfirmClose";
            this.ckConfirmClose.Size = new System.Drawing.Size(170, 20);
            this.ckConfirmClose.TabIndex = 27;
            this.ckConfirmClose.Text = "关闭窗体时进行确认";
            this.ckConfirmClose.UseVisualStyleBackColor = true;
            // 
            // pgInput
            // 
            this.pgInput.Controls.Add(this.label15);
            this.pgInput.Controls.Add(this.groupBox4);
            this.pgInput.Controls.Add(this.label1);
            this.pgInput.Controls.Add(this.ckTraverseSubfolders);
            this.pgInput.Location = new System.Drawing.Point(4, 26);
            this.pgInput.Margin = new System.Windows.Forms.Padding(4);
            this.pgInput.Name = "pgInput";
            this.pgInput.Padding = new System.Windows.Forms.Padding(4);
            this.pgInput.Size = new System.Drawing.Size(1010, 503);
            this.pgInput.TabIndex = 1;
            this.pgInput.Text = "输入选项";
            this.pgInput.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Location = new System.Drawing.Point(13, 372);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(979, 3);
            this.label15.TabIndex = 47;
            this.label15.Text = "label15";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rbKeepInputFileLocation);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.gbBackupFolders);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.rbBackup);
            this.groupBox4.Controls.Add(this.rbDelete);
            this.groupBox4.Location = new System.Drawing.Point(15, 21);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(976, 339);
            this.groupBox4.TabIndex = 46;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "转换后原输入文件处理方式";
            // 
            // rbKeepInputFileLocation
            // 
            this.rbKeepInputFileLocation.AutoSize = true;
            this.rbKeepInputFileLocation.Location = new System.Drawing.Point(29, 27);
            this.rbKeepInputFileLocation.Margin = new System.Windows.Forms.Padding(4);
            this.rbKeepInputFileLocation.Name = "rbKeepInputFileLocation";
            this.rbKeepInputFileLocation.Size = new System.Drawing.Size(185, 20);
            this.rbKeepInputFileLocation.TabIndex = 41;
            this.rbKeepInputFileLocation.Text = "保持输入文件位置不变";
            this.rbKeepInputFileLocation.UseVisualStyleBackColor = true;
            this.rbKeepInputFileLocation.CheckedChanged += new System.EventHandler(this.rbNoChange_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(53, 300);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(401, 12);
            this.label9.TabIndex = 45;
            this.label9.Text = "转换后直接删除原文件。主要用于一些程序产生的重要性不高的过程文件。";
            // 
            // gbBackupFolders
            // 
            this.gbBackupFolders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBackupFolders.Controls.Add(this.label14);
            this.gbBackupFolders.Controls.Add(this.ckKeepBackupFolderStruct);
            this.gbBackupFolders.Controls.Add(this.label4);
            this.gbBackupFolders.Controls.Add(this.label3);
            this.gbBackupFolders.Controls.Add(this.btnSuccessfulFolder);
            this.gbBackupFolders.Controls.Add(this.txtFailedFolder);
            this.gbBackupFolders.Controls.Add(this.txtSuccessfulFolder);
            this.gbBackupFolders.Controls.Add(this.btnFailedFolder);
            this.gbBackupFolders.Enabled = false;
            this.gbBackupFolders.Location = new System.Drawing.Point(56, 109);
            this.gbBackupFolders.Margin = new System.Windows.Forms.Padding(4);
            this.gbBackupFolders.Name = "gbBackupFolders";
            this.gbBackupFolders.Padding = new System.Windows.Forms.Padding(4);
            this.gbBackupFolders.Size = new System.Drawing.Size(905, 156);
            this.gbBackupFolders.TabIndex = 39;
            this.gbBackupFolders.TabStop = false;
            this.gbBackupFolders.Text = "移动输入文件至以下文件夹";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F);
            this.label14.Location = new System.Drawing.Point(47, 184);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(281, 12);
            this.label14.TabIndex = 50;
            this.label14.Text = "输出文件将根据输入目录的路径结构进行分类保存。";
            // 
            // ckKeepBackupFolderStruct
            // 
            this.ckKeepBackupFolderStruct.AutoSize = true;
            this.ckKeepBackupFolderStruct.Location = new System.Drawing.Point(21, 156);
            this.ckKeepBackupFolderStruct.Margin = new System.Windows.Forms.Padding(4);
            this.ckKeepBackupFolderStruct.Name = "ckKeepBackupFolderStruct";
            this.ckKeepBackupFolderStruct.Size = new System.Drawing.Size(138, 20);
            this.ckKeepBackupFolderStruct.TabIndex = 47;
            this.ckKeepBackupFolderStruct.Text = "保留原目录结构";
            this.ckKeepBackupFolderStruct.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 28);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(319, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "成功文件夹 （用于保存转换成功的文件夹）";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 92);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(319, 16);
            this.label3.TabIndex = 22;
            this.label3.Text = "失败文件夹 （用于保存转换失败的文件夹）";
            // 
            // btnSuccessfulFolder
            // 
            this.btnSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuccessfulFolder.Location = new System.Drawing.Point(801, 51);
            this.btnSuccessfulFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnSuccessfulFolder.Name = "btnSuccessfulFolder";
            this.btnSuccessfulFolder.Size = new System.Drawing.Size(81, 28);
            this.btnSuccessfulFolder.TabIndex = 18;
            this.btnSuccessfulFolder.Text = "选择...";
            this.btnSuccessfulFolder.UseVisualStyleBackColor = true;
            this.btnSuccessfulFolder.Click += new System.EventHandler(this.btnSuccessfulFolder_Click);
            // 
            // txtFailedFolder
            // 
            this.txtFailedFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFailedFolder.Location = new System.Drawing.Point(21, 112);
            this.txtFailedFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtFailedFolder.Name = "txtFailedFolder";
            this.txtFailedFolder.Size = new System.Drawing.Size(771, 26);
            this.txtFailedFolder.TabIndex = 20;
            this.txtFailedFolder.Text = "D:\\QDAS\\Backups\\Failed";
            // 
            // txtSuccessfulFolder
            // 
            this.txtSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSuccessfulFolder.Location = new System.Drawing.Point(21, 48);
            this.txtSuccessfulFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtSuccessfulFolder.Name = "txtSuccessfulFolder";
            this.txtSuccessfulFolder.Size = new System.Drawing.Size(771, 26);
            this.txtSuccessfulFolder.TabIndex = 17;
            this.txtSuccessfulFolder.Text = "D:\\QDAS\\Backups\\Success";
            // 
            // btnFailedFolder
            // 
            this.btnFailedFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFailedFolder.Location = new System.Drawing.Point(801, 111);
            this.btnFailedFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnFailedFolder.Name = "btnFailedFolder";
            this.btnFailedFolder.Size = new System.Drawing.Size(81, 28);
            this.btnFailedFolder.TabIndex = 21;
            this.btnFailedFolder.Text = "选择...";
            this.btnFailedFolder.UseVisualStyleBackColor = true;
            this.btnFailedFolder.Click += new System.EventHandler(this.btnFailedFolder_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F);
            this.label7.Location = new System.Drawing.Point(55, 53);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(281, 12);
            this.label7.TabIndex = 44;
            this.label7.Text = "不对原文件进行处理。此选项主要用于增量转换器。";
            // 
            // rbBackup
            // 
            this.rbBackup.AutoSize = true;
            this.rbBackup.Checked = true;
            this.rbBackup.Location = new System.Drawing.Point(29, 80);
            this.rbBackup.Margin = new System.Windows.Forms.Padding(4);
            this.rbBackup.Name = "rbBackup";
            this.rbBackup.Size = new System.Drawing.Size(153, 20);
            this.rbBackup.TabIndex = 42;
            this.rbBackup.TabStop = true;
            this.rbBackup.Text = "移动至指定文件夹";
            this.rbBackup.UseVisualStyleBackColor = true;
            this.rbBackup.CheckedChanged += new System.EventHandler(this.rbBackup_CheckedChanged);
            // 
            // rbDelete
            // 
            this.rbDelete.AutoSize = true;
            this.rbDelete.Location = new System.Drawing.Point(29, 273);
            this.rbDelete.Margin = new System.Windows.Forms.Padding(4);
            this.rbDelete.Name = "rbDelete";
            this.rbDelete.Size = new System.Drawing.Size(89, 20);
            this.rbDelete.TabIndex = 43;
            this.rbDelete.Text = "直接删除";
            this.rbDelete.UseVisualStyleBackColor = true;
            this.rbDelete.CheckedChanged += new System.EventHandler(this.rbDelete_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 8F);
            this.label1.Location = new System.Drawing.Point(44, 413);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(357, 11);
            this.label1.TabIndex = 32;
            this.label1.Text = "开启此选项后，如果输入目录下有子目录，子目录的数据也会得到转换。";
            this.label1.Visible = false;
            // 
            // ckTraverseSubfolders
            // 
            this.ckTraverseSubfolders.AutoSize = true;
            this.ckTraverseSubfolders.Location = new System.Drawing.Point(15, 388);
            this.ckTraverseSubfolders.Margin = new System.Windows.Forms.Padding(4);
            this.ckTraverseSubfolders.Name = "ckTraverseSubfolders";
            this.ckTraverseSubfolders.Size = new System.Drawing.Size(186, 20);
            this.ckTraverseSubfolders.TabIndex = 31;
            this.ckTraverseSubfolders.Text = "转换目录时遍历子目录";
            this.ckTraverseSubfolders.UseVisualStyleBackColor = true;
            this.ckTraverseSubfolders.Visible = false;
            // 
            // pgOutput
            // 
            this.pgOutput.Controls.Add(this.groupBox5);
            this.pgOutput.Controls.Add(this.groupBox3);
            this.pgOutput.Controls.Add(this.groupBox1);
            this.pgOutput.Controls.Add(this.groupBox2);
            this.pgOutput.Location = new System.Drawing.Point(4, 26);
            this.pgOutput.Margin = new System.Windows.Forms.Padding(4);
            this.pgOutput.Name = "pgOutput";
            this.pgOutput.Padding = new System.Windows.Forms.Padding(4);
            this.pgOutput.Size = new System.Drawing.Size(1010, 503);
            this.pgOutput.TabIndex = 0;
            this.pgOutput.Text = "输出选项";
            this.pgOutput.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbNoDatetimeAppendix);
            this.groupBox5.Controls.Add(this.rbUseDefaultDateTimeAppendix);
            this.groupBox5.Controls.Add(this.rbUseCustomizedDateAppendix);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Location = new System.Drawing.Point(16, 419);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(964, 247);
            this.groupBox5.TabIndex = 59;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "在输出文件名中追加日期时间";
            this.groupBox5.Visible = false;
            // 
            // rbNoDatetimeAppendix
            // 
            this.rbNoDatetimeAppendix.AutoSize = true;
            this.rbNoDatetimeAppendix.Checked = true;
            this.rbNoDatetimeAppendix.Location = new System.Drawing.Point(25, 33);
            this.rbNoDatetimeAppendix.Margin = new System.Windows.Forms.Padding(4);
            this.rbNoDatetimeAppendix.Name = "rbNoDatetimeAppendix";
            this.rbNoDatetimeAppendix.Size = new System.Drawing.Size(121, 20);
            this.rbNoDatetimeAppendix.TabIndex = 62;
            this.rbNoDatetimeAppendix.TabStop = true;
            this.rbNoDatetimeAppendix.Text = "不添加时间戳";
            this.rbNoDatetimeAppendix.UseVisualStyleBackColor = true;
            this.rbNoDatetimeAppendix.CheckedChanged += new System.EventHandler(this.RbNoDatetimeAppendix_CheckedChanged);
            // 
            // rbUseDefaultDateTimeAppendix
            // 
            this.rbUseDefaultDateTimeAppendix.AutoSize = true;
            this.rbUseDefaultDateTimeAppendix.Location = new System.Drawing.Point(25, 67);
            this.rbUseDefaultDateTimeAppendix.Margin = new System.Windows.Forms.Padding(4);
            this.rbUseDefaultDateTimeAppendix.Name = "rbUseDefaultDateTimeAppendix";
            this.rbUseDefaultDateTimeAppendix.Size = new System.Drawing.Size(545, 20);
            this.rbUseDefaultDateTimeAppendix.TabIndex = 60;
            this.rbUseDefaultDateTimeAppendix.Text = "添加默认时间时间戳，格式：output.dfq -> output_20190412091522.dfq";
            this.rbUseDefaultDateTimeAppendix.UseVisualStyleBackColor = true;
            this.rbUseDefaultDateTimeAppendix.CheckedChanged += new System.EventHandler(this.RbUseDefaultDateTimeFormat_CheckedChanged);
            // 
            // rbUseCustomizedDateAppendix
            // 
            this.rbUseCustomizedDateAppendix.AutoSize = true;
            this.rbUseCustomizedDateAppendix.Location = new System.Drawing.Point(25, 100);
            this.rbUseCustomizedDateAppendix.Margin = new System.Windows.Forms.Padding(4);
            this.rbUseCustomizedDateAppendix.Name = "rbUseCustomizedDateAppendix";
            this.rbUseCustomizedDateAppendix.Size = new System.Drawing.Size(153, 20);
            this.rbUseCustomizedDateAppendix.TabIndex = 61;
            this.rbUseCustomizedDateAppendix.Text = "添加自定义时间戳";
            this.rbUseCustomizedDateAppendix.UseVisualStyleBackColor = true;
            this.rbUseCustomizedDateAppendix.CheckedChanged += new System.EventHandler(this.RbUseCustomizedDateFormat_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.btnDateFormatInformation);
            this.groupBox6.Controls.Add(this.txtCustomedDateFormatResult);
            this.groupBox6.Controls.Add(this.txtCustomedDateFormat);
            this.groupBox6.Enabled = false;
            this.groupBox6.Location = new System.Drawing.Point(59, 127);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(905, 100);
            this.groupBox6.TabIndex = 59;
            this.groupBox6.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 65);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 16);
            this.label11.TabIndex = 56;
            this.label11.Text = "实际效果";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 29);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 55;
            this.label10.Text = "输入格式";
            // 
            // btnDateFormatInformation
            // 
            this.btnDateFormatInformation.Location = new System.Drawing.Point(769, 19);
            this.btnDateFormatInformation.Margin = new System.Windows.Forms.Padding(4);
            this.btnDateFormatInformation.Name = "btnDateFormatInformation";
            this.btnDateFormatInformation.Size = new System.Drawing.Size(128, 65);
            this.btnDateFormatInformation.TabIndex = 58;
            this.btnDateFormatInformation.Text = "格式说明\r\n";
            this.btnDateFormatInformation.UseVisualStyleBackColor = true;
            this.btnDateFormatInformation.Click += new System.EventHandler(this.BtnDateFormatInformation_Click);
            // 
            // txtCustomedDateFormatResult
            // 
            this.txtCustomedDateFormatResult.Location = new System.Drawing.Point(87, 60);
            this.txtCustomedDateFormatResult.Margin = new System.Windows.Forms.Padding(4);
            this.txtCustomedDateFormatResult.Name = "txtCustomedDateFormatResult";
            this.txtCustomedDateFormatResult.ReadOnly = true;
            this.txtCustomedDateFormatResult.Size = new System.Drawing.Size(405, 26);
            this.txtCustomedDateFormatResult.TabIndex = 57;
            this.txtCustomedDateFormatResult.Text = "output.dfq -> output_2019_04_12_10_20_14_453.dfq";
            // 
            // txtCustomedDateFormat
            // 
            this.txtCustomedDateFormat.Location = new System.Drawing.Point(87, 24);
            this.txtCustomedDateFormat.Margin = new System.Windows.Forms.Padding(4);
            this.txtCustomedDateFormat.Name = "txtCustomedDateFormat";
            this.txtCustomedDateFormat.Size = new System.Drawing.Size(405, 26);
            this.txtCustomedDateFormat.TabIndex = 54;
            this.txtCustomedDateFormat.Text = "yyyy_MM_dd_HH_mm_ss_fff";
            this.txtCustomedDateFormat.TextChanged += new System.EventHandler(this.TxtCustomedDateFormat_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtOutputFolder);
            this.groupBox3.Controls.Add(this.ckKeepOutFolderStruct);
            this.groupBox3.Controls.Add(this.lblKeepFolderStruct);
            this.groupBox3.Controls.Add(this.numlblKeepFolderLevel);
            this.groupBox3.Controls.Add(this.btOutputFolder);
            this.groupBox3.Location = new System.Drawing.Point(16, 23);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(969, 117);
            this.groupBox3.TabIndex = 51;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "输出文件夹 （转换好的文件输出位置）";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 8F);
            this.label13.Location = new System.Drawing.Point(48, 93);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(258, 11);
            this.label13.TabIndex = 49;
            this.label13.Text = "输出文件将根据输入目录的路径结构进行分类保存。";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputFolder.Location = new System.Drawing.Point(25, 24);
            this.txtOutputFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(845, 26);
            this.txtOutputFolder.TabIndex = 11;
            this.txtOutputFolder.Text = "D:\\QDAS\\Output";
            // 
            // ckKeepOutFolderStruct
            // 
            this.ckKeepOutFolderStruct.AutoSize = true;
            this.ckKeepOutFolderStruct.Location = new System.Drawing.Point(25, 64);
            this.ckKeepOutFolderStruct.Margin = new System.Windows.Forms.Padding(4);
            this.ckKeepOutFolderStruct.Name = "ckKeepOutFolderStruct";
            this.ckKeepOutFolderStruct.Size = new System.Drawing.Size(138, 20);
            this.ckKeepOutFolderStruct.TabIndex = 46;
            this.ckKeepOutFolderStruct.Text = "保留原目录结构";
            this.ckKeepOutFolderStruct.UseVisualStyleBackColor = true;
            this.ckKeepOutFolderStruct.CheckedChanged += new System.EventHandler(this.CkKeepOutFolderStruct_CheckedChanged);
            this.ckKeepOutFolderStruct.Click += new System.EventHandler(this.ckKeepFolderStruct_Click);
            // 
            // lblKeepFolderStruct
            // 
            this.lblKeepFolderStruct.AutoSize = true;
            this.lblKeepFolderStruct.Location = new System.Drawing.Point(189, 65);
            this.lblKeepFolderStruct.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKeepFolderStruct.Name = "lblKeepFolderStruct";
            this.lblKeepFolderStruct.Size = new System.Drawing.Size(71, 16);
            this.lblKeepFolderStruct.TabIndex = 47;
            this.lblKeepFolderStruct.Text = "保持级数";
            // 
            // numlblKeepFolderLevel
            // 
            this.numlblKeepFolderLevel.Enabled = false;
            this.numlblKeepFolderLevel.Location = new System.Drawing.Point(268, 61);
            this.numlblKeepFolderLevel.Margin = new System.Windows.Forms.Padding(4);
            this.numlblKeepFolderLevel.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numlblKeepFolderLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numlblKeepFolderLevel.Name = "numlblKeepFolderLevel";
            this.numlblKeepFolderLevel.Size = new System.Drawing.Size(87, 26);
            this.numlblKeepFolderLevel.TabIndex = 48;
            this.numlblKeepFolderLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numlblKeepFolderLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numlblKeepFolderLevel.ValueChanged += new System.EventHandler(this.NumlblKeepFolderLevel_ValueChanged);
            // 
            // btOutputFolder
            // 
            this.btOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btOutputFolder.Location = new System.Drawing.Point(880, 24);
            this.btOutputFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btOutputFolder.Name = "btOutputFolder";
            this.btOutputFolder.Size = new System.Drawing.Size(81, 28);
            this.btOutputFolder.TabIndex = 12;
            this.btOutputFolder.Text = "选择...";
            this.btOutputFolder.UseVisualStyleBackColor = true;
            this.btOutputFolder.Click += new System.EventHandler(this.btOutputFolder_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtTempFolder);
            this.groupBox1.Controls.Add(this.btnTempFolder);
            this.groupBox1.Location = new System.Drawing.Point(16, 148);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(969, 72);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "临时文件夹 （用于保存转换时的临时数据）";
            // 
            // txtTempFolder
            // 
            this.txtTempFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTempFolder.Location = new System.Drawing.Point(25, 28);
            this.txtTempFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtTempFolder.Name = "txtTempFolder";
            this.txtTempFolder.Size = new System.Drawing.Size(845, 26);
            this.txtTempFolder.TabIndex = 14;
            this.txtTempFolder.Text = "D:\\QDAS\\Temp";
            // 
            // btnTempFolder
            // 
            this.btnTempFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTempFolder.Location = new System.Drawing.Point(880, 27);
            this.btnTempFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnTempFolder.Name = "btnTempFolder";
            this.btnTempFolder.Size = new System.Drawing.Size(81, 28);
            this.btnTempFolder.TabIndex = 15;
            this.btnTempFolder.Text = "选择...";
            this.btnTempFolder.UseVisualStyleBackColor = true;
            this.btnTempFolder.Click += new System.EventHandler(this.btnTempFolder_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.rbMergeOutputFiles);
            this.groupBox2.Controls.Add(this.rbIncrementIndex);
            this.groupBox2.Controls.Add(this.rbReplaceOld);
            this.groupBox2.Controls.Add(this.rbAddTimeToNew);
            this.groupBox2.Location = new System.Drawing.Point(16, 228);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(969, 183);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "存在同名文件的处理方式";
            this.groupBox2.Visible = false;
            // 
            // rbMergeOutputFiles
            // 
            this.rbMergeOutputFiles.AutoSize = true;
            this.rbMergeOutputFiles.Location = new System.Drawing.Point(31, 147);
            this.rbMergeOutputFiles.Margin = new System.Windows.Forms.Padding(4);
            this.rbMergeOutputFiles.Name = "rbMergeOutputFiles";
            this.rbMergeOutputFiles.Size = new System.Drawing.Size(361, 20);
            this.rbMergeOutputFiles.TabIndex = 5;
            this.rbMergeOutputFiles.Text = "合并文件内容 (此功能只针对部分转换器可用）";
            this.rbMergeOutputFiles.UseVisualStyleBackColor = true;
            // 
            // rbIncrementIndex
            // 
            this.rbIncrementIndex.AutoSize = true;
            this.rbIncrementIndex.Location = new System.Drawing.Point(32, 111);
            this.rbIncrementIndex.Margin = new System.Windows.Forms.Padding(4);
            this.rbIncrementIndex.Name = "rbIncrementIndex";
            this.rbIncrementIndex.Size = new System.Drawing.Size(377, 20);
            this.rbIncrementIndex.TabIndex = 4;
            this.rbIncrementIndex.Text = "加增量编号，如: output.dfq -> output_001.dfq";
            this.rbIncrementIndex.UseVisualStyleBackColor = true;
            // 
            // rbReplaceOld
            // 
            this.rbReplaceOld.AutoSize = true;
            this.rbReplaceOld.Checked = true;
            this.rbReplaceOld.Location = new System.Drawing.Point(32, 32);
            this.rbReplaceOld.Margin = new System.Windows.Forms.Padding(4);
            this.rbReplaceOld.Name = "rbReplaceOld";
            this.rbReplaceOld.Size = new System.Drawing.Size(305, 20);
            this.rbReplaceOld.TabIndex = 0;
            this.rbReplaceOld.TabStop = true;
            this.rbReplaceOld.Text = "直接覆盖，用新生成的文件替换原文件.";
            this.rbReplaceOld.UseVisualStyleBackColor = true;
            // 
            // rbAddTimeToNew
            // 
            this.rbAddTimeToNew.AutoSize = true;
            this.rbAddTimeToNew.Location = new System.Drawing.Point(32, 72);
            this.rbAddTimeToNew.Margin = new System.Windows.Forms.Padding(4);
            this.rbAddTimeToNew.Name = "rbAddTimeToNew";
            this.rbAddTimeToNew.Size = new System.Drawing.Size(369, 20);
            this.rbAddTimeToNew.TabIndex = 1;
            this.rbAddTimeToNew.Text = "加时间戳，如: 1.dfq -> 1_20120516183025.dfq";
            this.rbAddTimeToNew.UseVisualStyleBackColor = true;
            // 
            // pgAutoTransducer
            // 
            this.pgAutoTransducer.Controls.Add(this.gbAutoConfig);
            this.pgAutoTransducer.Controls.Add(this.ckAutoTransduce);
            this.pgAutoTransducer.Location = new System.Drawing.Point(4, 26);
            this.pgAutoTransducer.Margin = new System.Windows.Forms.Padding(4);
            this.pgAutoTransducer.Name = "pgAutoTransducer";
            this.pgAutoTransducer.Padding = new System.Windows.Forms.Padding(4);
            this.pgAutoTransducer.Size = new System.Drawing.Size(1010, 503);
            this.pgAutoTransducer.TabIndex = 5;
            this.pgAutoTransducer.Text = "自动转换";
            this.pgAutoTransducer.UseVisualStyleBackColor = true;
            // 
            // gbAutoConfig
            // 
            this.gbAutoConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAutoConfig.Controls.Add(this.cbCircleUnit);
            this.gbAutoConfig.Controls.Add(this.dpEndTime);
            this.gbAutoConfig.Controls.Add(this.dpStartTime);
            this.gbAutoConfig.Controls.Add(this.label5);
            this.gbAutoConfig.Controls.Add(this.label6);
            this.gbAutoConfig.Controls.Add(this.label17);
            this.gbAutoConfig.Controls.Add(this.numCircleValue);
            this.gbAutoConfig.Enabled = false;
            this.gbAutoConfig.Location = new System.Drawing.Point(48, 48);
            this.gbAutoConfig.Margin = new System.Windows.Forms.Padding(4);
            this.gbAutoConfig.Name = "gbAutoConfig";
            this.gbAutoConfig.Padding = new System.Windows.Forms.Padding(4);
            this.gbAutoConfig.Size = new System.Drawing.Size(942, 147);
            this.gbAutoConfig.TabIndex = 27;
            this.gbAutoConfig.TabStop = false;
            this.gbAutoConfig.Text = "自动转换配置项";
            // 
            // cbCircleUnit
            // 
            this.cbCircleUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCircleUnit.FormattingEnabled = true;
            this.cbCircleUnit.Items.AddRange(new object[] {
            "秒",
            "分钟",
            "小时"});
            this.cbCircleUnit.Location = new System.Drawing.Point(169, 35);
            this.cbCircleUnit.Margin = new System.Windows.Forms.Padding(4);
            this.cbCircleUnit.Name = "cbCircleUnit";
            this.cbCircleUnit.Size = new System.Drawing.Size(121, 24);
            this.cbCircleUnit.TabIndex = 35;
            // 
            // dpEndTime
            // 
            this.dpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dpEndTime.Location = new System.Drawing.Point(328, 77);
            this.dpEndTime.Margin = new System.Windows.Forms.Padding(4);
            this.dpEndTime.Name = "dpEndTime";
            this.dpEndTime.ShowUpDown = true;
            this.dpEndTime.Size = new System.Drawing.Size(101, 26);
            this.dpEndTime.TabIndex = 33;
            this.dpEndTime.Value = new System.DateTime(2012, 6, 12, 23, 59, 59, 0);
            // 
            // dpStartTime
            // 
            this.dpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dpStartTime.Location = new System.Drawing.Point(88, 77);
            this.dpStartTime.Margin = new System.Windows.Forms.Padding(4);
            this.dpStartTime.Name = "dpStartTime";
            this.dpStartTime.ShowUpDown = true;
            this.dpStartTime.Size = new System.Drawing.Size(124, 26);
            this.dpStartTime.TabIndex = 32;
            this.dpStartTime.Value = new System.DateTime(2012, 6, 12, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(249, 85);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 27;
            this.label5.Text = "结束时间";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 83);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "开始时间";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 37);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 16);
            this.label17.TabIndex = 24;
            this.label17.Text = "监控周期";
            // 
            // numCircleValue
            // 
            this.numCircleValue.Location = new System.Drawing.Point(88, 33);
            this.numCircleValue.Margin = new System.Windows.Forms.Padding(4);
            this.numCircleValue.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numCircleValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCircleValue.Name = "numCircleValue";
            this.numCircleValue.Size = new System.Drawing.Size(75, 26);
            this.numCircleValue.TabIndex = 23;
            this.numCircleValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numCircleValue.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // ckAutoTransduce
            // 
            this.ckAutoTransduce.AutoSize = true;
            this.ckAutoTransduce.Location = new System.Drawing.Point(21, 21);
            this.ckAutoTransduce.Margin = new System.Windows.Forms.Padding(4);
            this.ckAutoTransduce.Name = "ckAutoTransduce";
            this.ckAutoTransduce.Size = new System.Drawing.Size(154, 20);
            this.ckAutoTransduce.TabIndex = 26;
            this.ckAutoTransduce.Text = "是否开启自动转换";
            this.ckAutoTransduce.UseVisualStyleBackColor = true;
            this.ckAutoTransduce.Click += new System.EventHandler(this.ckAutoTransfer_Click);
            // 
            // pgEncoding
            // 
            this.pgEncoding.Controls.Add(this.lbOutputEncoding);
            this.pgEncoding.Controls.Add(this.label8);
            this.pgEncoding.Controls.Add(this.lbInputEncoding);
            this.pgEncoding.Controls.Add(this.label12);
            this.pgEncoding.Location = new System.Drawing.Point(4, 26);
            this.pgEncoding.Name = "pgEncoding";
            this.pgEncoding.Padding = new System.Windows.Forms.Padding(3);
            this.pgEncoding.Size = new System.Drawing.Size(1010, 503);
            this.pgEncoding.TabIndex = 8;
            this.pgEncoding.Text = "编码";
            this.pgEncoding.UseVisualStyleBackColor = true;
            // 
            // lbOutputEncoding
            // 
            this.lbOutputEncoding.FormattingEnabled = true;
            this.lbOutputEncoding.ItemHeight = 16;
            this.lbOutputEncoding.Items.AddRange(new object[] {
            "Default",
            "BIG5",
            "Unicode",
            "UTF-8",
            "UTF-16"});
            this.lbOutputEncoding.Location = new System.Drawing.Point(499, 47);
            this.lbOutputEncoding.Name = "lbOutputEncoding";
            this.lbOutputEncoding.Size = new System.Drawing.Size(366, 132);
            this.lbOutputEncoding.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(496, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "输出文件编码";
            // 
            // lbInputEncoding
            // 
            this.lbInputEncoding.FormattingEnabled = true;
            this.lbInputEncoding.ItemHeight = 16;
            this.lbInputEncoding.Items.AddRange(new object[] {
            "Default",
            "BIG5",
            "Unicode",
            "UTF-8",
            "UTF-16"});
            this.lbInputEncoding.Location = new System.Drawing.Point(20, 47);
            this.lbInputEncoding.Name = "lbInputEncoding";
            this.lbInputEncoding.Size = new System.Drawing.Size(366, 132);
            this.lbInputEncoding.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "输入数据编码";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbAllConverters);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1010, 503);
            this.tabPage1.TabIndex = 9;
            this.tabPage1.Text = "转换器模块选择";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cbAllConverters
            // 
            this.cbAllConverters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAllConverters.FormattingEnabled = true;
            this.cbAllConverters.Location = new System.Drawing.Point(111, 43);
            this.cbAllConverters.Name = "cbAllConverters";
            this.cbAllConverters.Size = new System.Drawing.Size(458, 24);
            this.cbAllConverters.TabIndex = 1;
            this.cbAllConverters.SelectedIndexChanged += new System.EventHandler(this.cbAllConverters_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(34, 46);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 16);
            this.label27.TabIndex = 0;
            this.label27.Text = "模块名称";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(730, 557);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(141, 45);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(880, 557);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(141, 45);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 618);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("宋体", 12F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "综合设置";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.pgGeneral.ResumeLayout(false);
            this.pgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBatchCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numConvertInterval)).EndInit();
            this.pgInput.ResumeLayout(false);
            this.pgInput.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbBackupFolders.ResumeLayout(false);
            this.gbBackupFolders.PerformLayout();
            this.pgOutput.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numlblKeepFolderLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pgAutoTransducer.ResumeLayout(false);
            this.pgAutoTransducer.PerformLayout();
            this.gbAutoConfig.ResumeLayout(false);
            this.gbAutoConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCircleValue)).EndInit();
            this.pgEncoding.ResumeLayout(false);
            this.pgEncoding.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage pgOutput;
		private System.Windows.Forms.TabPage pgInput;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtOutputFolder;
		private System.Windows.Forms.Button btOutputFolder;
		private System.Windows.Forms.TextBox txtTempFolder;
		private System.Windows.Forms.Button btnTempFolder;
		private System.Windows.Forms.TabPage pgAutoTransducer;
		private System.Windows.Forms.GroupBox gbAutoConfig;
		private System.Windows.Forms.ComboBox cbCircleUnit;
		private System.Windows.Forms.DateTimePicker dpEndTime;
		private System.Windows.Forms.DateTimePicker dpStartTime;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.NumericUpDown numCircleValue;
		private System.Windows.Forms.CheckBox ckAutoTransduce;
		private System.Windows.Forms.GroupBox gbBackupFolders;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnSuccessfulFolder;
		private System.Windows.Forms.TextBox txtFailedFolder;
		private System.Windows.Forms.TextBox txtSuccessfulFolder;
		private System.Windows.Forms.Button btnFailedFolder;
		private System.Windows.Forms.TabPage pgGeneral;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rbReplaceOld;
		private System.Windows.Forms.RadioButton rbAddTimeToNew;
		private System.Windows.Forms.NumericUpDown numlblKeepFolderLevel;
		private System.Windows.Forms.Label lblKeepFolderStruct;
		private System.Windows.Forms.CheckBox ckKeepOutFolderStruct;
		private System.Windows.Forms.CheckBox ckRunAfterStart;
		private System.Windows.Forms.CheckBox ckAutoStart;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.CheckBox ckConfirmClose;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.RadioButton rbDelete;
		private System.Windows.Forms.RadioButton rbBackup;
		private System.Windows.Forms.RadioButton rbKeepInputFileLocation;
		private System.Windows.Forms.RadioButton rbIncrementIndex;
		private System.Windows.Forms.CheckBox ckTraverseSubfolders;
		private System.Windows.Forms.TabPage pgEncoding;
		private System.Windows.Forms.ListBox lbInputEncoding;
		private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox ckKeepBackupFolderStruct;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ListBox lbOutputEncoding;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCustomedDateFormatResult;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCustomedDateFormat;
        private System.Windows.Forms.Button btnDateFormatInformation;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbUseDefaultDateTimeAppendix;
        private System.Windows.Forms.RadioButton rbUseCustomizedDateAppendix;
        private System.Windows.Forms.RadioButton rbNoDatetimeAppendix;
        private System.Windows.Forms.RadioButton rbMergeOutputFiles;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown numConvertInterval;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtLastResID;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown numBatchCount;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cbAllConverters;
        private System.Windows.Forms.Label label27;
    }
}