﻿namespace QDasConverter
{
	partial class LoginForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			timer1 = new System.Windows.Forms.Timer(components);
			SuspendLayout();
			// 
			// timer1
			// 
			timer1.Interval = 1;
			timer1.Tick += new System.EventHandler(timer1_Tick);
			// 
			// LoginForm
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackgroundImage = global::QDasConverter.Properties.Resources.Welcome;
			ClientSize = new System.Drawing.Size(654, 494);
			Cursor = System.Windows.Forms.Cursors.AppStarting;
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			Name = "LoginForm";
			ShowInTaskbar = false;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "LoginForm";
			Load += new System.EventHandler(LoginForm_Load);
			Paint += new System.Windows.Forms.PaintEventHandler(LoginForm_Paint);
			ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Timer timer1;
	}
}