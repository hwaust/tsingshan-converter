﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QDasConverter.Forms
{
    public partial class TaskForm : Form
    {
        public static string lastSelectedFolder = ".";
        public TaskForm()
        {
            InitializeComponent();
        }

        private void BtnSuccessfulFolder_Click(object sender, EventArgs e)
        {
            if (rbFile.Checked)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.InitialDirectory = lastSelectedFolder;
                // ofd.Filter =Common.transducer.pd.GetExtFilter();
                ofd.FilterIndex = 1;
                ofd.Multiselect = true;
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtSuccessfulFolder.Text = ofd.FileNames[0] ;
                }
            }
        }
    }
}
