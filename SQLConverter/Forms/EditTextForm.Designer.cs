﻿namespace QDasConverter.Forms
{
	partial class EditTextForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			txtK2003 = new System.Windows.Forms.TextBox();
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			button1 = new System.Windows.Forms.Button();
			lblK2002 = new System.Windows.Forms.Label();
			SuspendLayout();
			// 
			// txtK2003
			// 
			txtK2003.Location = new System.Drawing.Point(124, 42);
			txtK2003.Name = "txtK2003";
			txtK2003.Size = new System.Drawing.Size(132, 21);
			txtK2003.TabIndex = 0;
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(33, 18);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(83, 12);
			label1.TabIndex = 1;
			label1.Text = "基准项(K2002)";
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(21, 45);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(95, 12);
			label2.TabIndex = 2;
			label2.Text = "基准参数(K2003)";
			// 
			// button1
			// 
			button1.Location = new System.Drawing.Point(180, 78);
			button1.Name = "button1";
			button1.Size = new System.Drawing.Size(76, 26);
			button1.TabIndex = 3;
			button1.Text = "确定";
			button1.UseVisualStyleBackColor = true;
			button1.Click += new System.EventHandler(button1_Click);
			// 
			// lblK2002
			// 
			lblK2002.AutoSize = true;
			lblK2002.Location = new System.Drawing.Point(122, 18);
			lblK2002.Name = "lblK2002";
			lblK2002.Size = new System.Drawing.Size(83, 12);
			lblK2002.TabIndex = 4;
			lblK2002.Text = "基准项(K2002)";
			// 
			// EditTextForm
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			ClientSize = new System.Drawing.Size(298, 116);
			Controls.Add(lblK2002);
			Controls.Add(button1);
			Controls.Add(label2);
			Controls.Add(label1);
			Controls.Add(txtK2003);
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			MaximizeBox = false;
			MinimizeBox = false;
			Name = "EditTextForm";
			ShowInTaskbar = false;
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			Text = "参考基准";
			Load += new System.EventHandler(EditTextForm_Load);
			ResumeLayout(false);
			PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtK2003;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label lblK2002;
	}
}