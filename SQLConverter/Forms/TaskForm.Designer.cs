﻿namespace QDasConverter.Forms
{
    partial class TaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            gbAutoConfig = new System.Windows.Forms.GroupBox();
            dpEndTime = new System.Windows.Forms.DateTimePicker();
            dpStartTime = new System.Windows.Forms.DateTimePicker();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            numCircleValue = new System.Windows.Forms.NumericUpDown();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            rbFile = new System.Windows.Forms.RadioButton();
            rbFolder = new System.Windows.Forms.RadioButton();
            groupBox1 = new System.Windows.Forms.GroupBox();
            btnSuccessfulFolder = new System.Windows.Forms.Button();
            txtSuccessfulFolder = new System.Windows.Forms.TextBox();
            btnCancel = new System.Windows.Forms.Button();
            btnSave = new System.Windows.Forms.Button();
            gbAutoConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(numCircleValue)).BeginInit();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // gbAutoConfig
            // 
            gbAutoConfig.Controls.Add(label2);
            gbAutoConfig.Controls.Add(label1);
            gbAutoConfig.Controls.Add(dpStartTime);
            gbAutoConfig.Controls.Add(label17);
            gbAutoConfig.Controls.Add(label5);
            gbAutoConfig.Controls.Add(numCircleValue);
            gbAutoConfig.Controls.Add(dpEndTime);
            gbAutoConfig.Controls.Add(label6);
            gbAutoConfig.Enabled = false;
            gbAutoConfig.Location = new System.Drawing.Point(13, 115);
            gbAutoConfig.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            gbAutoConfig.Name = "gbAutoConfig";
            gbAutoConfig.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            gbAutoConfig.Size = new System.Drawing.Size(544, 176);
            gbAutoConfig.TabIndex = 29;
            gbAutoConfig.TabStop = false;
            gbAutoConfig.Text = "自动转换配置项";
            // 
            // dpEndTime
            // 
            dpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            dpEndTime.Location = new System.Drawing.Point(320, 69);
            dpEndTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            dpEndTime.Name = "dpEndTime";
            dpEndTime.ShowUpDown = true;
            dpEndTime.Size = new System.Drawing.Size(101, 26);
            dpEndTime.TabIndex = 33;
            dpEndTime.Value = new System.DateTime(2012, 6, 12, 23, 59, 59, 0);
            // 
            // dpStartTime
            // 
            dpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            dpStartTime.Location = new System.Drawing.Point(88, 70);
            dpStartTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            dpStartTime.Name = "dpStartTime";
            dpStartTime.ShowUpDown = true;
            dpStartTime.Size = new System.Drawing.Size(124, 26);
            dpStartTime.TabIndex = 32;
            dpStartTime.Value = new System.DateTime(2012, 6, 12, 0, 0, 0, 0);
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(240, 76);
            label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(72, 16);
            label5.TabIndex = 27;
            label5.Text = "结束时间";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(9, 76);
            label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(72, 16);
            label6.TabIndex = 26;
            label6.Text = "开始时间";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(9, 37);
            label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(72, 16);
            label17.TabIndex = 24;
            label17.Text = "监控周期";
            // 
            // numCircleValue
            // 
            numCircleValue.Location = new System.Drawing.Point(88, 33);
            numCircleValue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            numCircleValue.Maximum = new decimal(new int[] {
            43200,
            0,
            0,
            0});
            numCircleValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            numCircleValue.Name = "numCircleValue";
            numCircleValue.Size = new System.Drawing.Size(124, 26);
            numCircleValue.TabIndex = 23;
            numCircleValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            numCircleValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(219, 37);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(296, 16);
            label1.TabIndex = 36;
            label1.Text = "单位：分钟，最大值为 43200，即30天。";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(33, 109);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(232, 16);
            label2.TabIndex = 37;
            label2.Text = "注：开始时间需小于结束时间。";
            // 
            // rbFile
            // 
            rbFile.AutoSize = true;
            rbFile.Checked = true;
            rbFile.Location = new System.Drawing.Point(27, 27);
            rbFile.Margin = new System.Windows.Forms.Padding(4);
            rbFile.Name = "rbFile";
            rbFile.Size = new System.Drawing.Size(58, 20);
            rbFile.TabIndex = 0;
            rbFile.TabStop = true;
            rbFile.Text = "文件";
            rbFile.UseVisualStyleBackColor = true;
            // 
            // rbFolder
            // 
            rbFolder.AutoSize = true;
            rbFolder.Location = new System.Drawing.Point(170, 27);
            rbFolder.Margin = new System.Windows.Forms.Padding(4);
            rbFolder.Name = "rbFolder";
            rbFolder.Size = new System.Drawing.Size(74, 20);
            rbFolder.TabIndex = 1;
            rbFolder.Text = "文件夹";
            rbFolder.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(btnSuccessfulFolder);
            groupBox1.Controls.Add(txtSuccessfulFolder);
            groupBox1.Controls.Add(rbFolder);
            groupBox1.Controls.Add(rbFile);
            groupBox1.Location = new System.Drawing.Point(13, 13);
            groupBox1.Margin = new System.Windows.Forms.Padding(4);
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new System.Windows.Forms.Padding(4);
            groupBox1.Size = new System.Drawing.Size(544, 94);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "任务类型";
            // 
            // btnSuccessfulFolder
            // 
            btnSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            btnSuccessfulFolder.Location = new System.Drawing.Point(436, 23);
            btnSuccessfulFolder.Name = "btnSuccessfulFolder";
            btnSuccessfulFolder.Size = new System.Drawing.Size(97, 29);
            btnSuccessfulFolder.TabIndex = 20;
            btnSuccessfulFolder.Text = "选择...";
            btnSuccessfulFolder.UseVisualStyleBackColor = true;
            btnSuccessfulFolder.Click += new System.EventHandler(BtnSuccessfulFolder_Click);
            // 
            // txtSuccessfulFolder
            // 
            txtSuccessfulFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            txtSuccessfulFolder.Location = new System.Drawing.Point(31, 61);
            txtSuccessfulFolder.Name = "txtSuccessfulFolder";
            txtSuccessfulFolder.Size = new System.Drawing.Size(506, 26);
            txtSuccessfulFolder.TabIndex = 19;
            txtSuccessfulFolder.Text = "D:\\QDAS\\Backups\\Success";
            // 
            // btnCancel
            // 
            btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            btnCancel.Location = new System.Drawing.Point(451, 298);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(106, 34);
            btnCancel.TabIndex = 31;
            btnCancel.Text = "取消";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            btnSave.Location = new System.Drawing.Point(339, 298);
            btnSave.Name = "btnSave";
            btnSave.Size = new System.Drawing.Size(106, 34);
            btnSave.TabIndex = 30;
            btnSave.Text = "保存";
            btnSave.UseVisualStyleBackColor = true;
            // 
            // TaskForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(579, 344);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Controls.Add(gbAutoConfig);
            Controls.Add(groupBox1);
            Font = new System.Drawing.Font("宋体", 12F);
            Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            Name = "TaskForm";
            Text = "任务窗体";
            gbAutoConfig.ResumeLayout(false);
            gbAutoConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(numCircleValue)).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbAutoConfig;
        private System.Windows.Forms.DateTimePicker dpEndTime;
        private System.Windows.Forms.DateTimePicker dpStartTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numCircleValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbFile;
        private System.Windows.Forms.RadioButton rbFolder;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSuccessfulFolder;
        private System.Windows.Forms.TextBox txtSuccessfulFolder;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}