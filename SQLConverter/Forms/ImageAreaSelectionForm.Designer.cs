﻿namespace QDasConverter.Forms
{
    partial class ImageAreaSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageAreaSelectionForm));
            pictureBox1 = new System.Windows.Forms.PictureBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            toolStrip1 = new System.Windows.Forms.ToolStrip();
            tiAdd = new System.Windows.Forms.ToolStripButton();
            tbConfirm = new System.Windows.Forms.ToolStripButton();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            tiClose = new System.Windows.Forms.ToolStripButton();
            statusStrip1 = new System.Windows.Forms.StatusStrip();
            tbMouseLocation = new System.Windows.Forms.ToolStripStatusLabel();
            tbAreaInfo = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            toolStrip1.SuspendLayout();
            statusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // pictureBox1
            // 
            pictureBox1.Location = new System.Drawing.Point(0, 68);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(1083, 438);
            pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(PictureBox1_Paint);
            pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(PictureBox1_MouseDown);
            pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(PictureBox1_MouseMove);
            pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(PictureBox1_MouseUp);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            label1.Location = new System.Drawing.Point(12, 39);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(88, 26);
            label1.TabIndex = 1;
            label1.Text = "区域选择";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("宋体", 12F);
            label2.Location = new System.Drawing.Point(106, 45);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(344, 16);
            label2.TabIndex = 2;
            label2.Text = "请使用鼠标左键拖动框选选择需要识别的区域。";
            // 
            // toolStrip1
            // 
            toolStrip1.BackColor = System.Drawing.Color.White;
            toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            tiAdd,
            tbConfirm,
            toolStripSeparator1,
            tiClose});
            toolStrip1.Location = new System.Drawing.Point(0, 0);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.Size = new System.Drawing.Size(1083, 39);
            toolStrip1.TabIndex = 4;
            toolStrip1.Text = "toolStrip1";
            // 
            // tiAdd
            // 
            tiAdd.Image = ((System.Drawing.Image)(resources.GetObject("tiAdd.Image")));
            tiAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            tiAdd.Name = "tiAdd";
            tiAdd.Size = new System.Drawing.Size(110, 36);
            tiAdd.Text = "添加区域(&N)";
            tiAdd.Click += new System.EventHandler(TiAdd_Click);
            // 
            // tbConfirm
            // 
            tbConfirm.Image = ((System.Drawing.Image)(resources.GetObject("tbConfirm.Image")));
            tbConfirm.ImageTransparentColor = System.Drawing.Color.Magenta;
            tbConfirm.Name = "tbConfirm";
            tbConfirm.Size = new System.Drawing.Size(110, 36);
            tbConfirm.Text = "确认选择(&O)";
            tbConfirm.Click += new System.EventHandler(TbConfirm_Click);
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // tiClose
            // 
            tiClose.Image = ((System.Drawing.Image)(resources.GetObject("tiClose.Image")));
            tiClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            tiClose.Name = "tiClose";
            tiClose.Size = new System.Drawing.Size(108, 36);
            tiClose.Text = "关闭窗体(&C)";
            tiClose.Click += new System.EventHandler(TiClose_Click);
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            tbMouseLocation,
            tbAreaInfo});
            statusStrip1.Location = new System.Drawing.Point(0, 570);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new System.Drawing.Size(1083, 22);
            statusStrip1.TabIndex = 5;
            statusStrip1.Text = "statusStrip1";
            // 
            // tbMouseLocation
            // 
            tbMouseLocation.AutoSize = false;
            tbMouseLocation.Name = "tbMouseLocation";
            tbMouseLocation.Size = new System.Drawing.Size(200, 17);
            // 
            // tbAreaInfo
            // 
            tbAreaInfo.AutoSize = false;
            tbAreaInfo.Name = "tbAreaInfo";
            tbAreaInfo.Size = new System.Drawing.Size(300, 17);
            // 
            // ImageAreaSelectionForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(1083, 592);
            Controls.Add(statusStrip1);
            Controls.Add(label1);
            Controls.Add(toolStrip1);
            Controls.Add(label2);
            Controls.Add(pictureBox1);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ImageAreaSelectionForm";
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "图像区域选：请使用鼠标左键拖动框选选择需要识别的区域。";
            Load += new System.EventHandler(ImageAreaSelectionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tiAdd;
        private System.Windows.Forms.ToolStripButton tiClose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tbMouseLocation;
        private System.Windows.Forms.ToolStripStatusLabel tbAreaInfo;
        private System.Windows.Forms.ToolStripButton tbConfirm;
    }
}