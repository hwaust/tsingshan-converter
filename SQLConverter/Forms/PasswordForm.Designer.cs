﻿namespace QDasConverter
{
	partial class PasswordForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			btnOK = new System.Windows.Forms.Button();
			btnCancel = new System.Windows.Forms.Button();
			groupBox1 = new System.Windows.Forms.GroupBox();
			txtPassword = new System.Windows.Forms.TextBox();
			groupBox1.SuspendLayout();
			SuspendLayout();
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(20, 53);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(281, 36);
			label1.TabIndex = 0;
			label1.Text = "注：设置此密码后，在关闭程序时即要输入此密码，\r\n    用于避免其他人无意中关闭本程序。\r\n    不需要请设置为空，并确定。";
			// 
			// btnOK
			// 
			btnOK.Location = new System.Drawing.Point(133, 115);
			btnOK.Name = "btnOK";
			btnOK.Size = new System.Drawing.Size(87, 28);
			btnOK.TabIndex = 1;
			btnOK.Text = "确定";
			btnOK.UseVisualStyleBackColor = true;
			btnOK.Click += new System.EventHandler(btnOK_Click);
			// 
			// btnCancel
			// 
			btnCancel.Location = new System.Drawing.Point(226, 115);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new System.Drawing.Size(87, 28);
			btnCancel.TabIndex = 2;
			btnCancel.Text = "取消";
			btnCancel.UseVisualStyleBackColor = true;
			btnCancel.Click += new System.EventHandler(btnCancel_Click);
			// 
			// groupBox1
			// 
			groupBox1.Controls.Add(txtPassword);
			groupBox1.Controls.Add(label1);
			groupBox1.Location = new System.Drawing.Point(12, 12);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new System.Drawing.Size(305, 97);
			groupBox1.TabIndex = 3;
			groupBox1.TabStop = false;
			groupBox1.Text = "关闭密码设置";
			// 
			// txtPassword
			// 
			txtPassword.Location = new System.Drawing.Point(22, 20);
			txtPassword.Name = "txtPassword";
			txtPassword.Size = new System.Drawing.Size(277, 21);
			txtPassword.TabIndex = 1;
			// 
			// PasswordForm
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			ClientSize = new System.Drawing.Size(326, 155);
			Controls.Add(groupBox1);
			Controls.Add(btnCancel);
			Controls.Add(btnOK);
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			MaximizeBox = false;
			MinimizeBox = false;
			Name = "PasswordForm";
			StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			Text = "设置关闭时的确认密码";
			Load += new System.EventHandler(PasswordForm_Load);
			groupBox1.ResumeLayout(false);
			groupBox1.PerformLayout();
			ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtPassword;
	}
}