﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QDasConverter.Forms
{
    public partial class ImageAreaSelectionForm : Form
    {
        public ImageAreaSelectionForm()
        {
            InitializeComponent();
        }

        private void ImageAreaSelectionForm_Load(object sender, EventArgs e)
        {
            Width = pictureBox1.Width + 15;
            Height = pictureBox1.Height + 160;
            Left = (Screen.PrimaryScreen.WorkingArea.Width - Width) / 2;
            Top = (Screen.PrimaryScreen.WorkingArea.Height - Height) / 2;
        }

        /// <summary>
        /// 加载指定路径的图片文件。
        /// </summary>
        /// <param name="imagepath"></param>
        public void LoadImage(string imagepath)
        {
            try
            {
                pictureBox1.Image = Image.FromFile(imagepath);
            }
            catch { }
        }

        bool isMousedown = false;
        Point start, end;
        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            isMousedown = true;
            start = e.Location;
        }

        private void PictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            tbMouseLocation.Text = $"当前鼠标位置：({e.X}，{e.Y})";
            Rectangle r = GetRectangle();
            tbAreaInfo.Text = $"当前选中区域：({r.X},{r.Y},{r.Width},{r.Height})";
            if (isMousedown)
            {
                end = e.Location;
                pictureBox1.Invalidate();
            }
        }

        private void PictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            end = e.Location;
            isMousedown = false;
            pictureBox1.Invalidate();
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = GetRectangle();
            Rectangle r1 = new Rectangle(r.X + 1, r.Y + 1, r.Width, r.Height);
            e.Graphics.DrawRectangle(Pens.DarkGray, r1);
            e.Graphics.DrawRectangle(Pens.White, r);
        }

        private void TiAdd_Click(object sender, EventArgs e)
        {
            start = new Point();
            end = new Point();
            pictureBox1.Invalidate();
        }

        private void TbConfirm_Click(object sender, EventArgs e)
        {
            if (GetRectangle().Width > 0 && GetRectangle().Height > 0)
            {
                Close();
            }
            else
            {
                MessageBox.Show("窗体选择不正确，请重新选择。");
            }

        }

        private void TiClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// 获得截取的区域信息。
        /// </summary>
        /// <returns></returns>
        public Rectangle GetRectangle()
        {
            int x = Math.Min(start.X, end.X);
            int y = Math.Min(start.Y, end.Y);

            return new Rectangle(x, y, Math.Abs(end.X - start.X), Math.Abs(end.Y - start.Y));

        }
    }
}
