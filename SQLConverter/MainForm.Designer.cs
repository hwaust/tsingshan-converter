﻿namespace QDasConverter
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnNewConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.mnSaveConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.mnExportConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnExit = new System.Windows.Forms.ToolStripMenuItem();
            this.生成RToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiStartTransfer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnTranduceSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.设置OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiSystemConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDBConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.tsConvertConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tiClosingPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.关于HToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于AToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsLastResID = new System.Windows.Forms.ToolStripStatusLabel();
            this.tbMain = new System.Windows.Forms.TabControl();
            this.tpOutputFilesPage = new System.Windows.Forms.TabPage();
            this.tiSaveOutputFiles = new System.Windows.Forms.ToolStrip();
            this.tiSaveResults = new System.Windows.Forms.ToolStripButton();
            this.tiClear = new System.Windows.Forms.ToolStripButton();
            this.lvResults = new QDasConverter.Utils.MyListView();
            this.chTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chInput = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chOutput = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mnFolder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnTransduce = new System.Windows.Forms.ToolStripMenuItem();
            this.mnOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnOpenInputFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.mnDeleteItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.取消ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbLogPage = new System.Windows.Forms.TabPage();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.tiOpenLogFile = new System.Windows.Forms.ToolStripButton();
            this.tiClearLogList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tiInfo = new System.Windows.Forms.ToolStripLabel();
            this.lvLogs = new QDasConverter.Utils.MyListView();
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colContent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dvCustomKField = new System.Windows.Forms.DataGridView();
            this.KName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KUsage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tiStart = new System.Windows.Forms.ToolStripButton();
            this.tiStop = new System.Windows.Forms.ToolStripButton();
            this.tiOpenAppFolder = new System.Windows.Forms.ToolStripButton();
            this.tiOpenOutputFolder = new System.Windows.Forms.ToolStripButton();
            this.tiLock = new System.Windows.Forms.ToolStripButton();
            this.tiConfig = new System.Windows.Forms.ToolStripButton();
            this.dbConnection = new System.Windows.Forms.ToolStripButton();
            this.tiSetID = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.taskbarMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnShowWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.mnHideWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnExitApplication = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tbMain.SuspendLayout();
            this.tpOutputFilesPage.SuspendLayout();
            this.tiSaveOutputFiles.SuspendLayout();
            this.mnFolder.SuspendLayout();
            this.tbLogPage.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvCustomKField)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.taskbarMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "File16X18.png");
            this.imageList1.Images.SetKeyName(1, "Folder16X18.png");
            this.imageList1.Images.SetKeyName(2, "Tranduce16X16.png");
            this.imageList1.Images.SetKeyName(3, "RedCross16X16.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件FToolStripMenuItem,
            this.生成RToolStripMenuItem,
            this.设置OToolStripMenuItem,
            this.关于HToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1424, 25);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // 文件FToolStripMenuItem
            // 
            this.文件FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnNewConfig,
            this.mnSaveConfig,
            this.mnExportConfig,
            this.toolStripMenuItem2,
            this.mnExit});
            this.文件FToolStripMenuItem.Name = "文件FToolStripMenuItem";
            this.文件FToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.文件FToolStripMenuItem.Text = "文件(&F)";
            // 
            // mnNewConfig
            // 
            this.mnNewConfig.Name = "mnNewConfig";
            this.mnNewConfig.Size = new System.Drawing.Size(142, 22);
            this.mnNewConfig.Text = "重置配置(&N)";
            this.mnNewConfig.Click += new System.EventHandler(this.mnNewConfig_Click);
            // 
            // mnSaveConfig
            // 
            this.mnSaveConfig.Name = "mnSaveConfig";
            this.mnSaveConfig.Size = new System.Drawing.Size(142, 22);
            this.mnSaveConfig.Text = "保存配置(&S)";
            this.mnSaveConfig.Click += new System.EventHandler(this.mnSaveConfig_Click);
            // 
            // mnExportConfig
            // 
            this.mnExportConfig.Name = "mnExportConfig";
            this.mnExportConfig.Size = new System.Drawing.Size(142, 22);
            this.mnExportConfig.Text = "导出配置(A)";
            this.mnExportConfig.Click += new System.EventHandler(this.mnExportConfig_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(139, 6);
            // 
            // mnExit
            // 
            this.mnExit.Name = "mnExit";
            this.mnExit.Size = new System.Drawing.Size(142, 22);
            this.mnExit.Text = "退出(&X)";
            // 
            // 生成RToolStripMenuItem
            // 
            this.生成RToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiStartTransfer,
            this.mnTranduceSelected});
            this.生成RToolStripMenuItem.Name = "生成RToolStripMenuItem";
            this.生成RToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.生成RToolStripMenuItem.Text = "生成(&R)";
            // 
            // tiStartTransfer
            // 
            this.tiStartTransfer.Name = "tiStartTransfer";
            this.tiStartTransfer.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tiStartTransfer.Size = new System.Drawing.Size(161, 22);
            this.tiStartTransfer.Text = "转换全部(&B)";
            this.tiStartTransfer.Click += new System.EventHandler(this.tiStart_Click);
            // 
            // mnTranduceSelected
            // 
            this.mnTranduceSelected.Name = "mnTranduceSelected";
            this.mnTranduceSelected.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.mnTranduceSelected.Size = new System.Drawing.Size(161, 22);
            this.mnTranduceSelected.Text = "转换选中(&P)";
            this.mnTranduceSelected.Visible = false;
            this.mnTranduceSelected.Click += new System.EventHandler(this.tiTransduceSelected_Click);
            // 
            // 设置OToolStripMenuItem
            // 
            this.设置OToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiSystemConfig,
            this.tsDBConfig,
            this.tsConvertConfig,
            this.toolStripMenuItem1,
            this.tiClosingPassword});
            this.设置OToolStripMenuItem.Name = "设置OToolStripMenuItem";
            this.设置OToolStripMenuItem.Size = new System.Drawing.Size(62, 21);
            this.设置OToolStripMenuItem.Text = "设置(&O)";
            // 
            // tiSystemConfig
            // 
            this.tiSystemConfig.Name = "tiSystemConfig";
            this.tiSystemConfig.Size = new System.Drawing.Size(153, 22);
            this.tiSystemConfig.Text = "通用配置(&C)";
            this.tiSystemConfig.Click += new System.EventHandler(this.tiSystemConfig_Click);
            // 
            // tsDBConfig
            // 
            this.tsDBConfig.Name = "tsDBConfig";
            this.tsDBConfig.Size = new System.Drawing.Size(153, 22);
            this.tsDBConfig.Text = "数据库配置(&D)";
            this.tsDBConfig.Click += new System.EventHandler(this.tsDBConfig_Click);
            // 
            // tsConvertConfig
            // 
            this.tsConvertConfig.Name = "tsConvertConfig";
            this.tsConvertConfig.Size = new System.Drawing.Size(153, 22);
            this.tsConvertConfig.Text = "转换配置(&T)";
            this.tsConvertConfig.Click += new System.EventHandler(this.tsConvertConfig_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(150, 6);
            this.toolStripMenuItem1.Visible = false;
            // 
            // tiClosingPassword
            // 
            this.tiClosingPassword.Name = "tiClosingPassword";
            this.tiClosingPassword.Size = new System.Drawing.Size(153, 22);
            this.tiClosingPassword.Text = "关机密码(&P)";
            this.tiClosingPassword.Visible = false;
            // 
            // 关于HToolStripMenuItem
            // 
            this.关于HToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于AToolStripMenuItem});
            this.关于HToolStripMenuItem.Name = "关于HToolStripMenuItem";
            this.关于HToolStripMenuItem.Size = new System.Drawing.Size(61, 21);
            this.关于HToolStripMenuItem.Text = "帮助(&H)";
            // 
            // 关于AToolStripMenuItem
            // 
            this.关于AToolStripMenuItem.Name = "关于AToolStripMenuItem";
            this.关于AToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.关于AToolStripMenuItem.Text = "关于(&A)";
            this.关于AToolStripMenuItem.Click += new System.EventHandler(this.tsAbout_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLastResID});
            this.statusStrip1.Location = new System.Drawing.Point(0, 839);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 21, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1424, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsLastResID
            // 
            this.tsLastResID.BackColor = System.Drawing.SystemColors.Control;
            this.tsLastResID.Name = "tsLastResID";
            this.tsLastResID.Size = new System.Drawing.Size(0, 17);
            // 
            // tbMain
            // 
            this.tbMain.Controls.Add(this.tpOutputFilesPage);
            this.tbMain.Controls.Add(this.tbLogPage);
            this.tbMain.Controls.Add(this.tabPage1);
            this.tbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMain.Location = new System.Drawing.Point(0, 81);
            this.tbMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbMain.Name = "tbMain";
            this.tbMain.SelectedIndex = 0;
            this.tbMain.Size = new System.Drawing.Size(1424, 758);
            this.tbMain.TabIndex = 19;
            // 
            // tpOutputFilesPage
            // 
            this.tpOutputFilesPage.Controls.Add(this.tiSaveOutputFiles);
            this.tpOutputFilesPage.Controls.Add(this.lvResults);
            this.tpOutputFilesPage.Location = new System.Drawing.Point(4, 28);
            this.tpOutputFilesPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpOutputFilesPage.Name = "tpOutputFilesPage";
            this.tpOutputFilesPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpOutputFilesPage.Size = new System.Drawing.Size(1416, 726);
            this.tpOutputFilesPage.TabIndex = 1;
            this.tpOutputFilesPage.Text = "输出列表";
            this.tpOutputFilesPage.UseVisualStyleBackColor = true;
            // 
            // tiSaveOutputFiles
            // 
            this.tiSaveOutputFiles.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tiSaveOutputFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiSaveResults,
            this.tiClear});
            this.tiSaveOutputFiles.Location = new System.Drawing.Point(4, 4);
            this.tiSaveOutputFiles.Name = "tiSaveOutputFiles";
            this.tiSaveOutputFiles.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.tiSaveOutputFiles.Size = new System.Drawing.Size(1408, 31);
            this.tiSaveOutputFiles.TabIndex = 28;
            this.tiSaveOutputFiles.Text = "toolStrip4";
            // 
            // tiSaveResults
            // 
            this.tiSaveResults.Image = global::QDasConverter.Properties.Resources.Save;
            this.tiSaveResults.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiSaveResults.Name = "tiSaveResults";
            this.tiSaveResults.Size = new System.Drawing.Size(108, 28);
            this.tiSaveResults.Text = "保存转换结果";
            this.tiSaveResults.Click += new System.EventHandler(this.tiSaveResults_Click);
            // 
            // tiClear
            // 
            this.tiClear.Image = global::QDasConverter.Properties.Resources.clear;
            this.tiClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiClear.Name = "tiClear";
            this.tiClear.Size = new System.Drawing.Size(84, 28);
            this.tiClear.Text = "清空列表";
            this.tiClear.Click += new System.EventHandler(this.tiClear_Click);
            // 
            // lvResults
            // 
            this.lvResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chTime,
            this.chInput,
            this.chState,
            this.chOutput});
            this.lvResults.ContextMenuStrip = this.mnFolder;
            this.lvResults.FullRowSelect = true;
            this.lvResults.GridLines = true;
            this.lvResults.HideSelection = false;
            this.lvResults.Location = new System.Drawing.Point(12, 56);
            this.lvResults.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvResults.MultiSelect = false;
            this.lvResults.Name = "lvResults";
            this.lvResults.Size = new System.Drawing.Size(1386, 657);
            this.lvResults.SmallImageList = this.imageList1;
            this.lvResults.TabIndex = 22;
            this.lvResults.UseCompatibleStateImageBehavior = false;
            this.lvResults.View = System.Windows.Forms.View.Details;
            // 
            // chTime
            // 
            this.chTime.DisplayIndex = 3;
            this.chTime.Text = "时间";
            this.chTime.Width = 103;
            // 
            // chInput
            // 
            this.chInput.DisplayIndex = 0;
            this.chInput.Text = "输入数据编号";
            this.chInput.Width = 318;
            // 
            // chState
            // 
            this.chState.DisplayIndex = 1;
            this.chState.Text = "状态";
            this.chState.Width = 92;
            // 
            // chOutput
            // 
            this.chOutput.DisplayIndex = 2;
            this.chOutput.Text = "输出文件";
            this.chOutput.Width = 424;
            // 
            // mnFolder
            // 
            this.mnFolder.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnFolder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnTransduce,
            this.mnOpenFile,
            this.mnOpenInputFolder,
            this.mnDeleteItem,
            this.toolStripSeparator4,
            this.取消ToolStripMenuItem});
            this.mnFolder.Name = "mnFolder";
            this.mnFolder.Size = new System.Drawing.Size(183, 140);
            // 
            // mnTransduce
            // 
            this.mnTransduce.Image = global::QDasConverter.Properties.Resources.Tranduce16X16;
            this.mnTransduce.Name = "mnTransduce";
            this.mnTransduce.Size = new System.Drawing.Size(182, 26);
            this.mnTransduce.Text = "转换选中项(&V)";
            this.mnTransduce.Click += new System.EventHandler(this.mnTransduce_Click);
            // 
            // mnOpenFile
            // 
            this.mnOpenFile.Name = "mnOpenFile";
            this.mnOpenFile.Size = new System.Drawing.Size(182, 26);
            this.mnOpenFile.Text = "打开当前文件(&F)";
            this.mnOpenFile.Click += new System.EventHandler(this.mnOpenFile_Click);
            // 
            // mnOpenInputFolder
            // 
            this.mnOpenInputFolder.Name = "mnOpenInputFolder";
            this.mnOpenInputFolder.Size = new System.Drawing.Size(182, 26);
            this.mnOpenInputFolder.Text = "打开当前文件夹(&O)";
            this.mnOpenInputFolder.Click += new System.EventHandler(this.mnOpenInputFolder_Click);
            // 
            // mnDeleteItem
            // 
            this.mnDeleteItem.Name = "mnDeleteItem";
            this.mnDeleteItem.Size = new System.Drawing.Size(182, 26);
            this.mnDeleteItem.Text = "移除选中项(&D)";
            this.mnDeleteItem.Click += new System.EventHandler(this.mnDeleteItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(179, 6);
            // 
            // 取消ToolStripMenuItem
            // 
            this.取消ToolStripMenuItem.Name = "取消ToolStripMenuItem";
            this.取消ToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.取消ToolStripMenuItem.Text = "取消(&X)";
            // 
            // tbLogPage
            // 
            this.tbLogPage.Controls.Add(this.toolStrip3);
            this.tbLogPage.Controls.Add(this.lvLogs);
            this.tbLogPage.Location = new System.Drawing.Point(4, 28);
            this.tbLogPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLogPage.Name = "tbLogPage";
            this.tbLogPage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbLogPage.Size = new System.Drawing.Size(1746, 874);
            this.tbLogPage.TabIndex = 2;
            this.tbLogPage.Text = "系统日志";
            this.tbLogPage.UseVisualStyleBackColor = true;
            // 
            // toolStrip3
            // 
            this.toolStrip3.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiOpenLogFile,
            this.tiClearLogList,
            this.toolStripSeparator5,
            this.tiInfo});
            this.toolStrip3.Location = new System.Drawing.Point(4, 4);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolStrip3.Size = new System.Drawing.Size(1738, 31);
            this.toolStrip3.TabIndex = 23;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // tiOpenLogFile
            // 
            this.tiOpenLogFile.Image = global::QDasConverter.Properties.Resources.Open;
            this.tiOpenLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiOpenLogFile.Name = "tiOpenLogFile";
            this.tiOpenLogFile.Size = new System.Drawing.Size(108, 28);
            this.tiOpenLogFile.Text = "打开日志文件";
            this.tiOpenLogFile.Click += new System.EventHandler(this.tiOpenLogFile_Click);
            // 
            // tiClearLogList
            // 
            this.tiClearLogList.Image = global::QDasConverter.Properties.Resources.clear;
            this.tiClearLogList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiClearLogList.Name = "tiClearLogList";
            this.tiClearLogList.Size = new System.Drawing.Size(108, 28);
            this.tiClearLogList.Text = "清除日志列表";
            this.tiClearLogList.Click += new System.EventHandler(this.tiClearLogList_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
            // 
            // tiInfo
            // 
            this.tiInfo.Name = "tiInfo";
            this.tiInfo.Size = new System.Drawing.Size(276, 28);
            this.tiInfo.Text = "注：日志列表最多显示1000行，多余的会被删除。";
            // 
            // lvLogs
            // 
            this.lvLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvLogs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colTime,
            this.colEvent,
            this.colContent});
            this.lvLogs.FullRowSelect = true;
            this.lvLogs.GridLines = true;
            this.lvLogs.HideSelection = false;
            this.lvLogs.Location = new System.Drawing.Point(12, 56);
            this.lvLogs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvLogs.Name = "lvLogs";
            this.lvLogs.Size = new System.Drawing.Size(1723, 800);
            this.lvLogs.TabIndex = 20;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            this.lvLogs.View = System.Windows.Forms.View.Details;
            // 
            // colID
            // 
            this.colID.Text = "编号";
            this.colID.Width = 63;
            // 
            // colTime
            // 
            this.colTime.Text = "时间";
            this.colTime.Width = 138;
            // 
            // colEvent
            // 
            this.colEvent.Text = "事件";
            this.colEvent.Width = 47;
            // 
            // colContent
            // 
            this.colContent.Text = "说明";
            this.colContent.Width = 682;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dvCustomKField);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(1746, 874);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "自定义参数";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dvCustomKField
            // 
            this.dvCustomKField.AllowUserToAddRows = false;
            this.dvCustomKField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvCustomKField.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dvCustomKField.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvCustomKField.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KName,
            this.KUsage,
            this.KDescription,
            this.KValue});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvCustomKField.DefaultCellStyle = dataGridViewCellStyle5;
            this.dvCustomKField.Enabled = false;
            this.dvCustomKField.Location = new System.Drawing.Point(12, 9);
            this.dvCustomKField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dvCustomKField.MultiSelect = false;
            this.dvCustomKField.Name = "dvCustomKField";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvCustomKField.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dvCustomKField.RowHeadersWidth = 62;
            this.dvCustomKField.RowTemplate.Height = 23;
            this.dvCustomKField.Size = new System.Drawing.Size(0, 0);
            this.dvCustomKField.TabIndex = 1;
            this.dvCustomKField.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DvCustomKField_CellEndEdit);
            this.dvCustomKField.Resize += new System.EventHandler(this.DvCustomKField_Resize);
            // 
            // KName
            // 
            this.KName.HeaderText = "K域";
            this.KName.MinimumWidth = 8;
            this.KName.Name = "KName";
            this.KName.ReadOnly = true;
            this.KName.Width = 150;
            // 
            // KUsage
            // 
            this.KUsage.HeaderText = "作用";
            this.KUsage.MinimumWidth = 8;
            this.KUsage.Name = "KUsage";
            this.KUsage.ReadOnly = true;
            this.KUsage.Width = 150;
            // 
            // KDescription
            // 
            this.KDescription.HeaderText = "说明";
            this.KDescription.MinimumWidth = 8;
            this.KDescription.Name = "KDescription";
            this.KDescription.ReadOnly = true;
            this.KDescription.Width = 250;
            // 
            // KValue
            // 
            this.KValue.HeaderText = "设定值";
            this.KValue.MinimumWidth = 8;
            this.KValue.Name = "KValue";
            this.KValue.Width = 150;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 56);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 56);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.White;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiStart,
            this.tiStop,
            this.toolStripSeparator1,
            this.tiOpenAppFolder,
            this.tiOpenOutputFolder,
            this.toolStripSeparator2,
            this.tiLock,
            this.tiConfig,
            this.dbConnection,
            this.toolStripLabel1,
            this.toolStripSeparator3,
            this.tiSetID});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolStrip1.Size = new System.Drawing.Size(1424, 56);
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tiStart
            // 
            this.tiStart.Image = global::QDasConverter.Properties.Resources.Transduce24X24;
            this.tiStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiStart.Name = "tiStart";
            this.tiStart.Size = new System.Drawing.Size(75, 53);
            this.tiStart.Text = "开始转换(&S)";
            this.tiStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiStart.Click += new System.EventHandler(this.tiStart_Click);
            // 
            // tiStop
            // 
            this.tiStop.Enabled = false;
            this.tiStop.Image = global::QDasConverter.Properties.Resources.Stop;
            this.tiStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiStop.Name = "tiStop";
            this.tiStop.Size = new System.Drawing.Size(75, 53);
            this.tiStop.Text = "停止转换(&S)";
            this.tiStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiStop.Click += new System.EventHandler(this.tiStop_Click);
            // 
            // tiOpenAppFolder
            // 
            this.tiOpenAppFolder.Image = global::QDasConverter.Properties.Resources.OpenAppFolder1;
            this.tiOpenAppFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiOpenAppFolder.Name = "tiOpenAppFolder";
            this.tiOpenAppFolder.Size = new System.Drawing.Size(99, 53);
            this.tiOpenAppFolder.Text = "打开程序目录(&P)";
            this.tiOpenAppFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiOpenAppFolder.ToolTipText = "打开程序目录";
            this.tiOpenAppFolder.Click += new System.EventHandler(this.tiOpenAppFolder_Click);
            // 
            // tiOpenOutputFolder
            // 
            this.tiOpenOutputFolder.Image = global::QDasConverter.Properties.Resources.OpenAppFolder1;
            this.tiOpenOutputFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiOpenOutputFolder.Name = "tiOpenOutputFolder";
            this.tiOpenOutputFolder.Size = new System.Drawing.Size(102, 53);
            this.tiOpenOutputFolder.Text = "打开输出目录(&O)";
            this.tiOpenOutputFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiOpenOutputFolder.Click += new System.EventHandler(this.tiOpenOutputFolder_Click);
            // 
            // tiLock
            // 
            this.tiLock.Image = global::QDasConverter.Properties.Resources.Lock48X48;
            this.tiLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiLock.Name = "tiLock";
            this.tiLock.Size = new System.Drawing.Size(74, 53);
            this.tiLock.Text = "锁定窗口(&L)";
            this.tiLock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiLock.Click += new System.EventHandler(this.tiLock_Click);
            // 
            // tiConfig
            // 
            this.tiConfig.Image = global::QDasConverter.Properties.Resources.gif_46_006;
            this.tiConfig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiConfig.Name = "tiConfig";
            this.tiConfig.Size = new System.Drawing.Size(76, 53);
            this.tiConfig.Text = "通用配置(&C)";
            this.tiConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiConfig.Click += new System.EventHandler(this.tiConfig_Click);
            // 
            // dbConnection
            // 
            this.dbConnection.Image = global::QDasConverter.Properties.Resources.config_db;
            this.dbConnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dbConnection.Name = "dbConnection";
            this.dbConnection.Size = new System.Drawing.Size(96, 53);
            this.dbConnection.Text = "数据库连接配置";
            this.dbConnection.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dbConnection.Click += new System.EventHandler(this.dbConnection_Click);
            // 
            // tiSetID
            // 
            this.tiSetID.Image = global::QDasConverter.Properties.Resources.task1;
            this.tiSetID.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tiSetID.Name = "tiSetID";
            this.tiSetID.Size = new System.Drawing.Size(75, 53);
            this.tiSetID.Text = "测试按键(&T)";
            this.tiSetID.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tiSetID.Click += new System.EventHandler(this.tiSetID_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(12, 53);
            this.toolStripLabel1.Text = " ";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.taskbarMenu;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "青山质量转换器";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // taskbarMenu
            // 
            this.taskbarMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.taskbarMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnShowWindow,
            this.mnHideWindow,
            this.toolStripMenuItem3,
            this.mnExitApplication});
            this.taskbarMenu.Name = "taskbarMenu";
            this.taskbarMenu.Size = new System.Drawing.Size(118, 76);
            // 
            // mnShowWindow
            // 
            this.mnShowWindow.Name = "mnShowWindow";
            this.mnShowWindow.Size = new System.Drawing.Size(117, 22);
            this.mnShowWindow.Text = "显示(&S)";
            this.mnShowWindow.Click += new System.EventHandler(this.mnShowWindow_Click);
            // 
            // mnHideWindow
            // 
            this.mnHideWindow.Name = "mnHideWindow";
            this.mnHideWindow.Size = new System.Drawing.Size(117, 22);
            this.mnHideWindow.Text = "隐藏(&H)";
            this.mnHideWindow.Click += new System.EventHandler(this.mnHideWindow_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(114, 6);
            // 
            // mnExitApplication
            // 
            this.mnExitApplication.Name = "mnExitApplication";
            this.mnExitApplication.Size = new System.Drawing.Size(117, 22);
            this.mnExitApplication.Text = "退出(X)";
            this.mnExitApplication.Click += new System.EventHandler(this.mnExitApplication_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 56);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1424, 861);
            this.Controls.Add(this.tbMain);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(1440, 900);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "标题";
            this.Deactivate += new System.EventHandler(this.MainForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewMainForm_FormClosing);
            this.Load += new System.EventHandler(this.NewMainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tbMain.ResumeLayout(false);
            this.tpOutputFilesPage.ResumeLayout(false);
            this.tpOutputFilesPage.PerformLayout();
            this.tiSaveOutputFiles.ResumeLayout(false);
            this.tiSaveOutputFiles.PerformLayout();
            this.mnFolder.ResumeLayout(false);
            this.tbLogPage.ResumeLayout(false);
            this.tbLogPage.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvCustomKField)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.taskbarMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem 文件FToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem mnNewConfig;
		private System.Windows.Forms.ToolStripMenuItem mnSaveConfig;
		private System.Windows.Forms.ToolStripMenuItem mnExportConfig;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem mnExit;
		private System.Windows.Forms.ToolStripMenuItem 设置OToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem tiSystemConfig;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem tiClosingPassword;
		private System.Windows.Forms.ToolStripMenuItem 生成RToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem tiStartTransfer;
		private System.Windows.Forms.ToolStripMenuItem 关于HToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 关于AToolStripMenuItem;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.TabControl tbMain;
		private System.Windows.Forms.TabPage tpOutputFilesPage;
		private System.Windows.Forms.ColumnHeader colID;
		private System.Windows.Forms.ColumnHeader colTime;
		private System.Windows.Forms.ColumnHeader colEvent;
		private System.Windows.Forms.ColumnHeader colContent;
		private System.Windows.Forms.ContextMenuStrip mnFolder;
		private System.Windows.Forms.ToolStripMenuItem mnOpenInputFolder;
		private System.Windows.Forms.ToolStripMenuItem mnDeleteItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem 取消ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem mnTransduce;
        private System.Windows.Forms.ColumnHeader chInput;
        private System.Windows.Forms.ColumnHeader chState;
        private System.Windows.Forms.ColumnHeader chOutput;
		private System.Windows.Forms.ColumnHeader chTime;
		private System.Windows.Forms.ToolStripButton tiStop;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton tiOpenAppFolder;
		private System.Windows.Forms.ToolStripButton tiOpenOutputFolder;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton tiLock;
		private System.Windows.Forms.ToolStripButton tiConfig;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripMenuItem mnTranduceSelected;
		private System.Windows.Forms.ToolStripMenuItem mnOpenFile;
        private System.Windows.Forms.TabPage tbLogPage;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton tiOpenLogFile;
        private System.Windows.Forms.ToolStripButton tiClearLogList;
        private System.Windows.Forms.ToolStrip tiSaveOutputFiles;
        private System.Windows.Forms.ToolStripButton tiSaveResults;
        private System.Windows.Forms.ToolStripButton tiClear;
        private QDasConverter.Utils.MyListView lvLogs;
        private QDasConverter.Utils.MyListView lvResults;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dvCustomKField;
        private System.Windows.Forms.DataGridViewTextBoxColumn KName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KUsage;
        private System.Windows.Forms.DataGridViewTextBoxColumn KDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn KValue;
        private System.Windows.Forms.ToolStripButton dbConnection;
        private System.Windows.Forms.ToolStripStatusLabel tsLastResID;
        private System.Windows.Forms.ToolStripButton tiSetID;
        private System.Windows.Forms.ToolStripMenuItem tsConvertConfig;
        private System.Windows.Forms.ToolStripMenuItem tsDBConfig;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tiStart;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip taskbarMenu;
        private System.Windows.Forms.ToolStripMenuItem mnShowWindow;
        private System.Windows.Forms.ToolStripMenuItem mnHideWindow;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mnExitApplication;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel tiInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}