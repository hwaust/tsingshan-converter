﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasConverter.Utils
{
    public class CurvePoint
    {
        public short Index { get; set; }
        public double Torque { get; set; }
        public double Angle { get; set; }
        public double TorqueRate { get; set; }
        public double Current { get; set; }
        public string Marker { get; set; }
        public float PointDuration { get; set; }

        public override string ToString()
        {
            return $"Point{{" +
                   $"torque={Torque}" +
                   $", angle={Angle}" +
                   $", torqueRate={TorqueRate}" +
                   $", current={Current}" +
                   $", marker='{Marker}'" +
                   $", time={PointDuration}" +
                   $"}}";
        }
    }
}
