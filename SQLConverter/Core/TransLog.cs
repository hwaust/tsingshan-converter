﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QDasConverter.Core
{
    /// <summary>
    /// 转换时的日志类，用于记录一些转换信息，每个Log对象表示一条日志信息，核心信息为：时间(LogDateTime, 自动生成)、日志类型(LogType)、正文(LogContent)。
    /// </summary>
    public class TransLog
    {
        /// <summary>
        /// 转文件字符串时的分隔符。
        /// </summary>
        public char Splitter = (char)15;

        /// <summary>
        /// 用于生成最新的编号，默认值为10000。
        /// </summary>
        public static int LogID = 10000;

        /// <summary>
        /// 日志编号。
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 日志的类型。
        /// </summary>
        public LogType LogType { get; set; }

        /// <summary>
        /// 转换时间。
        /// </summary>
        public DateTime LogDateTime { get; set; }

        /// <summary>
        /// 输入文件或文件夹。
        /// </summary>
        public String Input { get; set; }

        /// <summary>
        /// 输出文件或文件组。
        /// </summary>
        public String Output { get; set; }

        /// <summary>
        /// 日志的内容。
        /// </summary>
        public string LogContent { get; set; }

        /// <summary>
        /// 备注。
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 可以附加的数据。
        /// </summary>
        public object Tag { get; set; }


        /// <summary>
        /// 以当前时间和INFO创建 TransLog 实例。
        /// </summary>
        public TransLog()
        {
            ID = ++LogID;
            LogDateTime = DateTime.Now;
            LogType =  LogType.INFO;
        }

        /// <summary>
        /// 以日志类型和消息内容创建 TransLog 实例。
        /// </summary>
        /// <param name="type">日志类型。</param>
        /// <param name="loginfo">日志内容。</param>
        public TransLog(LogType type, string logContent)
        {
            ID = ++LogID;
            LogDateTime = DateTime.Now;
            LogType = type;
            LogContent = logContent;
            Input = "";
            Output = "";
            Remark = "";
        }

        /// <summary>
        /// 以日志类型，消息内容，输入文件，输出文件和备注创建 TransLog 实例。
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        /// <param name="infile"></param>
        /// <param name="outfile"></param>
        /// <param name="remark"></param>
        public TransLog(LogType type, string message, string infile, string outfile = "No output", string remark = "-")
        {
            ID = ++LogID;
            LogDateTime = DateTime.Now;
            LogType = type;
            LogContent = message;
            Input = infile;
            Output = outfile;
            Remark = remark;
        }

        /// <summary>
        /// 返回文件中的保存字符串，格式为：ID,LogDateTime,LogType,LogContent,Input,Output,Remark。中间使用splitter分隔。
        /// </summary>
        /// <returns></returns>
        public String ToFileString()
        {
            StringBuilder b = new StringBuilder();
            String s = Splitter.ToString();
            b.Append(ID + s);
            b.Append(LogDateTime.ToString("yyyyMMddHHmmss") + s);
            b.Append(LogType + s);
            b.Append(LogContent + s);
            b.Append(Input + s);
            b.Append(Output);
            b.Append(Remark + s);
            return b.ToString();
        }

        /// <summary>
        /// 将内容转换为字符串数组，格式为：new string[] { ID.ToString(), LogDateTime.ToString("yyyy/MM/dd HH:mm:ss"), LogType.ToString(), LogContent };
        /// </summary>
        /// <returns></returns>
        public string[] ToStrings()
        {
            return new string[] { ID.ToString(), LogDateTime.ToString("yyyy/MM/dd HH:mm:ss"), LogType.ToString(), LogContent };
        }

        /// <summary>
        /// 返回字符串数组，格式为：new string[] { ID.ToString(), LogDateTime.ToString(), LogType.ToString(), LogContent, Output };
        /// </summary>
        /// <returns></returns>
        public string[] GetStrings()
        {
            //编号，0.时间，1.事件，2.原因，3.输出文件
            return new string[] { ID.ToString(), LogDateTime.ToString(), LogType.ToString(), LogContent, Output };
        }
    }
}
