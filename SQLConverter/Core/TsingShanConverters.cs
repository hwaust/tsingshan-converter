﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasConverter.Core
{

    /// <summary>
    /// 青山的所有转换器类型。
    /// </summary>
    public enum TsingShanConverters
    {
        C2021T01_atlas,
        C2021T02_cvinet,
        C2021T03_CTL,
        C2021T04_DisenNVH,
        C2021T05_DisenConsole,
        C2021T06_HoutaNVH,
        C2021T07_HoutaConsole,
        C2021T08_Kisler,
        C2021T09_Excel,
        C2021T10_SPH,
        C2021T11_TDMS,
        /// <summary>
        /// 2021/11/17加入，蓝瑞项目
        /// </summary>
        C2021T20_ComaAtlas
    }
}
