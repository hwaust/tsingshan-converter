﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasConverter.Core
{
    public class C2021T11_Kisler : ConvertBase
    {
        /// <summary>
        /// 数据目录。
        /// </summary>
        public string DataDirectory = @"C:\data\Kisler.DIFF0030";

        public string BackupDirectory = @"C:\data\Kisler.DIFF0030.Backups";

        public override void Initialize()
        {
            base.Initialize();
            NeedConnectionCheck = false;
            DataDirectory = ia.ReadString("DataPath", DataDirectory);
            Interval = 10;
        }

        /// <summary>
        /// 将输入文件移动到指定的目录，文件名不变。
        /// </summary>
        /// <param name="srcfilepath">输入文件。</param>
        /// <param name="targetdir">目标文件夹。</param>
        /// <param name="deleteScr">是否删除原文件，默认值为false。</param>
        public void CopyFileTo(string srcfilepath, string targetdir, bool deleteScr = false)
        {
            var filedir = Path.GetDirectoryName(srcfilepath);
            var filename = Path.GetFileName(srcfilepath);
            File.Copy(srcfilepath, Path.Combine(targetdir, filename));

            if (deleteScr)
                File.Delete(srcfilepath);
        }

        public override void OneLoop_Process()
        {
            foreach (var csvfile in Directory.GetFiles(DataDirectory))
            {
                // csvfile = "Part_DIFF0030_MP-009_2019-03-01_03-11-15_KB00002_OK.csv";

                var lines = File.ReadAllLines(csvfile);

                List<string> list = new List<string>();
                for (int i = 0; i < lines.Length; i++)
                {
                    if (!lines[i].StartsWith("Measuring curve"))
                        continue;
                    i += 10;
                    while (lines[i].Length > 0 && char.IsDigit(lines[i][0]))
                    {
                        list.Add(lines[i]);
                        i++;
                    }
                }

                // Time; X(ABSOLUTE); Y; X (ABSOLUTE)
                // 0,2184; 215,77600; -0,00384; 215,77600
                foreach (var line in list)
                {
                    // var items = line.Replace(',', '.').Split(';');
                    // Console.WriteLine($"Time:{items[0]},  Y:{items[2]},  X:{items[1]}={items[3]}");
                }

                Console.WriteLine("Curves.Count=" + list.Count);

                CopyFileTo(csvfile, BackupDirectory, true);
            }
            base.OneLoop_Process();
        }
    }
}
