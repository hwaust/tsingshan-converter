﻿using QDasConverter.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasConverter.Core
{
    /// <summary>
    /// 代理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TransFileCompleteEventHandler(object sender, List<TransLog> logs);

   
    /// <summary>
    /// 文件转换完成时的参数，包括是否转换成功和待转换原文件两个参数。
    /// </summary>
    public class TransFileCompleteArgs : EventArgs
    {
        /// <summary>
        /// 是否转换成功。
        /// </summary>
        public bool Successed { get; set; }

        /// <summary>
        /// 待转换的原文件。
        /// </summary>
        public string SourceFile { get; set; }
    }
}
