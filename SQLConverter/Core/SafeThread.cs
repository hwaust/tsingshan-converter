﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QDasConverter.Core
{
    /// <summary>
    /// 一个安全可靠的，用于可暂停的多线程转换程序。此类在线程开始和停止前后都可以进行相应的操作，以保证数据完整性。
    /// </summary>
    public class SafeThread
    {
        bool isPaused = false;
        object locker = new object();
        Thread t;

        public bool IsBackground = true;


        public virtual void Initialize()
        {

        }

        /// <summary>
        /// 每次循环开始时要做的事情。
        /// </summary>
        public virtual void OnOneLoopStart()
        {

        }

        /// <summary>
        /// 在多线程处理中，一次循环要做的任务。
        /// </summary>
        public virtual void ProcessOneLoop()
        {

        }

        /// <summary>
        /// 每次循环执行完成后要做的事情。
        /// </summary>
        public virtual void OnOneLoopComplete()
        {

        }

        public virtual bool IsLoopCompleted()
        {
            return true;
        }

        public void Start()
        {
            if (t == null)
            {
                t = t ?? new Thread(MainThreadProcess);
                t.IsBackground = IsBackground;
                OnStart();
                t.Start();
            }
            else if (t.ThreadState == ThreadState.Running)
            {
                return;
            }
            else
            {
                Console.WriteLine("Start failed: Current ThreadState=" + t.ThreadState);
            }
        }

        public bool Stop()
        {
            bool stopped = false;
            if (t?.ThreadState == ThreadState.Running || t?.ThreadState == ThreadState.Background)
            {
                try
                {
                    OnStopping();
                    t.Abort();
                }
                catch (ThreadAbortException)
                {
                    stopped = true;
                    OnStopped();
                }
            }
            return stopped;
        }

        public void About()
        {

        }

        /// <summary>
        /// 线程执行的主程序。
        /// </summary>
        public void MainThreadProcess()
        {
            OnStart();
            do
            {
                OnOneLoopStart();
                ProcessOneLoop();
                OnOneLoopComplete();
                while (isPaused)
                    Thread.Sleep(1);
            } while (!IsLoopCompleted());
            OnStopped();
        }

        public void Pause()
        {
            lock (locker)
            {
                isPaused = true;
            }
        }

        public void Continue()
        {
            lock (locker)
            {
                isPaused = true;
            }
        }

        /// <summary>
        /// 在线程启动时所要执行的任务。
        /// </summary>
        public virtual void OnStart()
        {
            
        }

        /// <summary>
        /// 线程将要暂停时要做的事情。
        /// </summary>
        public virtual void OnPausing()
        {
            
        }

        /// <summary>
        /// 线程调用Stop()方法之前时要做的事情。
        /// </summary>
        public virtual void OnStopping()
        {

        }

        /// <summary>
        /// 线程调用Stop()成功后要做的事情。
        /// </summary>
        public virtual void OnStopped()
        {

        }
    }
}
