﻿/***********************************************
【名称】蒂森终检台数据转换器。
************************************************/
using QDAS;
using QDasConverter.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindGoes6.Database;

namespace QDasConverter.Core
{
    /// <summary>
    /// 蒂森终检台数据转换器。
    /// </summary>
    public class C2021T05_DisenConsole : ConvertBase 
    {
        /// <summary>
        /// 对Code表24小时更新1次。
        /// </summary>
        DateTime lastCodeUpdateTime = new DateTime(2021, 1, 1);

        Dictionary<string, string> codes = new Dictionary<string, string>();

        
        Dictionary<string, Chs> dics = new Dictionary<string, Chs>();


        struct Chs
        {
            public string code;
            public string testItem;
            public string k2101;
            public string k2110;
            public string k2111;
            public string consoleType;
            public string remark;

            /// <summary>
            /// 以 DF727B17_12 这样的格式返回。
            /// </summary>
            /// <returns></returns>
            internal string type_code()
            {
                return consoleType + "_" + code;
            }
        }

        public C2021T05_DisenConsole()
        {
            DisplayName = "蒂森终检台";
            TableName =   "Act_Eol_Test_Data";
            TableIDName = "ID";
            ConverterKey = nameof(C2021T05_DisenConsole);
            var ConfigFileFullpah = GetConfigurefileFullpath(ConverterKey);
            Version = "beta1. Released Date: 2022/01/30 Inner Name: C2021T05_DisenConsole";
            Version = $"beta2. Released Date: 2022/03/30 Inner Name: {ConverterKey}";
        }


        public override bool OnStart()
        {
            LoadConfigs();
            if (DebugMode)
            {
                SqlConnectionString = "Data Source = 117.71.62.132,8092;Initial Catalog = QS_727_DB;User ID = sa; Password = Huaun@2021;";
                sm = new WindGoes6.Database.SQLManager(SqlConnectionString);
            }

            return base.OnStart();
        }

        private void LoadConfigs(string configFile = @"./configs/C2021T03_CTL_Properties.csv")
        {
            dics.Clear();

            foreach (var item in File.ReadAllLines(configFile, Encoding.UTF8))
            {
                var line = item.Trim();
                if (line.Length == 0)
                    continue;
                var items = line.Split(',');
                if (items.Length>2 && items[1].Trim().Length > 0)
                {
             // 0,250,300,325,350,375,400,450,500,600,700,800,900,1200  mA NULL    2017-12-13 13:43:19.000 0118-0002_TOWER1 EOL Check
             // Code	TestItem	名称值（K2101）	下限值（K2110）	上限值（K2111）	备注
             // 27  I O 测试_拨叉1位置传感器     450 4550
             // 2021/12/21更新，添加一列
             // Code,ChName,名称值（K2101）,下限值（K2110）,上限值（K2111）,Type,备注
             //  27,I O 测试_拨叉1位置传感器,,450,4550,DF727B17,
             var chs = new Chs()
                    {
                        code = items[0].Trim(),
                        testItem = items[1].Trim(),
                        k2101 = items[2].Trim(),
                        k2110 = items[3].Trim(),
                        k2111 = items[4].Trim(),
                        consoleType = items[5].Trim(),
                        remark = items[6].Trim()
                    };
                    if (!dics.ContainsKey(chs.code))
                        dics.Add(chs.type_code(), chs);
                }
            }

            Console.WriteLine(dics.Count);
        }

        public override void OneLoop_Prepare()
        {
            if ((DateTime.Now - lastCodeUpdateTime).TotalHours > 24)
            {
                sm.CommandText = "SELECT Code,TestItem FROM QS_727_DB.dbo.Act_Test_Code";
                var data = sm.GetStrings();
                if (data != null)
                    foreach (var row in sm.GetStrings())
                        if (!codes.Keys.Contains(row[0]))
                            codes.Add(row[0], row[1]);
                lastCodeUpdateTime = DateTime.Now;
            }

            // 
            if (LastResID == 0)
            {
                LastResID = GetNextID(LastResID);
            }

            base.OneLoop_Prepare();
        }

        public override void OneLoop_Process()
        {
            sm.CommandText = "SELECT ID,[Type],SN,Code,TestItem,TestValue,Unit,IsOK,TestTime,DeviceCode,Producer,Sources";
            sm.CommandText += $" FROM Act_Eol_Test_Data WHERE ID >= {LastResID} and ID < {LastResID + BatchCount}";

            var data = sm.GetStrings();

            // 索引.名称      K值     示例  
            //  0.ID	     -	    280253
            //  1.Type	     K1001	DF727A01
            //  2.SN	     K0014	S40135B150001000B1000JA00184
            //  3.Code	     K2001	58
            //  4.TestItem	 K2002	液压系统建压能力测试_600
            //  5.TestValue	 K0001	1935
            //  6.Unit	     K2142	kPa
            //  7.IsOK	     -	    OK
            //  8.TestTime	 K0004	2018/2/11 13:01
            //  9.DeviceCode K0012	
            // 10.Producer	 -	    EOL
            // 11.Sources	 -	    Check

            // 按设备唯一编号(如S40135C15000100BN0400LB00234)进行分组
            Dictionary<string, List<string[]>> groups = new Dictionary<string, List<string[]>>();
            List<string> keys = new List<string>();

            foreach (var row in data)
            {
                var key = row[2]; // 设备编号
                if (!groups.ContainsKey(key))
                {
                    keys.Add(key);
                    groups.Add(key, new List<string[]>());
                }
                groups[key].Add(row);
            }

            for (int i = 0; i < keys.Count; i++)
            {
                var datai = groups[keys[i]];
                datai.OrderBy(d => d[3]);  // 根据编号排序。

                if (datai.Count == 0)
                    continue;

                QFile qf = new QFile();
                qf[1001] =  datai[0][1];
                qf[1002] =  datai[0][1];
                qf[1086] = "DCT-0590"; // 2021/12/27 可能是 datai[0][9];
                qf[1206] = "蒂森检测台";
                qf[1203] = "终检";

                foreach (var item in datai)
                {
                    /* 1. 先对数据进行验证，如果数据错了，不添加参数。 */
                    // 如果是空值则继续下个循环
                    string valuestr = item[5];
                    if (string.IsNullOrEmpty(valuestr))
                        continue;

                    // 用于处理每行的数值。
                    List<double> values = new List<double>();

                    // 多值的情况
                    if (valuestr.Contains(","))
                    {
                        //多值情况取终值
                        List<double> machs = new List<double>();
                        foreach (var str in valuestr.Split(','))
                            if (double.TryParse(str, out double v0))
                                machs.Add(v0);
                        double midValue = GetMidValue(machs);
                        values.Add(midValue);

                        //foreach (var str in valuestr.Split(','))
                        //    if (double.TryParse(str, out double v0))
                        //        values.Add(v0);

                    }
                    // 单值的情况
                    else if (double.TryParse(item[5], out double testvalue))
                    {
                        values.Add(testvalue);
                    }
                    // 解析失败继续下一循环
                    else
                    {
                        continue;
                    }
                    // 如果没有数据，则继续下一循环。
                    if (values.Count == 0)
                        continue;

                    /* 2. 开始提取参数和测量数据并转换为DFQ文件 */
                    // 添加参数层
                    QCharacteristic qc = qf.AddQCharacteristic();
                    qc[2001] = item[3];
                    qc[2002] = item[4]; // 这项就是测量的名称
                    if (dics.ContainsKey(item[1]+"_"+item[3]))
                    {
                        var chi = dics[item[1] + "_" + item[3]];
                        qc[2101] = chi.k2101;
                        qc[2110] = chi.k2110;
                        qc[2111] = chi.k2111;
                    }
                    qc[2142] = item[6];

                    // 处理数值层
                    var b1 = DateTime.TryParse(item[8], out DateTime testTime);
                    foreach (double value in values)
                    {
                        var di = qc.AddItem();
                        di.value = value;
                        di.date = b1 ? testTime : DateTime.MinValue;

                        // 软件版本号，仅在code=26时取testValue的值
                        if (item[3]=="26")
                            di[0009] = values[0];

                        // 检测设备catalog, 转换规则：0071-001 --> 总成预检测-CTL1
                        //if (item[9].Contains("0071-"))
                        //{
                        //    if (int.TryParse(item[9].Replace("0071-", ""), out int cid))
                        //    {
                        //        di[0012] = "110501" + (cid-1);
                        //        di[0010] = "110501" + (cid-1);
                        //    }
                        //}
                        di[0010] = "1107010";
                        di[0014] = item[2];// 零件ID
                    }
                }

                SaveToDFQ(qf, OutputDirectory, "DSC_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff"));
            }

        }

    }




}
