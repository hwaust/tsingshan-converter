﻿/********************************************
名称：日志级别类
作用：定义日志级别，同时可以控制日志输出。
参考：
- Log4j
  https://howtodoinjava.com/log4j/
- Log4j log levels – Log4j2 log levels example （推荐)
  https://howtodoinjava.com/log4j/logging-levels-in-log4j/
- 干货：日志打印的8种级别（很详细）
  https://zhuanlan.zhihu.com/p/63810820

更新：
2021/10/01 创建此类（从原内容继承修改）。
*********************************************/

namespace QDasConverter.Core
{
    /// <summary>
    /// 日志的类型，在非调试模式不显示TRACE和DEBUG的日志，详细作用如下：
    /// TRACE：程序运行状态追踪，如各类变量的值。
    /// DEBUG：用于验证值的正确性，如格式是否正确，连接是否成功等。 
    /// INFO ：执行的结果信息，此信息用于记录一些业务执行情况的信息，如文件输出，登陆记录等。 
    /// WARN ：潜在的可能导致出错的风险状况，如执行时间过长，文件体积过大等。 
    /// ERROR：错误事件，但应用可能还能继续运行，如打开失败，找不到文件，转换出错等。 
    /// FATAL：非常严重的可能会导致应用终止执行错误事件，如内存溢出等原因导致的程序退出。 
    /// </summary>
    public enum LogType : int
    {
        /// <summary>
        /// 为了追踪信息而定义的内容，一般用于输出一些变量的值。
        /// </summary>
        TRACE = 0,
        /// <summary>
        /// 用于验证值的正确性，如格式是否正确，连接是否成功等。
        /// </summary>
        Debug = 1,
        /// <summary>
        /// 执行的结果信息，此信息用于记录一些业务执行情况的信息，如文件输出，登陆记录等。 。
        /// </summary>
        INFO = 10,
        /// <summary>
        /// 潜在的可能导致出错的风险状况，如执行时间过长，文件体积过大等。
        /// </summary>
        WARNNING = 11,
        /// <summary>
        /// 错误事件，但应用可能还能继续运行，如打开失败，找不到文件，转换出错等。
        /// </summary>
        Error = 100,
        /// <summary>
        /// 非常严重的可能会导致应用终止执行错误事件，如内存溢出等原因导致的程序退出。 
        /// </summary>
        Fatal = 101
    }
}
