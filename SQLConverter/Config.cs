﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QDasConverter
{
    public class Config
    {
        /// <summary>
        /// 所有的编码。
        /// </summary>
        public static string[] AllEncodings = new string[] { "Default", "UTF-8", "GB2312", "UTF-16", "UTF-32", "UTF-7", "UTF-16BE", "US-ASCII"};

        /// <summary>
        /// 连接字符串。
        /// </summary>
        public static string ConnectionString = "ConnectionString";

        /// <summary>
        /// 转换器名称。
        /// </summary>
        public static string Converter = "Converter";


        /// <summary>
        /// 转换器名称。
        /// </summary>
        public static string OutputDirectory = "OutputDirectory";

        /// <summary>
        /// 转换器名称。
        /// </summary>
        public static string TempDirectory = "TempDirectory";

        /// <summary>
        /// 最后一个转换记录的编号。
        /// </summary>
        public static string LastResID = "LastResID";

        /// <summary>
        /// 每批转换的ID范围，中间不一定有这么多。
        /// </summary>
        public static string BatchCount = "BatchCount";

        /// <summary>
        /// 每两次同步的时间间隔。
        /// </summary>
        public static string ConvertInterval = "ConvertInterval";


        /// <summary>
        /// 每两次同步的时间间隔。
        /// </summary>
        public static string InputEncodingIndex = "InputEncodingIndex";


        /// <summary>
        /// 每两次同步的时间间隔。
        /// </summary>
        public static string OutputEncodingIndex = "OutputEncodingIndex";

        /// <summary>
        /// 是否为调试模式，只有值为1时才表示启动调试模式。
        /// </summary>
        public static string DebugMode = "DebugMode";
    }
}
