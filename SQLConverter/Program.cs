﻿using QDasConverter.Forms;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QDasConverter
{
	static class Program
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		
		static void Main()
		{
			try
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Control.CheckForIllegalCrossThreadCalls = false;
				Common.Initialize();
				Application.Run(new MainForm());
			}catch (Exception ex)
			{
	
				AddLog(ex.Message);
            }

		}

        private static void AddLog(string message)
        {
            Common.AddLog(message);
        }
    }
}
