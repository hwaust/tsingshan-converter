【MainForm】 **独立线程**
主要用于用户交互，包括用户输入和输出显示等，具体包括：
1) 连接配置
2) 任务配置
3) 任务启停
4) 执行状态查看


【SQL Processor】 **独立线程**
用于执行SQL的任务，并能够随时启停。
1) 可指定转换模块；
2) 可配置运行时间间隔；
3) 可配置超时时间间隔；
4) 能够随时启停。

【SqlConvertProcessor】
1. OnStart 前会根据是否需要检测连接；
2. OnStart 时读取 LastResID；
3. OnStop  时写入 LastResID；
4. 

【Converter】
核心思路：不参与IO，所有配置信息全由外部初始化。
1) 配置路径，包括输出路径、临时路径等；
2) 配置连接字符串，指定所要连接的字符串；
3) 任务执行，包括各个阶段的任务；
4) 在最后阶段才把事务完成，这样保证不会重复。
   原理：临时建立一个文件夹，把完成的内容放在里面，只有数据齐了才能复制。
5）统一使用 LastResID 表示主表的最后一个ID。

【参考资料】
测试连接：Data Source = 117.71.62.132,8092;Initial Catalog = A30;User ID = sa; Password = Huaun@2021;

SELECT 
  schema_name(systable.schema_id) as TableSchema, systable.name as TableName, sysids.rows as NumberOfRows
FROM 
  sys.tables as systable, sysindexes as sysids 
WHERE 
  systable.object_id = sysids.id and sysids.indid <=1 and sysids.rows > 10  
ORDER BY NumberOfRows DESC